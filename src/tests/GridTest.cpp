#include "GridTest.h"

TEST_CASE("Testing some basic grid functions")
{
  using openpixi::Settings;
  using openpixi::rational;
  using openpixi::index;

  constexpr index nDimensions = openpixi::Simulation::nDimensions;

  Settings settings({4, 2, 3}, {1.0, 1.0, 1.0}, 0, 1, 0.25, 2.0);
  openpixi::Simulation simulation(settings);
  auto     gridSize = simulation.gridSize;

  SECTION("getIndex: correct order")
  {
    index positionIndex = 0;
    for(index iX = 0; iX < gridSize[Settings::x]; ++iX)
    {
      for(index iY = 0; iY < gridSize[Settings::y]; ++iY)
      {
        for(index iZ = 0; iZ < gridSize[Settings::z]; ++iZ)
        {
          REQUIRE(positionIndex == simulation.getIndex({iX, iY, iZ}));
          ++positionIndex;
        }
      }
    }
  }


  SECTION("getIndex/getPosition: bijective")
  {
    for(index i = 0; i < simulation.accumulatedCellCounts[0]; ++i)
    {
      std::array<index, nDimensions> position = simulation.getPosition(i);
      REQUIRE(simulation.getIndex(position) == i);
    }
  }

  // Performing the index shift manually or with the class method should give the same result
  SECTION("index shifting")
  {
    std::array<index, nDimensions> position;
    for(index i = 0; i < 10; ++i)
    {
      // Generate random position
      for(index j = 0; j < nDimensions; ++j)
      {
        position[j] = rand() % gridSize[j];
      }
      index oldPositionIndex = simulation.getIndex(position);

      // Select random shift
      const index direction   = rand() % nDimensions;
      const index orientation = 2 * (rand() % 2) - 1;

      position[direction] += orientation;
      position[direction]  = (position[direction] % gridSize[direction] +
                              gridSize[direction]) % gridSize[direction];
      index manuallyShiftedPositionIndex = simulation.getIndex(position);

      index shiftedPositionIndex = simulation.shift(oldPositionIndex, direction,
                                                    static_cast<openpixi::Orientation>(orientation));

      REQUIRE(manuallyShiftedPositionIndex == shiftedPositionIndex);
    }
  }
}