#pragma once

#include <array>
#include <vector>
#include <random>
#include <memory>

#include "catch.hpp"

#include "../openpixi/CustomTypes.h"

#include "../openpixi/GroupAndAlgebra.h"
#include "../openpixi/Settings.h"
#include "../openpixi/Simulation.h"

#include "../openpixi/initial/GaussViolationExample.h"
#include "../openpixi/diagnostics/GaussConstraint.h"
#include "../openpixi//initial/CoherentMvModel.h"
#include "../openpixi/initial/CgcInitialCondition.h"

openpixi::GroupElement createRandomGroupElement(const openpixi::rational amplitude);