#include "Su2Test.h"

#include <functional>

using openpixi::AlgebraElement;
using openpixi::GroupElement;
using openpixi::rational;
using Eigen::Matrix2cd;

TEST_CASE("Simple SU(2) AlgebraElement tests")
{
  SECTION("Addition")
  {
    AlgebraElement a(1.0, 2.0, 3.0);
    AlgebraElement b(5.0, 6.0, 7.0);

    AlgebraElement result = a + b;
    compareAlgebraElements(result, AlgebraElement(6.0, 8.0, 10.0));

    result += a;
    compareAlgebraElements(result, AlgebraElement(7.0, 10.0, 13.0));

    // Check if original variables are unaffected
    compareAlgebraElements(a, AlgebraElement(1.0, 2.0, 3.0));
    compareAlgebraElements(b, AlgebraElement(5.0, 6.0, 7.0));
  }

  SECTION("Unary '-' operator")
  {
    AlgebraElement a(1.0, 2.0, 3.0);

    AlgebraElement result = -a;
    compareAlgebraElements(result, AlgebraElement(-1.0, -2.0, -3.0));

    // Check if original variables are unaffected
    compareAlgebraElements(a, AlgebraElement(1.0, 2.0, 3.0));
  }

  SECTION("Scalar multiplication")
  {
    AlgebraElement a(1.0, 2.0, 3.0);

    AlgebraElement result = a * 10.0;
    compareAlgebraElements(result, AlgebraElement(10.0, 20.0, 30.0));

    result = 10.0 * a;
    compareAlgebraElements(result, AlgebraElement(10.0, 20.0, 30.0));

    result *= 0.1;
    compareAlgebraElements(result, AlgebraElement(1.0, 2.0, 3.0));

    // Check if original variables are unaffected
    compareAlgebraElements(a, AlgebraElement(1.0, 2.0, 3.0));
  }

  SECTION("Square")
  {
    AlgebraElement a(1.0, 2.0, 3.0);
    REQUIRE(a.square() == Approx(14.0));
  }

  SECTION("Dot product")
  {
    rational dotProduct;
    AlgebraElement a(3.0, 5.0, 13.0);
    AlgebraElement b(2.0, 7.0, 11.0);
    dotProduct = a.dot(b);

    REQUIRE(dotProduct == Approx(184.0));

    // Check if original variables are unaffected
    compareAlgebraElements(a, AlgebraElement(3.0, 5.0, 13.0));
    compareAlgebraElements(b, AlgebraElement(2.0, 7.0, 11.0));
  }
}


TEST_CASE("Simple SU(2) GroupElement tests")
{
  SECTION("Addition")
  {
    GroupElement a(1.0, 2.0, 3.0, 4.0);
    GroupElement b(5.0, 6.0, 7.0, 8.0);

    GroupElement result = a + b;
    compareGroupElements(result, GroupElement(6.0, 8.0, 10.0, 12.0));

    result += a;
    compareGroupElements(result, GroupElement(7.0, 10.0, 13.0, 16.0));

    // Check if original variables are unaffected
    compareGroupElements(a, GroupElement(1.0, 2.0, 3.0, 4.0));
    compareGroupElements(b, GroupElement(5.0, 6.0, 7.0, 8.0));
  }

  SECTION("Unary '-' operator")
  {
    GroupElement a(1.0, 2.0, 3.0, 4.0);

    GroupElement result = -a;
    compareGroupElements(result, GroupElement(-1.0, -2.0, -3.0, -4.0));

    // Check if original variables are unaffected
    compareGroupElements(a, GroupElement(1.0, 2.0, 3.0, 4.0));
  }

  SECTION("Scalar multiplication")
  {
    GroupElement a(1.0, 2.0, 3.0, 4.0);

    GroupElement result = a * 10.0;
    compareGroupElements(result, GroupElement(10.0, 20.0, 30.0, 40.0));

    result = 10.0 * a;
    compareGroupElements(result, GroupElement(10.0, 20.0, 30.0, 40.0));

    result *= 0.1;
    compareGroupElements(result, GroupElement(1.0, 2.0, 3.0, 4.0));

    // Check if original variables are unaffected
    compareGroupElements(a, GroupElement(1.0, 2.0, 3.0, 4.0));
  }

  SECTION("Dagger")
  {
    GroupElement a(1.0, 2.0, 3.0, 4.0);

    GroupElement result = a.dagger();
    compareGroupElements(result, GroupElement(1.0, -2.0, -3.0, -4.0));

    // Check if original variables are unaffected
    compareGroupElements(a, GroupElement(1.0, 2.0, 3.0, 4.0));
  }
}


TEST_CASE("Complex SU(2) tests")
{
  using namespace std::literals;
  using openpixi::index;

  // Define identity and Pauli matrices
  std::vector<Matrix2cd> pauliMatrices(4);

  pauliMatrices[0] << 1.0f + 0.0if, 0.0f + 0.0if,
    0.0f + 0.0if, 1.0f + 0.0if;
  pauliMatrices[1] << 0.0f + 0.0if, 1.0f + 0.0if,
    1.0f + 0.0if, 0.0f + 0.0if;
  pauliMatrices[2] << 0.0f + 0.0if, 0.0f - 1.0if,
    0.0f + 1.0if, 0.0f + 0.0if;
  pauliMatrices[3] << 1.0f + 0.0if, 0.0f + 0.0if,
    0.0f + 0.0if, -1.0f + 0.0if;

  SECTION("Multiplication")
  {
    for(index i = 0; i < 10; ++i)
    {
      GroupElement u  = createRandomGroupElement();
      GroupElement v  = createRandomGroupElement();
      Matrix2cd    uM = convertGroupElementToEigenMatrix(u, pauliMatrices);
      Matrix2cd    vM = convertGroupElementToEigenMatrix(v, pauliMatrices);

      GroupElement w  = u * v;
      Matrix2cd    wM = uM * vM;

      compareGroupElements(w, convertEigenMatrixToGroupElement(wM, pauliMatrices));

      w  = u;
      w *= v;

      compareGroupElements(w, convertEigenMatrixToGroupElement(wM, pauliMatrices));
    }
  }

  SECTION("log() and exp(): bijective property")
  {
    for(index i = 0; i < 10; ++i)
    {
      // This only works for group elements 'close' to the identity
      AlgebraElement a = createRandomAlgebraElement(1.0);
      GroupElement   u = a.exp();
      AlgebraElement b = u.log();

      compareAlgebraElements(a, b);
    }
  }

  SECTION("log() and exp(): correct calculation")
  {
    for(index i = 0; i < 10; ++i)
    {
      AlgebraElement a = createRandomAlgebraElement(10.0);
      GroupElement   u = a.exp();
      AlgebraElement b = u.log();

      Matrix2cd aM = convertAlgebraElementToEigenMatrix(a, pauliMatrices);
      aM *= 1.0if;
      Matrix2cd uM = aM.exp();
      Matrix2cd vM = uM.log();
      vM *= -1.0if;

      compareGroupElements(u, convertEigenMatrixToGroupElement(uM, pauliMatrices));
      compareAlgebraElements(b, convertEigenMatrixToAlgebraElement(vM, pauliMatrices));
    }
  }

  SECTION("Adjoint action")
  {
    for(index i = 0; i < 10; ++i)
    {
      AlgebraElement a = createRandomAlgebraElement(0.5);
      GroupElement   u = a.exp();
      AlgebraElement b = createRandomAlgebraElement();
      AlgebraElement c = b.act(u);

      // Quick sanity check
      CHECK(b.square() == Approx(c.square()));

      Matrix2cd uM = convertGroupElementToEigenMatrix(u, pauliMatrices);
      Matrix2cd vM = convertAlgebraElementToEigenMatrix(b, pauliMatrices);
      Matrix2cd cM = uM.adjoint() * vM * uM;

      compareAlgebraElements(c, convertEigenMatrixToAlgebraElement(cM, pauliMatrices));
    }
  }
}


// --- Utility functions for comparing results -----------------------------------------------------

void compareAlgebraElements(const AlgebraElement actual,
                            const AlgebraElement expected)
{
  CHECK(actual.v1 == Approx(expected.v1));
  CHECK(actual.v2 == Approx(expected.v2));
  CHECK(actual.v3 == Approx(expected.v3));
}


void compareGroupElements(GroupElement actual, GroupElement expected)
{
  CHECK(actual.g0 == Approx(expected.g0));
  CHECK(actual.g1 == Approx(expected.g1));
  CHECK(actual.g2 == Approx(expected.g2));
  CHECK(actual.g3 == Approx(expected.g3));
}


// --- Useful functions for complex tests ----------------------------------------------------------

GroupElement convertEigenMatrixToGroupElement(const Matrix2cd matrix,
                                              const std::vector<Matrix2cd> pauliMatrices)
{
  rational g0, g1, g2, g3;
  g0 = 0.5 * (pauliMatrices[0] * matrix).real().trace();
  g1 = 0.5 * (pauliMatrices[1] * matrix).imag().trace();
  g2 = 0.5 * (pauliMatrices[2] * matrix).imag().trace();
  g3 = 0.5 * (pauliMatrices[3] * matrix).imag().trace();

  return GroupElement(g0, g1, g2, g3);
}


Matrix2cd convertGroupElementToEigenMatrix(const GroupElement groupElement,
                                           const std::vector<Matrix2cd> pauliMatrix)
{
  using namespace std::literals;

  Matrix2cd matrix;
  matrix += groupElement.g1 * pauliMatrix[1];
  matrix += groupElement.g2 * pauliMatrix[2];
  matrix += groupElement.g3 * pauliMatrix[3];
  matrix *= 1.0if;
  matrix += groupElement.g0 * pauliMatrix[0];
  return matrix;
}


Matrix2cd convertAlgebraElementToEigenMatrix(const AlgebraElement algebraElement,
                                             const std::vector<Matrix2cd> pauliMatrices)
{
  Matrix2cd matrix;
  matrix += algebraElement.v1 * pauliMatrices[1];
  matrix += algebraElement.v2 * pauliMatrices[2];
  matrix += algebraElement.v3 * pauliMatrices[3];
  matrix *= 0.5;
  return matrix;
}


AlgebraElement convertEigenMatrixToAlgebraElement(const Matrix2cd matrix,
                                                  const std::vector<Matrix2cd> pauliMatrices)
{
  rational v1, v2, v3;
  v1 = (pauliMatrices[1] * matrix).real().trace();
  v2 = (pauliMatrices[2] * matrix).real().trace();
  v3 = (pauliMatrices[3] * matrix).real().trace();

  return AlgebraElement(v1, v2, v3);
}


GroupElement createRandomGroupElement(const double amplitude)
{
  static std::random_device randomDevice;
  static std::mt19937 engine(randomDevice());

  std::uniform_real_distribution<> distribution(-amplitude, amplitude);
  auto getRandomNumber = std::bind(distribution, engine);
  return GroupElement(getRandomNumber(), getRandomNumber(), getRandomNumber(),
                      getRandomNumber(engine));
}


AlgebraElement createRandomAlgebraElement(const double amplitude)
{
  static std::random_device randomDevice;
  static std::mt19937 engine(randomDevice());

  std::uniform_real_distribution<> distribution(-amplitude, amplitude);
  auto getRandomNumber = std::bind(distribution, engine);
  return AlgebraElement(distribution(engine), distribution(engine), distribution(engine));
}