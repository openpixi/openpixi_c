#pragma once

#include <memory>
#include <utility>
#include <array>

#include "catch.hpp"

#include "../openpixi/CustomTypes.h"

#include "../openpixi/Settings.h"
#include "../openpixi/Simulation.h"

#include "../openpixi/initial/GaussianPulse.h"
#include "../openpixi/diagnostics/ProjectedEnergyDensity.h"
#include "../openpixi//initial/CoherentMvModel.h"
#include "../openpixi/initial/CgcInitialCondition.h"