#pragma once

#include <vector>
#include <memory>
#include <utility>
#include <array>

#include "catch.hpp"

#include "../openpixi/CustomTypes.h"

#include "../openpixi/Settings.h"
#include "../openpixi/Simulation.h"

#include "../openpixi/initial/GaussianPulse.h"

#include "../openpixi/diagnostics/ProjectedEnergyDensity.h"
#include "../openpixi/diagnostics/ProjectedPowerDensity.h"
#include "../openpixi/diagnostics/PoyntingVector.h"
#include "../openpixi/diagnostics/ProjectedDivS.h"
#include "../openpixi/diagnostics/ProjectedJInE.h"
#include "../openpixi//initial/CoherentMvModel.h"
#include "../openpixi/initial/CgcInitialCondition.h"