#pragma once

#include <array>

#include "catch.hpp"

#include "../openpixi/CustomTypes.h"

#include "../openpixi/Settings.h"
#include "../openpixi/Simulation.h"