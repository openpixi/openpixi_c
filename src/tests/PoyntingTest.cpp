#include "PoyntingTest.h"

TEST_CASE("Testing Poynting's Theorem")
{
  using openpixi::Settings;
  using openpixi::rational;
  using openpixi::index;
  using Array3Index    = std::array<index, Settings::nDimensions>;
  using Array3Rational = std::array<rational, Settings::nDimensions>;

  constexpr int t0 = 0;
  constexpr int nTimeSteps = 200;

  SECTION("Just fields")
  {
    // Set approximation accuracy
    Approx approx = Approx::custom().epsilon(5.0e-2);

    const Array3Index gridSize = {64, 8, 8};
    const Array3Rational latticeSpacings = {1.0, 1.0, 1.0};
    const rational    timeStep = 0.25;
    const rational    couplingConstant = 2.0;
    Settings settings(gridSize, latticeSpacings, t0, nTimeSteps, timeStep, couplingConstant);

    // Initial conditions
    auto gaussianPulse = std::make_shared<openpixi::GaussianPulse>();
    gaussianPulse->width  = 8.0;
    gaussianPulse->center = {settings.gridSize[Settings::x] / 2.0, 0.0, 0.0};
    gaussianPulse->amplitudeVector = {0.0, 0.01, 0.0};
    settings.initialConditions.push_back(std::move(gaussianPulse));

    // Diagnostics
    auto projectedEnergyDensity = std::make_shared<openpixi::ProjectedEnergyDensity>(settings);
    settings.diagnostics.push_back(projectedEnergyDensity);
    auto projectedPowerDensity  =
      std::make_shared<openpixi::ProjectedPowerDensity>(settings, projectedEnergyDensity);
    settings.diagnostics.push_back(projectedPowerDensity);
    auto poyntingVector = std::make_shared<openpixi::PoyntingVector>(settings);
    auto projectedDivS  = std::make_shared<openpixi::ProjectedDivS>(settings, poyntingVector);
    // Projected divS must be calculated before the Poynting vector is updated because the power
    // density is also calculated for the previous time step
    settings.diagnostics.push_back(projectedDivS);
    settings.diagnostics.push_back(poyntingVector);

    openpixi::Simulation simulation(settings);
    simulation.applyInitialConditions();

    for(index i = 0; i < nTimeSteps; ++i)
    {
      simulation.evolve();
    }
    simulation.executeDiagnostics();
    simulation.evolve();
    simulation.executeDiagnostics();

    // Check if divS + dE/dt = 0, with S being the Poynting vector and E the energy density
    const auto nLongitudinalCells = simulation.rightBoundary() - simulation.leftBoundary();
    std::vector<rational> totalProjectedPowerDensity(nLongitudinalCells);
    for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
    {
      totalProjectedPowerDensity[iL] = projectedPowerDensity->longitudinalElectric[iL] +
                                       projectedPowerDensity->transverseElectric[iL] +
                                       projectedPowerDensity->longitudinalMagnetic[iL] +
                                       projectedPowerDensity->transverseMagnetic[iL];
    }

    auto maximumProjectedPowerDensity = *std::max_element(totalProjectedPowerDensity.begin(),
                                                          totalProjectedPowerDensity.end());

    for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
    {
      CHECK(approx((projectedDivS->projectedDivS[iL] + totalProjectedPowerDensity[iL]) /
                   maximumProjectedPowerDensity) == 0);
    }
  }

  SECTION("With particles")
  {
    using Direction = Settings::Direction;

    // Set approximation accuracy
    Approx approx = Approx::custom().epsilon(7.0e-2);

    const Array3Index gridSize   = {128, 8, 8};
    const Array3Rational latticeSpacings = {1.0, 1.0, 1.0};
    const rational    timeStep   = 0.25;
    const rational    couplingConstant = 2.0;
    const auto boundaryCondition = openpixi::BoundaryCondition::fixed;
    Settings   settings(gridSize, latticeSpacings, t0, nTimeSteps, timeStep, couplingConstant,
                        boundaryCondition);

    // Initial conditions
    rational width  = 8.0 / settings.latticeSpacings[Direction::x];
    rational center = width * openpixi::CoherentMvModel::widthFactor + 2.0;
    rational mu = 0.1;    // Physical units!
    auto     coherentMvModel     = std::make_unique<openpixi::CoherentMvModel>(center, width, mu,
                                                                               settings, 1);
    rational infraredRegulator   = 0.03; // Phys. units!
    auto     cgcInitialCondition =
      std::make_shared<openpixi::CgcInitialCondition>(std::move(coherentMvModel),
                                                      openpixi::Velocity::positive,
                                                      infraredRegulator, 0.0,
                                                      openpixi::utility::Cutoff::none);
    settings.initialConditions.push_back(std::move(cgcInitialCondition));

    // Diagnostics
    auto projectedEnergyDensity = std::make_shared<openpixi::ProjectedEnergyDensity>(settings);
    settings.diagnostics.push_back(projectedEnergyDensity);
    auto projectedPowerDensity  =
      std::make_shared<openpixi::ProjectedPowerDensity>(settings, projectedEnergyDensity);
    settings.diagnostics.push_back(projectedPowerDensity);
    auto poyntingVector = std::make_shared<openpixi::PoyntingVector>(settings);
    auto projectedDivS  = std::make_shared<openpixi::ProjectedDivS>(settings, poyntingVector);
    // Projected divS must be calculated before the Poynting vector is updated because the power
    // density is also calculated for the previous time step
    settings.diagnostics.push_back(projectedDivS);
    settings.diagnostics.push_back(poyntingVector);
    auto projectedJInE = std::make_shared<openpixi::ProjectedJInE>(settings);
    settings.diagnostics.push_back(projectedJInE);

    openpixi::Simulation simulation(settings);
    simulation.applyInitialConditions();

    for(index i = 0; i < nTimeSteps; ++i)
    {
      simulation.evolve();
    }
    simulation.executeDiagnostics();
    simulation.evolve();
    simulation.executeDiagnostics();

    // Check if divS + dH/dt + j * E = 0, with S being the Poynting vector, H the energy density,
    // j the current density and E the electric field
    const auto nLongitudinalCells = simulation.rightBoundary() - simulation.leftBoundary();
    std::vector<rational> totalProjectedPowerDensity(nLongitudinalCells);

    int i = 0;
    for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
    {
      totalProjectedPowerDensity[i] = projectedPowerDensity->longitudinalElectric[iL] +
                                      projectedPowerDensity->transverseElectric[iL] +
                                      projectedPowerDensity->longitudinalMagnetic[iL] +
                                      projectedPowerDensity->transverseMagnetic[iL];
      i++;
    }

    auto maximumProjectedPowerDensity = *std::max_element(totalProjectedPowerDensity.begin(),
                                                          totalProjectedPowerDensity.end());

    i = 0;
    for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
    {
      CHECK(approx((projectedDivS->projectedDivS[iL] + totalProjectedPowerDensity[i] +
                    projectedJInE->projectedJInE[iL]) / maximumProjectedPowerDensity) == 0);
      i++;
    }
  }
}