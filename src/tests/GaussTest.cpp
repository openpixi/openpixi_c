#include "GaussTest.h"

#include <functional>

TEST_CASE("Testing the Gauss constraint method")
{
  using openpixi::Settings;
  using openpixi::rational;
  using openpixi::index;
  using Array3Index    = std::array<index, Settings::nDimensions>;
  using Array3Rational = std::array<rational, Settings::nDimensions>;

  constexpr index nDimensions = openpixi::Simulation::nDimensions;

  // Set approximation accuracy
  Approx approx    = Approx::custom().epsilon(1.0e-10);

  constexpr int t0 = 0;
  constexpr int nTimeSteps   = 100;
  const Array3Index gridSize = {16, 16, 16};
  const Array3Rational latticeSpacings = {0.5, 1.0, 1.0};
  const rational    couplingConstant   = 2.0;
  const rational    timeStep = 0.25;
  Settings settings(gridSize, latticeSpacings, t0, nTimeSteps, timeStep, couplingConstant);

  // Initial conditions
  auto initialCondition = std::make_shared<openpixi::GaussViolationExample>(1.0);
  settings.initialConditions.push_back(initialCondition);

  openpixi::Simulation simulation(settings);
  simulation.applyInitialConditions();

  const index nCells = simulation.accumulatedCellCounts[0];

  // First test the simple, basically Abelian result
  SECTION("Compare initial field configuration to analytic result")
  {
    // Due to discontinuity of the field, the result is only correct inside the simulation box ->
    // leave out the boundary cells
    for(index iX = 1; iX < gridSize[Settings::x] - 1; ++iX)
    {
      for(index iY = 1; iY < gridSize[Settings::y] - 1; ++iY)
      {
        for(index iZ = 1; iZ < gridSize[Settings::z] - 1; ++iZ)
        {
          std::array<index, nDimensions> posistion = {iX, iY, iZ};
          index positionIndex    = simulation.getIndex(posistion);
          rational latticeResult =
            openpixi::GaussConstraint::calculateGaussConstraint(simulation, positionIndex).square();
          CHECK(latticeResult == approx(initialCondition->analyticalResult));
        }
      }
    }
  }

  // Test the non-Abelian part by applying a random gauge transformation
  SECTION("Apply random gauge transformation and compare")
  {
    std::vector<openpixi::GroupElement> V(nCells);
    for(index index = 0; index < nCells; ++index)
    {
      V[index] = createRandomGroupElement(1.0);
    }
    simulation.applyGaugeTransformation(V);

    // Due to discontinuity of the field, the result is only correct inside the simulation box ->
    // leave out the boundary cells
    for(index iX = 1; iX < gridSize[Settings::x] - 1; ++iX)
    {
      for(index iY = 1; iY < gridSize[Settings::y] - 1; ++iY)
      {
        for(index iZ = 1; iZ < gridSize[Settings::z] - 1; ++iZ)
        {
          std::array<index, nDimensions> position = {iX, iY, iZ};
          index positionIndex    = simulation.getIndex(position);
          rational latticeResult =
            openpixi::GaussConstraint::calculateGaussConstraint(simulation, positionIndex).square();
          CHECK(latticeResult == approx(initialCondition->analyticalResult));
        }
      }
    }
  }

  // Test if the equations of motion preserve the Gauss constraint at each point
  SECTION("Evolve fields and compare")
  {
    std::vector<openpixi::GroupElement> V(nCells);
    for(index i = 0; i < nCells; ++i)
    {
      V[i] = createRandomGroupElement(1.0);
    }
    simulation.applyGaugeTransformation(V);

    for(int i = 0; i < nTimeSteps; ++i)
    {
      simulation.evolve();
    }

    // Due to discontinuity of the field, the result is only correct inside the simulation box ->
    // leave out the boundary cells
    for(index iX = 1; iX < gridSize[Settings::x] - 1; ++iX)
    {
      for(index iY = 1; iY < gridSize[Settings::y] - 1; ++iY)
      {
        for(index iZ = 1; iZ < gridSize[Settings::z] - 1; ++iZ)
        {
          std::array<index, nDimensions> position = {iX, iY, iZ};
          index positionIndex    = simulation.getIndex(position);
          rational latticeResult =
            openpixi::GaussConstraint::calculateGaussConstraint(simulation, positionIndex).square();
          CHECK(latticeResult == approx(initialCondition->analyticalResult));
        }
      }
    }
  }

  SECTION("Evolve nucleons")
  {
    using Direction = Settings::Direction;
    using openpixi::GaussConstraint;

    constexpr int t0 = 0;
    constexpr int nTimeSteps   = 100;
    const Array3Index gridSize = {64, 16, 16};
    const Array3Rational latticeSpacings = {1.0, 1.0, 1.0};
    const rational    couplingConstant   = 2.0;
    const rational    timeStep   = 0.25;
    const auto boundaryCondition = openpixi::BoundaryCondition::fixed;
    Settings   settings(gridSize, latticeSpacings, t0, nTimeSteps, timeStep, couplingConstant,
                        boundaryCondition);

    // Initial conditions
    rational width  = 4.0;
    rational center = width * openpixi::CoherentMvModel::widthFactor + 2.0;
    rational mu = 0.1;
    auto     coherentMvModel     = std::make_unique<openpixi::CoherentMvModel>(center, width, mu,
                                                                               settings, 1);
    rational infraredRegulator   = 0.03;
    auto     cgcInitialCondition =
      std::make_shared<openpixi::CgcInitialCondition>(std::move(coherentMvModel),
                                                      openpixi::Velocity::positive,
                                                      infraredRegulator, 0.0,
                                                      openpixi::utility::Cutoff::none);
    settings.initialConditions.push_back(std::move(cgcInitialCondition));

    openpixi::Simulation simulation(settings);
    simulation.applyInitialConditions();

    for(int i = 0; i < nTimeSteps; ++i)
    {
      simulation.evolve();
    }

    // Due to discontinuity of the field, the result is only correct inside the simulation box ->
    // leave out the boundary cells
    for(index iX = 1; iX < gridSize[Settings::x] - 1; ++iX)
    {
      for(index iY = 1; iY < gridSize[Settings::y] - 1; ++iY)
      {
        for(index iZ = 1; iZ < gridSize[Settings::z] - 1; ++iZ)
        {
          index positionIndex = simulation.getIndex({iX, iY, iZ});
          rational gaussConstraint =
            GaussConstraint::calculateGaussConstraint(simulation, positionIndex).square();
          // After the evolution, the Gauss constraint should still be 0
          CHECK(0 == approx(gaussConstraint));
        }
      }
    }
  }
}


openpixi::GroupElement createRandomGroupElement(const openpixi::rational amplitude)
{
  static std::random_device randomDevice;
  static std::mt19937 engine(randomDevice());

  std::uniform_real_distribution<> distribution(-amplitude, amplitude);
  auto getRandomNumber = std::bind(distribution, engine);
  openpixi::AlgebraElement algebraElement(getRandomNumber(), getRandomNumber(), getRandomNumber());
  return algebraElement.exp();
}