#include "EnergyTest.h"

TEST_CASE("Testing the conservation of energy")
{
  using openpixi::Settings;
  using openpixi::rational;
  using openpixi::index;
  using Array3Index    = std::array<index, Settings::nDimensions>;
  using Array3Rational = std::array<rational, Settings::nDimensions>;

  constexpr int t0 = 0;
  constexpr int nTimeSteps = 200;

  SECTION("Just fields")
  {
    // Set approximation accuracy
    Approx approx = Approx::custom().epsilon(1.0e-4);

    const Array3Index    gridSize = {64, 8, 8};
    const Array3Rational latticeSpacings = {1.0, 1.0, 1.0};
    const rational       timeStep = 0.25;
    const rational       couplingConstant = 2.0;
    openpixi::Settings   settings(gridSize, latticeSpacings, t0, nTimeSteps, timeStep,
                                  couplingConstant);

    // Initial conditions
    auto gaussianPulse = std::make_shared<openpixi::GaussianPulse>();
    gaussianPulse->width  = 8.0;
    gaussianPulse->center = {settings.gridSize[Settings::x] / 2.0, 0.0, 0.0};
    gaussianPulse->amplitudeVector = {0.0, 0.01, 0.0};
    settings.initialConditions.push_back(std::move(gaussianPulse));

    // Diagnostics
    auto projectedEnergyDensity = std::make_shared<openpixi::ProjectedEnergyDensity>(settings);
    settings.diagnostics.push_back(projectedEnergyDensity);

    openpixi::Simulation simulation(settings);
    simulation.applyInitialConditions();
    simulation.executeDiagnostics();

    // Calculate initial total energy
    rational initialTotalEnergy = 0.0;
    for(index i = simulation.leftBoundary(); i < simulation.rightBoundary(); ++i)
    {
      initialTotalEnergy += projectedEnergyDensity->longitudinalElectric[i];
      initialTotalEnergy += projectedEnergyDensity->transverseElectric[i];
      initialTotalEnergy += projectedEnergyDensity->longitudinalMagnetic[i];
      initialTotalEnergy += projectedEnergyDensity->transverseMagnetic[i];
    }

    // Evolve fields and links in time
    for(int i = 0; i < nTimeSteps; ++i)
    {
      simulation.evolve();
    }

    simulation.executeDiagnostics();

    // Calculate final total energy
    rational finalTotalEnergy = 0.0;
    for(index i = simulation.leftBoundary(); i < simulation.rightBoundary(); ++i)
    {
      finalTotalEnergy += projectedEnergyDensity->longitudinalElectric[i];
      finalTotalEnergy += projectedEnergyDensity->transverseElectric[i];
      finalTotalEnergy += projectedEnergyDensity->longitudinalMagnetic[i];
      finalTotalEnergy += projectedEnergyDensity->transverseMagnetic[i];
    }

    CHECK(approx((finalTotalEnergy - initialTotalEnergy) / initialTotalEnergy) == 0.0);
  }

  SECTION("With particles")
  {
    using Direction = Settings::Direction;

    // Set approximation accuracy
    Approx approx = Approx::custom().epsilon(1.0e-2);

    const Array3Index    gridSize = {128, 8, 8};
    const Array3Rational latticeSpacings = {1.0, 1.0, 1.0};
    const rational       timeStep = 0.25;
    const rational       couplingConstant = 2.0;
    const auto boundaryCondition  = openpixi::BoundaryCondition::fixed;
    openpixi::Settings   settings(gridSize, latticeSpacings, t0, nTimeSteps, timeStep,
                                  couplingConstant, boundaryCondition);

    // Initial conditions
    rational width  = 8.0 / settings.latticeSpacings[Direction::x];
    rational center = width * openpixi::CoherentMvModel::widthFactor + 2.0;
    rational mu = 0.1;   // Physical units!
    auto     coherentMvModel     = std::make_unique<openpixi::CoherentMvModel>(center, width, mu,
                                                                               settings, 1);
    rational infraredRegulator   = 0.03; // Phys. units!
    auto     cgcInitialCondition =
      std::make_shared<openpixi::CgcInitialCondition>(std::move(coherentMvModel),
                                                      openpixi::Velocity::positive,
                                                      infraredRegulator, 0.0,
                                                      openpixi::utility::Cutoff::none);
    settings.initialConditions.push_back(std::move(cgcInitialCondition));

    // Diagnostics
    auto projectedEnergyDensity = std::make_shared<openpixi::ProjectedEnergyDensity>(settings);
    settings.diagnostics.push_back(projectedEnergyDensity);

    openpixi::Simulation simulation(settings);
    simulation.applyInitialConditions();
    simulation.executeDiagnostics();

    // Calculate initial total energy
    rational initialTotalEnergy = 0.0;
    for(index iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
    {
      initialTotalEnergy += projectedEnergyDensity->longitudinalElectric[iL];
      initialTotalEnergy += projectedEnergyDensity->transverseElectric[iL];
      initialTotalEnergy += projectedEnergyDensity->longitudinalMagnetic[iL];
      initialTotalEnergy += projectedEnergyDensity->transverseMagnetic[iL];
    }

    for(int i = 0; i < nTimeSteps; ++i)
    {
      simulation.evolve();
    }

    simulation.executeDiagnostics();

    // Calculate final total energy
    rational finalTotalEnergy = 0.0;
    for(index iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
    {
      finalTotalEnergy += projectedEnergyDensity->longitudinalElectric[iL];
      finalTotalEnergy += projectedEnergyDensity->transverseElectric[iL];
      finalTotalEnergy += projectedEnergyDensity->longitudinalMagnetic[iL];
      finalTotalEnergy += projectedEnergyDensity->transverseMagnetic[iL];
    }

    CHECK(approx((finalTotalEnergy - initialTotalEnergy) / initialTotalEnergy) == 0.0);
  }
}