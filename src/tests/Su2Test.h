#pragma once

#include <vector>
#include <complex>
#include <random>

#include "catch.hpp"

#include "../openpixi/CustomTypes.h"
#include "../openpixi/GroupAndAlgebra.h"

#include <Eigen/Dense>
#include <unsupported/Eigen/MatrixFunctions>

// --- Utility functions for comparing results -----------------------------------------------------
void compareAlgebraElements(const openpixi::AlgebraElement actual,
                            const openpixi::AlgebraElement expected);
void compareGroupElements(const openpixi::GroupElement actual,
                          const openpixi::GroupElement expected);

// --- Useful functions for complex tests ----------------------------------------------------------
openpixi::GroupElement
convertEigenMatrixToGroupElement(const Eigen::Matrix2cd matrix,
                                 const std::vector<Eigen::Matrix2cd> pauliMatrices);
Eigen::Matrix2cd
convertGroupElementToEigenMatrix(const openpixi::GroupElement groupElement,
                                 const std::vector<Eigen::Matrix2cd> pauliMatrices);
Eigen::Matrix2cd
convertAlgebraElementToEigenMatrix(const openpixi::AlgebraElement algebraElement,
                                   const std::vector<Eigen::Matrix2cd> pauliMatrices);
openpixi::AlgebraElement
convertEigenMatrixToAlgebraElement(const Eigen::Matrix2cd matrix,
                                   const std::vector<Eigen::Matrix2cd> pauliMatrices);

openpixi::GroupElement createRandomGroupElement(const double amplitude = 10.0);
openpixi::AlgebraElement createRandomAlgebraElement(const double amplitude = 10.0);