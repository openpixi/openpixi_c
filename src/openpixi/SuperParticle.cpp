#include "SuperParticle.h"

#include "Simulation.h"

namespace openpixi
{
std::ostream & openpixi::operator<<(std::ostream & stream, Velocity const & velocity)
{
  switch(velocity)
  {
    default:
    case Velocity::positive:
      stream << "positive";
      break;
    case Velocity::negative:
      stream << "negative";
      break;
  }
  return stream;
}


template<Velocity v>
SuperParticle<v>::SuperParticle(Simulation const & simulation, const index position,
                                const int interCellPosition) : previousPosition(position),
  currentPosition(position), interCellPosition(interCellPosition)
{
  using Direction = Settings::Direction;

  unitFactorQ = simulation.latticeSpacings[Direction::y] *
                simulation.latticeSpacings[Direction::z] /
                (simulation.couplingConstant * simulation.timeStep *
                 simulation.latticeSpacings[Direction::x]);

  const index nTransversalCells = simulation.accumulatedCellCounts[1];
  Q.resize(nTransversalCells);
  for(index i = 0; i < nTransversalCells; ++i)
  {
    Q[i] = AlgebraElement();
  }

  const index nLongitudinalCells = simulation.gridSize[Direction::x];
  nextPosition = (position + static_cast<int>(v) + nLongitudinalCells) % nLongitudinalCells;
}


/// @brief  Moves the super particle depending on its velocity.
///
/// This is done by updating @a interCellPosition. If it has an over- or underflow,
/// @a previousPosition, @a currentPosition and @a nextPosition are updated as well. Periodic
/// boundary conditions are implemented for all positions.
template<Velocity v>
void SuperParticle<v>::updatePosition(Simulation const & simulation)
{
  using Direction = Settings::Direction;

  const int  n = simulation.nParticlesPerCell;
  const auto nLongitudinalCells = simulation.gridSize[Direction::x];

  interCellPosition += static_cast<int>(v);
  // Check if super particle jumps to next cell
  if(interCellPosition > n / 2)
  {
    interCellPosition = -(n - 1) / 2;
    previousPosition  = currentPosition;
    currentPosition   = nextPosition;
    // Periodic boundary conditions
    nextPosition = (nextPosition + 1) % nLongitudinalCells;
  }
  else if(interCellPosition < -(n - 1) / 2)
  {
    interCellPosition = n / 2;
    previousPosition  = currentPosition;
    currentPosition   = nextPosition;
    // Periodic boundary conditions
    nextPosition = (nextPosition + nLongitudinalCells - 1) % nLongitudinalCells;
  }
}


/// @brief  Parallel transports the charges of the super particle from previous to current position.
///
/// The right moving charges are updated like
/// @f[Q_{x+1}(t)=U^\dagger_{x,1}(t-\frac{a_0}{2})Q_x(t-a_0)U_{x,1}(t-\frac{a_0}{2}).@f]
/// Here @f$x@f$ is the @a previousPosition and @f$x+1@f$ is the @a currentPosition.
template<>
void SuperParticle<Velocity::positive>::updateCharges(Simulation const & simulation)
{
  using Direction = Settings::Direction;

  const auto offset = simulation.getIndex({previousPosition, 0, 0});
#pragma omp for schedule(static)
  for(index i = 0; i < Q.size(); ++i)
  {
    Q[i] = Q[i].act(simulation.getLink(i + offset, Direction::x, Orientation::positive,
                                       TimeIndex::previous));
  }
}


/// @brief  Parallel transports the charges of the super particle from previous to current position.
///
/// The left moving charges are updated like
/// @f[Q_{x-1}(t)=U_{x-1,1}(t-\frac{a_0}{2})Q_x(t-a_0)U^\dagger_{x-1,1}(t-\frac{a_0}{2}).@f]
/// Here @f$x@f$ is the @a previousPosition and @f$x-1@f$ is the @a currentPosition.
template<>
void SuperParticle<Velocity::negative>::updateCharges(Simulation const & simulation)
{
  using Direction = Settings::Direction;

  const auto offset = simulation.getIndex({currentPosition, 0, 0});
#pragma omp for schedule(static)
  for(index i = 0; i < Q.size(); ++i)
  {
    Q[i] = Q[i].act(simulation.getLink(i + offset, Direction::x, Orientation::positive,
                                       TimeIndex::previous).dagger());
  }
}


/// @brief  Interpolates the charges of the super particle to the charge density.
///
/// Using NGP interpolation, the whole charge gets mapped to the charge density
/// @f[\rho_x=Q_x,@f]
/// where @f$x@f$ is the @a currentPosition and @f$\rho_x@f$ and @f$Q_x@f$ are dimensionless.
template<Velocity v>
void SuperParticle<v>::interpolateChargeDensity(Simulation & simulation) const
{
  const auto offset = simulation.getIndex({currentPosition, 0, 0});
#pragma omp for schedule(static)
  for(index i = 0; i < Q.size(); ++i)
  {
    simulation.rho[i + offset] += Q[i];
  }
}


/// @brief  The induced current when moving from current to next position is interpolated to the
///         current density.
///
/// For right moving super particles the induced dimensionless current density is
/// @f[j_{x}^{a,1}(t+\frac{a_0}{2})=Q_x^a(t).@f]
/// Here @f$x@f$ is the @a currentPosition. The current and charge densities satisfy the continuity
/// equation for every single particle.
///
/// @see    interpolateChargeDensity()
template<>
void SuperParticle<Velocity::positive>::interpolateCurrentDensity(Simulation & simulation) const
{
  const auto offset = simulation.getIndex({currentPosition, 0, 0});
#pragma omp for schedule(static)
  for(index i = 0; i < Q.size(); ++i)
  {
    simulation.newJ[i + offset] += Q[i];
  }
}


/// @brief  The induced current when moving from current to next position is interpolated to the
///         current density.
///
/// For left moving super particles the induced dimensionless current density is
/// @f[j_{x-1}^{a,1}(t+\frac{a_0}{2})=-U_{x-1,1}(t+\frac{a_0}{2})Q_x^a(t)U_{x-1,1}^\dagger(t+
/// \frac{a_0}{2}).@f]
/// Here @f$x@f$ is the @a currentPosition and @f$x-1@f$ is the @a nextPosition. The current
/// and charge densities satisfy the continuity equation for every single particle.
///
/// @see    interpolateChargeDensity()
template<>
void SuperParticle<Velocity::negative>::interpolateCurrentDensity(Simulation & simulation) const
{
  using Direction = Settings::Direction;

  const auto offset = simulation.getIndex({nextPosition, 0, 0});
#pragma omp for schedule(static)
  for(index i = 0; i < Q.size(); ++i)
  {
    simulation.newJ[i + offset] -= Q[i].act(simulation.getLink(i + offset, Direction::x,
                                                               Orientation::positive,
                                                               TimeIndex::next).dagger());
  }
}


/// @brief  Interpolates the charge and current density of newly spawned super particles.
///
/// @see    interpolateCurrentDensity()
/// @see    interpolateChargeDensity()
template<Velocity v>
void SuperParticle<v>::initializeChargeAndCurrentDensity(Simulation & simulation) const
{
  // Check if the super particle will jump to another cell
  const int n = simulation.nParticlesPerCell;
  if(((v == Velocity::positive) && (interCellPosition == n / 2)) ||
     ((v == Velocity::negative) && (interCellPosition == -(n - 1) / 2)))
  {
    interpolateCurrentDensity(simulation);
  }
  interpolateChargeDensity(simulation);
}


/// @brief  Updates the position and the charges of the super particle as well as the charge and
///         current density.
///
/// @return 1 if the particle hit the boundary, 0 otherwise.
/// @see    updatePosition()
/// @see    updateCharges()
/// @see    interpolateCurrentDensity()
/// @see    interpolateChargeDensity()
template<Velocity v>
int SuperParticle<v>::update(Simulation & simulation)
{
#pragma omp single
  updatePosition(simulation);
  if((simulation.boundaryCondition == BoundaryCondition::fixed) &&
     ((currentPosition == simulation.rightBoundary()) ||
      (currentPosition == simulation.leftBoundary())))
  {
    return 1;
  }
  else
  {
    const int n = simulation.nParticlesPerCell;
    // Parallel transport the charges if the super particle jumped to another cell
    if(((v == Velocity::positive) && (interCellPosition == -(n - 1) / 2)) ||
       ((v == Velocity::negative) && (interCellPosition == n / 2)))
    {
      updateCharges(simulation);
    }
    // Calculate the current density if the super particle will jump to another cell
    else if(((v == Velocity::positive) && (interCellPosition == n / 2)) ||
            ((v == Velocity::negative) && (interCellPosition == -(n - 1) / 2)))
    {
      interpolateCurrentDensity(simulation);
    }

    interpolateChargeDensity(simulation);
    return 0;
  }
}


template class SuperParticle<Velocity::positive>;
template class SuperParticle<Velocity::negative>;
}