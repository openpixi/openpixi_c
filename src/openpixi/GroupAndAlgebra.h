/// @file   GroupAndAlgebra.h
/// @brief  This file chooses the gauge group used for the simulation by including the corresponding
///         header file for openpixi::GroupElement and openpixi::AlgebraElement.

#pragma once

#include "Su2.h"