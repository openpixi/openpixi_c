#pragma once

#include <stddef.h>

#ifndef USE_FLOAT
#define USE_FLOAT 0
#endif

namespace openpixi
{
/// @brief  Used for every floating point variable in the project.
///
/// If `USE_FLOAT` is defined, `rational` is defined as `float`, otherwise it is defined as
/// `double`. This allows to easily switch the floating point precision of the whole project.
#if USE_FLOAT == 1
typedef float  rational;
#else
typedef double rational;
#endif

/// @brief  Used for indexing all arrays, vectors, etc.
typedef unsigned int index;
}