#include "GroupAndAlgebra.h"
#include <cmath>

namespace openpixi
{
constexpr rational normLimit = 1.0e-20;

// --- ALGEBRA -------------------------------------------------------------------------------------
AlgebraElement::AlgebraElement(void) : v1(0.0), v2(0.0), v3(0.0)
{
}


AlgebraElement::AlgebraElement(const rational parameter1, const rational parameter2,
                               const rational parameter3) :
  v1(parameter1), v2(parameter2), v3(parameter3)
{
}


AlgebraElement & AlgebraElement::operator+=(AlgebraElement const & right)
{
  v1 += right.v1;
  v2 += right.v2;
  v3 += right.v3;

  return *this;
}


AlgebraElement & AlgebraElement::operator-=(AlgebraElement const & right)
{
  v1 -= right.v1;
  v2 -= right.v2;
  v3 -= right.v3;

  return *this;
}


AlgebraElement & AlgebraElement::operator*=(const rational right)
{
  v1 *= right;
  v2 *= right;
  v3 *= right;

  return *this;
}


AlgebraElement AlgebraElement::operator-(void)
{
  return AlgebraElement(-v1, -v2, -v3);
}


/// @brief  Returns a component of the algebra element.
///
/// @param[in]  i If 2, returns @a v3, if 1 returns @a v2, otherwise returns @a v1
rational AlgebraElement::getComponent(const index i) const
{
  switch(i)
  {
    default:
    // Fallthrough
    case 0:
      return v1;
      break;
    case 1:
      return v2;
      break;
    case 2:
      return v3;
      break;
  }
}


/// @brief  Sets a component of the algebra element to the given value.
///
/// @param[in]  i     If 2, sets @a v3, if 1 sets @a v2, otherwise sets @a v1
/// @param[in]  value The value, the component gets set to.
void AlgebraElement::setComponent(const index i, const rational value)
{
  switch(i)
  {
    default:
    // Fallthrough
    case 0:
      v1 = value;
      break;
    case 1:
      v2 = value;
      break;
    case 2:
      v3 = value;
      break;
  }
}


/// @brief  Performs the transformation @f$v\rightarrow g^\dagger v g@f$.
///
/// @f$v@f$ is the algebra element.
/// @param[in]  groupElement @f$g@f$
AlgebraElement AlgebraElement::act(GroupElement const & groupElement) const
{
  GroupElement A(0.0, v1 / 2.0, v2 / 2.0, v3 / 2.0);

  A = groupElement.dagger() * A * groupElement;
  return A.project();
}


/// @brief  Exponentiates the algebra element to get the corresponding group element.
///
/// The calculation is done in the following way
/// @f[g_0=\cos\left(\frac{|v|}{2}\right),\quad g_a=\frac{v_a}{|v|}\sin\left(\frac{|v|}{2}\right),
/// \quad a=1,2,3@f] where @f$v_a@f$ are the components of the algebra element and @f$g_0@f$ and
/// @f$g_a@f$ are the components of the group element.
GroupElement AlgebraElement::exp(void) const
{
  rational norm = v1 * v1 + v2 * v2 + v3 * v3;

  if(norm < normLimit)
  {
    // If the algebra element is close to 0 use linearized exponential function
    // exp(v_a * sigma_a / 2) = 1 + v_a * sigma_a / 2
    return GroupElement(1.0, 0.5 * v1, 0.5 * v2, 0.5 * v3);
  }
  else
  {
    // Otherwise, really do the calculation
    norm = std::sqrt(norm) / 2.0;
    rational sineFactor = std::sin(norm) / (norm * 2.0);
    return GroupElement(std::cos(norm), v1 * sineFactor, v2 * sineFactor, v3 * sineFactor);
  }
}


/// @brief  Returns @f$2\mathrm{Tr}(v^2)@f$.
rational AlgebraElement::square(void) const
{
  return v1 * v1 + v2 * v2 + v3 * v3;
}


/// @brief  Returns @f$v_a w^a@f$.
///
/// This is the dot product of two algebra elements with components @f$v_a@f$ and @f$w^a@f$. It is
/// equivalent to @f$2\mathrm{Tr}(v\cdot w)@f$ with @f$v@f$ and @f$w@f$ being algebra elements.
/// @param[in]  right @f$w@f$
rational AlgebraElement::dot(AlgebraElement const & right) const
{
  return v1 * right.v1 + v2 * right.v2 + v3 * right.v3;
}


AlgebraElement operator+(AlgebraElement left, AlgebraElement const & right)
{
  return left += right;
}


AlgebraElement operator-(AlgebraElement left, AlgebraElement const & right)
{
  return left -= right;
}


AlgebraElement operator*(AlgebraElement left, const rational right)
{
  return left *= right;
}


AlgebraElement operator*(const rational left, AlgebraElement right)
{
  return right *= left;
}


// --- GROUP ---------------------------------------------------------------------------------------

GroupElement::GroupElement(void) : g0(1.0), g1(0.0), g2(0.0), g3(0.0)
{
}


GroupElement::GroupElement(const rational parameter0, const rational parameter1,
                           const rational parameter2, const rational parameter3) :
  g0(parameter0), g1(parameter1), g2(parameter2), g3(parameter3)
{
}


GroupElement & GroupElement::operator+=(GroupElement const & right)
{
  g0 += right.g0;
  g1 += right.g1;
  g2 += right.g2;
  g3 += right.g3;

  return *this;
}


GroupElement & GroupElement::operator-=(GroupElement const & right)
{
  g0 -= right.g0;
  g1 -= right.g1;
  g2 -= right.g2;
  g3 -= right.g3;

  return *this;
}


GroupElement & GroupElement::operator*=(const rational right)
{
  g0 *= right;
  g1 *= right;
  g2 *= right;
  g3 *= right;

  return *this;
}


GroupElement & GroupElement::operator*=(GroupElement const & right)
{
  rational h0;
  rational h1;
  rational h2;
  rational h3;

  h0 = g0 * right.g0 - g1 * right.g1 - g2 * right.g2 - g3 * right.g3;
  h1 = g0 * right.g1 + g1 * right.g0 - g2 * right.g3 + g3 * right.g2;
  h2 = g0 * right.g2 + g2 * right.g0 - g3 * right.g1 + g1 * right.g3;
  h3 = g0 * right.g3 + g3 * right.g0 - g1 * right.g2 + g2 * right.g1;

  g0 = h0;
  g1 = h1;
  g2 = h2;
  g3 = h3;

  return *this;
}


GroupElement GroupElement::operator-(void)
{
  return GroupElement(-g0, -g1, -g2, -g3);
}


/// @brief  Returns the hermitian conjugate of the group element.
GroupElement GroupElement::dagger(void) const
{
  return GroupElement(g0, -g1, -g2, -g3);
}


/// @brief  Returns @f$2\mathrm{Im}\mathrm{Tr}(t^a\cdot g)@f$

/// @f$t^a@f$ is the ath generator of the group and @f$g@f$ the group element. This corresponds to
/// the projection of the group element onto the generators of the group. It is also equivalent to
/// calculating the linearized algebra element.
AlgebraElement GroupElement::project(void) const
{
  return AlgebraElement(2.0 * g1, 2.0 * g2, 2.0 * g3);
}


/// @brief  Calculates the logarithm of the group element to get the corresponding algebra element.
///
/// @f[v_a=2 g_a\frac{\arccos(g_0)}{\sqrt{1-g_0^2}},\quad a=1,2,3@f] where @f$v_a@f$ are the
/// components of the algebra element and @f$g_0@f$ and @f$g_a@f$ the components of the group
/// element.
AlgebraElement GroupElement::log(void) const
{
  rational norm = 1.0 - g0 * g0;

  if(norm < normLimit)
  {
    // If the group element is too close to the unit element, return the linearized element of the
    // algebra
    return project();
  }
  else
  {
    // Otherwise, really do the calculation
    norm = std::sqrt(norm);
    rational arccosineFactor = 2.0 * std::acos(g0) / norm;
    return AlgebraElement(g1 * arccosineFactor, g2 * arccosineFactor, g3 * arccosineFactor);
  }
}


GroupElement operator+(GroupElement left, GroupElement const & right)
{
  return left += right;
}


GroupElement operator-(GroupElement left, GroupElement const & right)
{
  return left -= right;
}


GroupElement operator*(GroupElement left, const rational right)
{
  return left *= right;
}


GroupElement operator*(const rational left, GroupElement right)
{
  return right *= left;
}


GroupElement operator*(GroupElement left, GroupElement const & right)
{
  return left *= right;
}
}