#pragma once

#include <array>
#include <vector>
#include <string>
#include <stddef.h>
#include <netcdf.h>

#include "CustomTypes.h"

namespace openpixi
{
class Settings;

/// @brief  A class for handling a NetCDF file to save the diagnositc data.
class DataFile
{
public:
  static constexpr int noError = NC_NOERR;

  DataFile(std::string const & fileName, Settings const & settings, int & error);
  ~DataFile();

  template<std::size_t N>
  int addVariable(std::string const & name, std::array<int, N> const & dimensionIds,
                  int & variableId) const;
  template<std::size_t N>
  int writeVariable(const int variableId, std::array<std::size_t, N> const & start,
                    std::array<std::size_t, N> const & count,
                    std::vector<rational> const & data) const;
  int writeTimeVariable(Settings const & settings, const int currentTimeStep) const;

  int tDimensionId(void) const;
  int xDimensionId(void) const;
  int yDimensionId(void) const;
  int zDimensionId(void) const;

private:
#if USE_FLOAT == 1
  static constexpr int ncRational_ = NC_FLOAT;
#else
  static constexpr int ncRational_ = NC_DOUBLE;
#endif

  int fileId_;
  int tDimensionId_;
  int xDimensionId_;
  int yDimensionId_;
  int zDimensionId_;
  int tId_;
  int xId_;
  int yId_;
  int zId_;

  std::string createFileNamePrefix(void) const;
  int defineDimensions(Settings const & settings);
  int defineCoordinateVariables(void);
  int defineAttributes(Settings const & settings) const;
  int fillSpatialCoordinateVariables(Settings const & settings) const;
  static void printErrorMessage(int error);
};


/// @brief  Adds a variable of type openpixi::rational to the NetCDF file.
///
/// @tparam     N             Number of dimensions of the variable.
/// @param[in]  name          Name of the variable.
/// @param[in]  dimensionIds  An array containing the ids of the dimensions of the variable.
/// @param[out] variableId    Id of the added variable.
/// @return     A NetCDF error code if something went wrong, or @a noError otherwise.
template<std::size_t N>
int DataFile::addVariable(std::string const & name, std::array<int, N> const & dimensionIds,
                          int & variableId) const
{
  int error = nc_def_var(fileId_, name.data(), ncRational_, N, dimensionIds.data(), &variableId);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
  }

  return error;
}


/// @brief  Writes the given data to the NetCDF variable.
///
/// @tparam     N           Number of dimensions of the variable.
/// @param[in]  variableId  Id of the NetCDF variable to write to.
/// @param[in]  start       Array containing the indexes of the variable, where the write starts.
/// @param[in]  count       Array containing the count of how many values will be written in each
///                         dimension of the variable.
/// @param[in]  data        The data that will be written to the NetCDF variable.
template<std::size_t N>
int DataFile::writeVariable(const int variableId, std::array<std::size_t, N> const & start,
                            std::array<std::size_t, N> const & count,
                            std::vector<rational> const & data) const
{
#if USE_FLOAT == 1
  int error = nc_put_vara_float(fileId_, variableId, start.data(), count.data(), data.data());
#else
  int error = nc_put_vara_double(fileId_, variableId, start.data(), count.data(), data.data());
#endif
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
  }

  return error;
}
}