#pragma once

#include "CustomTypes.h"

namespace openpixi
{
class GroupElement;

/// @brief  A class representing an su(2) algebra element in the fundamental representation
///
/// An algebra element @f$v@f$ is parametrized by
/// @f[v=\frac{1}{2} v_a\sigma^a,\quad a=1,2,3@f]
/// where @f$\sigma^a@f$ are the Pauli matrices and @f$v_a@f$ correspond to the member variables
/// @a v1, @a v2 and @a v3.
class AlgebraElement
{
public:
  rational v1;
  rational v2;
  rational v3;

  AlgebraElement(void);
  AlgebraElement(const rational parameter1, const rational parameter2, const rational parameter3);
  AlgebraElement & operator+=(AlgebraElement const & right);
  AlgebraElement & operator-=(AlgebraElement const & right);
  AlgebraElement & operator*=(const rational right);
  AlgebraElement operator-(void);

  rational getComponent(const index i) const;
  void setComponent(const index i, const rational value);
  AlgebraElement act(GroupElement const & groupElement) const;
  GroupElement exp(void) const;
  rational square(void) const;
  rational dot(AlgebraElement const & right) const;
};

AlgebraElement operator+(AlgebraElement left, AlgebraElement const & right);
AlgebraElement operator-(AlgebraElement left, AlgebraElement const & right);
AlgebraElement operator*(AlgebraElement left, const rational right);
AlgebraElement operator*(const rational left, AlgebraElement right);


/// @brief  A class representing an SU(2) group element in the fundamental representation.
///
/// A group element @f$g@f$ is parametrized by
/// @f[g=g_0\mathbf{1}+\mathrm{i} g_a\sigma^a,\quad a=1,2,3@f]
/// where @f$\mathbf{1}@f$ is the unit matrix, @f$\mathrm{i}@f$ the imaginary unit, @f$\sigma^a@f$
/// are the Pauli matrices and @f$g_0@f$ and @f$g_a@f$ correspond to the member variables @a g0,
/// @a g1, @a g2, and @a g3.
class GroupElement
{
public:
  static constexpr int   groupDimension = 2;      ///< = N for SU(N)
  static constexpr index nGenerators    = groupDimension * groupDimension - 1;

  rational g0;
  rational g1;
  rational g2;
  rational g3;

  GroupElement(void);
  GroupElement(const rational parameter0, const rational parameter1, const rational parameter2,
               const rational parameter3);

  GroupElement & operator+=(GroupElement const & right);
  GroupElement & operator-=(GroupElement const & right);
  GroupElement & operator*=(const rational right);
  GroupElement & operator*=(GroupElement const & right);
  GroupElement operator-(void);

  GroupElement dagger(void) const;
  AlgebraElement project(void) const;
  AlgebraElement log(void) const;
};

GroupElement operator+(GroupElement left, GroupElement const & right);
GroupElement operator-(GroupElement left, GroupElement const & right);
GroupElement operator*(GroupElement left, const rational right);
GroupElement operator*(const rational left, GroupElement right);
GroupElement operator*(GroupElement left, GroupElement const & right);
}