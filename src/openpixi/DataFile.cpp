#include "DataFile.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <chrono>

#include "Settings.h"
#include "Utility.h"
#include "diagnostics/Diagnostics.h"
#include "initial/InitialCondition.h"

namespace openpixi
{
/// @brief  Creates a NetCDF file and sets all dimensions, attributes and variables.
///
/// The NetCDF file is created with the given file name minus anything past the first '.' in the
/// string plus a prefix containing the date and time. The file extension is set to ".nc".
/// Dimensions and Variables for time, x-, y- and z-coordinate are defined. The time is an unlimited
/// dimension. All necessary parameters needed to reproduce the simulation are added as global
/// attributes. Variables are also added for each diagnostic in Settings::diagnostics. The define
/// mode is ended and the variables for x,y and z are filled with their values in physical units.
///
/// @param[in]  fileName  Name of the NetCDF file to be created. Any file extension will be removed.
/// @param[in]  settings  Settings of the current simulation.
/// @param[out] error     An error code returned by the NetCDF functions. If it's not @a noError,
///                       something went wrong during construction and the object can't be used
///                       properly.
///
/// @see    Diagnostics::createDataFileVariables()
DataFile::DataFile(std::string const & fileName, Settings const & settings, int & error)
{
  std::string name = createFileNamePrefix();
  // Remove a possible file extension in the given name by removing anything past the first '.' in
  // the string
  name.append(fileName.substr(0, fileName.find(".")));
  name.append(".nc");
  error = nc_create(name.data(), NC_CLOBBER, &fileId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return;
  }

  // Define dimensions for t,x,y,z
  error = defineDimensions(settings);
  if(error != NC_NOERR)
  {
    return;
  }

  // Define variables for coordinates t,x,y,z
  error = defineCoordinateVariables();
  if(error != NC_NOERR)
  {
    return;
  }

  // Define attributes containing all necessary information to reproduce the simulation
  error = defineAttributes(settings);
  if(error != NC_NOERR)
  {
    return;
  }

  // Define variables for all diagnostics that are written to the data file
  for(auto const & diagnostic : settings.diagnostics)
  {
    diagnostic->createDataFileVariables(*this);
  }

  // End define mode
  error = nc_enddef(fileId_);
  if(error != NC_NOERR)
  {
    return;
  }

  // Fill x,y,z coordinate variables with data
  error = fillSpatialCoordinateVariables(settings);
  if(error != NC_NOERR)
  {
    return;
  }
}


/// @brief  Destructor closes the NetCDF file.
DataFile::~DataFile()
{
  int error = nc_close(fileId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
  }
}


/// @brief  Writes the current time in physical units to the NetCDF file.
int DataFile::writeTimeVariable(Settings const & settings, const int currentTimeStep) const
{
  std::array<std::size_t, 1> start = {currentTimeStep - settings.t0};
  std::array<std::size_t, 1> count = {1};
  std::vector<rational> time = {currentTimeStep * settings.timeStep};
  int error = writeVariable(tId_, start, count, time);
  return error;
}


/// @brief  Returns the id of the time dimension.
int DataFile::tDimensionId(void) const
{
  return tDimensionId_;
}


/// @brief  Returns the id of the x dimension.
int DataFile::xDimensionId(void) const
{
  return xDimensionId_;
}


/// @brief  Returns the id of the y dimension.
int DataFile::yDimensionId(void) const
{
  return yDimensionId_;
}


/// @brief  Returns the id of the z dimension.
int DataFile::zDimensionId(void) const
{
  return zDimensionId_;
}


/// @brief  Returns the current date and time as a string in the format "YYYYMMDD_HHMM_".
std::string DataFile::createFileNamePrefix(void) const
{
  auto time = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
  auto timeInfo = utility::localTime(time);

  std::stringstream suffix;
  suffix << timeInfo.tm_year + 1900 <<
    std::setfill('0') << std::setw(2) << timeInfo.tm_mon + 1 <<
    std::setfill('0') << std::setw(2) << timeInfo.tm_mday << "_" <<
    std::setfill('0') << std::setw(2) << timeInfo.tm_hour <<
    std::setfill('0') << std::setw(2) << timeInfo.tm_min << "_";

  return suffix.str();
}


/// @brief  Defines dimensions for t,x,y,z. The time dimension is unlimited.
///
/// @param[in]  settings  Settings of the current simulation.
/// @return     A NetCDF error code if something went wrong, or @a noError otherwise.
int DataFile::defineDimensions(Settings const & settings)
{
  using Direction = Settings::Direction;

  int error = nc_def_dim(fileId_, "t", NC_UNLIMITED, &tDimensionId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_def_dim(fileId_, "x", settings.gridSize[Direction::x], &xDimensionId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_def_dim(fileId_, "y", settings.gridSize[Direction::y], &yDimensionId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_def_dim(fileId_, "z", settings.gridSize[Direction::z], &zDimensionId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  return error;
}


/// @brief  Defines variables for t,x,y,z of type openpixi::rational.
///
/// @return A NetCDF error code if something went wrong, or @a noError otherwise.
int DataFile::defineCoordinateVariables(void)
{
  int error = nc_def_var(fileId_, "t", ncRational_, 1, &tDimensionId_, &tId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_def_var(fileId_, "x", ncRational_, 1, &xDimensionId_, &xId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_def_var(fileId_, "y", ncRational_, 1, &yDimensionId_, &yId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_def_var(fileId_, "z", ncRational_, 1, &zDimensionId_, &zId_);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  return error;
}


/// @brief  Defines attributes containing all parameters to reproduce the simulation.
///
/// A "units" attribute is also added to the time variable according to the COARDS convention. This
/// allows e.g. ParaView to recognize this variable as the time variable.
///
/// @param[in]  settings  Settings of the current simulation.
/// @return     A NetCDF error code if something went wrong, or @a noError otherwise.
int DataFile::defineAttributes(Settings const & settings) const
{
  using Direction = Settings::Direction;

  std::string title = "openpixi diagnostic data";
  int error = nc_put_att_text(fileId_, NC_GLOBAL, "title", title.size(), title.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  std::string conventions = "COARDS";
  error = nc_put_att_text(fileId_, NC_GLOBAL, "Conventions", conventions.size(),
                          conventions.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  std::stringstream stream;
  stream << "(" << settings.latticeSpacings[Direction::x] << "," <<
    settings.latticeSpacings[Direction::y] << "," <<
    settings.latticeSpacings[Direction::z] << ")";
  std::string latticeSpacings = stream.str();
  error = nc_put_att_text(fileId_, NC_GLOBAL, "latticeSpacings", latticeSpacings.size(),
                          latticeSpacings.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  std::string timeStep = std::to_string(settings.timeStep);
  error = nc_put_att_text(fileId_, NC_GLOBAL, "timeStep", timeStep.size(), timeStep.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  std::string couplingConstant = std::to_string(settings.couplingConstant);
  error = nc_put_att_text(fileId_, NC_GLOBAL, "couplingConstant", couplingConstant.size(),
                          couplingConstant.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  for(index i = 0; i < settings.initialConditions.size(); ++i)
  {
    auto description = settings.initialConditions[i]->getDescription();
    std::stringstream name;
    name << "InitialCondition" << i;
    error = nc_put_att_text(fileId_, NC_GLOBAL, name.str().data(), description.size(),
                            description.data());
    if(error != NC_NOERR)
    {
      printErrorMessage(error);
      return error;
    }
  }

  std::string timeUnit = "fm/c since 2000-01-01 00:00:00";
  error = nc_put_att_text(fileId_, tId_, "units", timeUnit.size(), timeUnit.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  std::string spaceUnit = "fm";
  error = nc_put_att_text(fileId_, xId_, "units", spaceUnit.size(), spaceUnit.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_put_att_text(fileId_, yId_, "units", spaceUnit.size(), spaceUnit.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  error = nc_put_att_text(fileId_, zId_, "units", spaceUnit.size(), spaceUnit.data());
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  return error;
}


/// @brief  Fills the x,y,z variables with physical values.
///
/// @param[in]  settings  Settings of the current simulation.
/// @return     A NetCDF error code if something went wrong, or @a noError otherwise.
int DataFile::fillSpatialCoordinateVariables(Settings const & settings) const
{
  using Direction = Settings::Direction;

  std::vector<rational> x(settings.gridSize[Direction::x]);
  for(int i = 0; i < x.size(); ++i)
  {
    x[i] = i * settings.latticeSpacings[Direction::x];
  }
  std::vector<rational> y(settings.gridSize[Direction::y]);
  for(int i = 0; i < y.size(); ++i)
  {
    y[i] = i * settings.latticeSpacings[Direction::y];
  }
  std::vector<rational> z(settings.gridSize[Direction::z]);
  for(int i = 0; i < z.size(); ++i)
  {
    z[i] = i * settings.latticeSpacings[Direction::z];
  }

  // Write coordinate variables to file
  std::array<std::size_t, 1> start = {0};
  std::array<std::size_t, 1> count = {settings.gridSize[Direction::x]};
  int error = writeVariable(xId_, start, count, x);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  count = {settings.gridSize[Direction::y]};
  error = writeVariable(yId_, start, count, y);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  count = {settings.gridSize[Direction::z]};
  error = writeVariable(zId_, start, count, z);
  if(error != NC_NOERR)
  {
    printErrorMessage(error);
    return error;
  }

  return error;
}


/// @brief  Prints the corresponding message of the given NetCDF error.
void DataFile::printErrorMessage(int error)
{
  std::cout << "NetCDF error: " << nc_strerror(error) << std::endl;
}
}