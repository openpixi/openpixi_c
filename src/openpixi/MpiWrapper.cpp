#include "MpiWrapper.h"

#include <mpi.h>

namespace mpiwrapper
{
Mpi::Mpi()
{
  MPI_Init(nullptr, nullptr);
  MPI_Comm_size(MPI_COMM_WORLD, &worldSize_);
  MPI_Comm_rank(MPI_COMM_WORLD, &worldRank_);
}


Mpi::~Mpi()
{
  MPI::Finalize();
}


/// @brief  Returns a reference to a static mpiwrapper::Mpi object.
Mpi & Mpi::getInstance(void)
{
  static Mpi instance;
  return instance;
}


/// @brief  Writes the rank of the process in, and the size of the world communicator to the given
///         stream.
void Mpi::writeWorldInfo(std::ostream & outputStream) const
{
  outputStream << "I am process " << worldRank_ + 1 << " of " << worldSize_ << "." << std::endl;
}


int mpiwrapper::Mpi::worldSize(void) const
{
  return worldSize_;
}


int mpiwrapper::Mpi::worldRank(void) const
{
  return worldRank_;
}
}