#pragma once

#include <array>
#include <vector>
#include <memory>

#include "CustomTypes.h"

namespace openpixi
{
class InitialCondition;
class Diagnostics;

enum class BoundaryCondition
{
  periodic,
  fixed
};

/// @brief  A class containing all necessary settings for a lattice simulation.
class Settings
{
public:
  static constexpr index nDimensions = 3;
  static constexpr index nPlaquettes = nDimensions * (nDimensions - 1) / 2;

  /// @brief  This is used throughout the whole project for the definition of directions, the
  ///         components of spacial vectors, etc.
  enum Direction
  {
    x = 0,    ///< @f$x@f$ = 0
    y,        ///< @f$y@f$ = 1
    z         ///< @f$z@f$ = 2
  };

  const std::array<index, nDimensions> gridSize;              ///< @f$(n_1, n_2, n_3)@f$
  const std::array<rational, nDimensions> latticeSpacings;    ///< @f$(a_1, a_2, a_3)@f$
  const int t0;                                               ///< start time of the simulation
  const int nTimeSteps;
  const rational timeStep;                                    ///< @f$a_0@f$
  const rational couplingConstant;                            ///< @f$g@f$
  const int nParticlesPerCell;

  /// @brief  Determines the active simulation grid
  ///
  /// The active simulation grid is the part of the grid which is updated by Simulation::evolve()
  /// and inside which the diagnostic values are calculated. If @a boundaryCondition is
  /// BoundaryCondition::periodic, the active grid equals the whole grid since Simulation::shift()
  /// already implements periodic boundary conditions. If @a boundaryCondition is
  /// BoundaryCondition::fixed, the active grid is the whole grid minus the longitudinal boundaries
  /// i.e. the left- and right-most transversal planes. Therefore these boundaries are held fixed.
  const BoundaryCondition boundaryCondition;

  /// @brief  A vector containing the initial conditions applied at the start of a simulation.
  ///
  /// @see    Simulation::applyInitialConditions()
  std::vector<std::shared_ptr<InitialCondition>> initialConditions;

  /// @brief  A vector containing the diagnostics executed during each simulation step.
  ///
  /// @see    Simulation::executeDiagnostics()
  std::vector<std::shared_ptr<Diagnostics>> diagnostics;

  Settings(const std::array<index, nDimensions> gridSize,
           const std::array<rational, nDimensions> latticeSpacings, const int t0,
           const int nTimeSteps, const rational timeStep, const rational couplingConstant,
           const BoundaryCondition boundaryCondition = BoundaryCondition::periodic);

  index leftBoundary(void) const;
  index rightBoundary(void) const;
  index leftIndexBoundary(void) const;
  index rightIndexBoundary(void) const;

private:
  index leftBoundary_;
  index rightBoundary_;
  index leftIndexBoundary_;
  index rightIndexBoundary_;
};
}