#pragma once

#include <vector>

#include "../CustomTypes.h"
#include "Diagnostics.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the square of the linearized algebra elements
///         corresponding to the old gauge links at every grid point.
class OldGaugeLinks : public Diagnostics
{
public:
  std::vector<rational> oldGaugeLinksX;
  std::vector<rational> oldGaugeLinksY;
  std::vector<rational> oldGaugeLinksZ;

  OldGaugeLinks(Settings const & settings, const bool shouldWriteToFile = false);
  ~OldGaugeLinks() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int oldGaugeLinksXId_;
  int oldGaugeLinksYId_;
  int oldGaugeLinksZId_;
};
}