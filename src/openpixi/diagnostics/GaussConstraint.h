#pragma once

#include <vector>
#include <memory>

#include "../CustomTypes.h"
#include "../GroupAndAlgebra.h"
#include "Diagnostics.h"
#include "DivE.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the Gauss constraint at every grid point.
///
/// The following equation is called Gauss constraint
/// @f[\sum_{i=1}^{3}\frac{E_{x,i}^a-(U_{x-i,i}^\dagger\mathcal{E}_{x-i,i}U_{x-i,i})^a}{a_i}-
///    \rho_x^a=0.@f]
/// The functions or (if available) the already calculated values in @ref DivE are used to
/// compute the left hand side at every grid point in physical units.
class GaussConstraint : public Diagnostics
{
public:
  std::vector<AlgebraElement> gaussConstraint;
  std::shared_ptr<DivE> divE;

  GaussConstraint(Settings const & settings, std::shared_ptr<DivE> divE,
                  const bool shouldWriteToFile = false);
  GaussConstraint(Settings const & settings, const bool shouldWriteToFile = false);
  ~GaussConstraint() = default;

  static AlgebraElement calculateGaussConstraint(Simulation const & simulation,
                                                 const index positionIndex);
  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int gaussConstraintId_;
};
}