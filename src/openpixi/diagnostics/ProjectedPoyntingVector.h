#pragma once

#include <array>
#include <vector>
#include <memory>

#include "../CustomTypes.h"
#include "../Settings.h"
#include "Diagnostics.h"

namespace openpixi
{
class Simulation;
class DataFile;
class PoyntingVector;

class ProjectedPoyntingVector : public Diagnostics
{
public:
  std::array<std::vector<rational>, Settings::nDimensions> projectedPoyntingVector;
  std::shared_ptr<PoyntingVector> poyntingVector;

  ProjectedPoyntingVector(Settings const & settings, std::shared_ptr<PoyntingVector> poyntingVector,
                          const bool shouldWriteToFile = false);
  ProjectedPoyntingVector(Settings const & settings, const bool shouldWriteToFile = false);
  ~ProjectedPoyntingVector() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int projectedPoyntingVectorXId_;
  int projectedPoyntingVectorYId_;
  int projectedPoyntingVectorZId_;
};
}