#pragma once

#include <array>
#include <vector>

#include "../CustomTypes.h"

#include "../GroupAndAlgebra.h"
#include "../Settings.h"

#include "Diagnostics.h"

namespace openpixi
{
class Simulation;
class DataFile;

/// @brief  A class providing multiple ways to calculate the Poynting vector at every grid point.
///
/// It is calculated in physical units.
class PoyntingVector : public Diagnostics
{
public:
  /// @brief  Determines how the Poynting Vector is calculated.
  ///
  /// Either of the following two equations can be used:
  /// @f[S_x^i=\frac{2}{g}\sum_{j\ne i}\frac{\text{unitFactorE}_j}{a_ia_j}\mathrm{Im}
  ///    \mathrm{Tr}(E_{x}^{a,j}t_aU_{x,j-i})@f]
  /// @f[S_{x,i}=\text{unitFactorE}_j\ \text{unitFactorB}_k\ \epsilon_{ijk}\mathcal{E}_x^j
  ///    \mathcal{B}_x^k@f]
  ///
  /// poyntingVectorCalculationMode | description
  /// :---------------------------: | :----------
  ///               1               | equation 1
  ///               2               | equation 2
  static constexpr int poyntingVectorCalculationMode = 1;

  std::array<std::vector<rational>, Settings::nDimensions> poyntingVector;

  PoyntingVector(Settings const & settings, const bool shouldWriteToFile = false);
  ~PoyntingVector();

  static rational calculatePoyntingVector(Simulation const & simulation, const index positionIndex,
                                          const index direction);
  static rational calculatePoyntingVector(Simulation const & simulation,
                                          const std::array<index, Settings::nDimensions> position,
                                          const index direction);
  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int poyntingVectorXId_;
  int poyntingVectorYId_;
  int poyntingVectorZId_;
};
}