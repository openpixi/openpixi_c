#include "ProjectedGaussConstraint.h"

#include <array>
#include <stddef.h>
#include <utility>

#include "../GroupAndAlgebra.h"
#include "../Settings.h"
#include "../Simulation.h"
#include "GaussConstraint.h"
#include "../DataFile.h"

namespace openpixi
{
ProjectedGaussConstraint::ProjectedGaussConstraint(Settings const & settings,
                                                   std::shared_ptr<GaussConstraint> gaussConstraint,
                                                   const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile), gaussConstraint(std::move(gaussConstraint))
{
  projectedGaussConstraint.resize(settings.gridSize[Settings::Direction::x]);
  totalGaussConstraint = 0.0;
}


ProjectedGaussConstraint::ProjectedGaussConstraint(Settings const & settings,
                                                   const bool shouldWriteToFile) :
  ProjectedGaussConstraint(settings, nullptr, shouldWriteToFile)
{
}


/// @brief  Calculates the square of the projected and total Gauss constraint in physical units.
///
/// This is done by integrating over transversal planes or the whole active simulation grid
/// respectively.
/// @see    Settings::boundaryCondition
void ProjectedGaussConstraint::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;
  const auto nTransversalCells = simulation.accumulatedCellCounts[1];
  const auto areaElement   = simulation.latticeSpacings[Direction::y] *
                             simulation.latticeSpacings[Direction::z];
  const auto volumeElement = areaElement * simulation.latticeSpacings[Direction::x];
  static AlgebraElement sum;

#pragma omp single
  sum = AlgebraElement();

  AlgebraElement localSum;
#pragma omp for schedule(static) nowait
  for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
  {
    AlgebraElement result;
    auto positionOffset = simulation.getIndex({iL, 0, 0});
    if(gaussConstraint == nullptr)
    {
      for(auto i = positionOffset; i < positionOffset + nTransversalCells; ++i)
      {
        result += GaussConstraint::calculateGaussConstraint(simulation, i);
      }
    }
    else
    {
      for(auto i = positionOffset; i < positionOffset + nTransversalCells; ++i)
      {
        result += gaussConstraint->gaussConstraint[i];
      }
    }

    // Multiply by the area element to really calculate the integral and not just the sum
    projectedGaussConstraint[iL] = (result * areaElement).square();
    localSum += result;
  }
#pragma omp critical
  sum += localSum;

  // Multiply by the volume element to really calculate the integral and not just the sum
#pragma omp barrier
#pragma omp single nowait
  totalGaussConstraint = (sum * volumeElement).square();
}


/// @brief  Creates variables for the projected gauss constraint in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a projectedGaussConstraint and
/// @a totalGaussConstraint is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedGaussConstraint::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 2> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId()};
    int error = dataFile.addVariable("gaussConstraint_proj", dimensionIds,
                                     projectedGaussConstraintId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable<1>("gaussConstraint_total", {dataFile.tDimensionId()},
                                    totalGaussConstraintId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the projected Gauss constraint to the data file.
///
/// If @a shouldWriteToFile is true, then @a projectedGaussConstraint and @a totalGaussConstraint
/// are written to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedGaussConstraint::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                              const int currentTimeIndex) const
{
  if(shouldWriteToFile)
  {
    const index n = projectedGaussConstraint.size();
    std::array<std::size_t, 2> start = {currentTimeIndex, 0};
    std::array<std::size_t, 2> count = {1, n};
    int error = dataFile.writeVariable(projectedGaussConstraintId_, start, count,
                                       projectedGaussConstraint);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable<1>(totalGaussConstraintId_, {currentTimeIndex}, {1},
                                      {totalGaussConstraint});
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}