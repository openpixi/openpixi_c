#include "OldGaugeLinks.h"

#include <array>
#include <stddef.h>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
OldGaugeLinks::OldGaugeLinks(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;
  const auto nCells = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                      settings.gridSize[Direction::z];

  oldGaugeLinksX.resize(nCells);
  oldGaugeLinksY.resize(nCells);
  oldGaugeLinksZ.resize(nCells);
}


/// @brief  Calculates the square of linearized algebra elements corresponding to the old gauge
///         links at every grid point.
///
/// This means that GroupElement::project() and AlgebraElement::square() are applied to all elements
/// of Simulation::oldU.
void OldGaugeLinks::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;

  // This is over the whole grid, not only the active part
#pragma omp for schedule(static) nowait
  for(index i = 0; i < simulation.accumulatedCellCounts[0]; ++i)
  {
    oldGaugeLinksX[i] = simulation.oldU[i][Direction::x].project().square();
    oldGaugeLinksY[i] = simulation.oldU[i][Direction::y].project().square();
    oldGaugeLinksZ[i] = simulation.oldU[i][Direction::z].project().square();
  }
}


/// @brief  Creates variables for the square of the old gauge links in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a oldGaugeLinksX, @a oldGaugeLinksY and
/// @a oldGaugeLinksZ is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int OldGaugeLinks::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("oldGaugeLinksX", dimensionIds, oldGaugeLinksXId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("oldGaugeLinksY", dimensionIds, oldGaugeLinksYId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("oldGaugeLinksZ", dimensionIds, oldGaugeLinksZId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the square of the old gauge links to the data file.
///
/// If @a shouldWriteToFile is true, then @a oldGaugeLinksX, @a oldGaugeLinksY and @a oldGaugeLinksZ
/// are written to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int OldGaugeLinks::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(oldGaugeLinksXId_, start, count, oldGaugeLinksX);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(oldGaugeLinksYId_, start, count, oldGaugeLinksY);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(oldGaugeLinksZId_, start, count, oldGaugeLinksZ);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}