#pragma once

#include <vector>
#include <memory>

#include "../CustomTypes.h"

#include "Diagnostics.h"

#define USE_LEFT_DERIVATION 0

namespace openpixi
{
class Settings;
class Simulation;
class PoyntingVector;
class DataFile;

/// @brief  A class providing a way to calculate the divergence of the Poynting vector integrated
///         over transversal planes.
///
/// It uses the values provided by PoyntingVector to calculate its divergence in physical units.
/// Due to the periodic boundary conditions, the transversal derivatives vanishes when summing over
/// transversal planes. Therefore only the longitudinal part is calculated. The longitudinal
/// direction is @f$x@f$.
/// @see    Settings::Direction
class ProjectedDivS : public Diagnostics
{
public:
  std::vector<rational> projectedDivS;

  std::shared_ptr<PoyntingVector> poyntingVector;

  ProjectedDivS(Settings const & settings, std::shared_ptr<PoyntingVector> newPoyntingVector,
                const bool shouldWriteToFile = false);
  ~ProjectedDivS();

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int projectedDivSId_;
};
}