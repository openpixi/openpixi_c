#pragma once

#include <vector>
#include <memory>

#include "../CustomTypes.h"

#include "../Settings.h"
#include "Diagnostics.h"

namespace openpixi
{
class Simulation;
class DataFile;
class EnergyDensity;

/// @brief  A class providing a way to calculate the energy density integrated over transversal
///         planes.
///
/// It uses the functions or (if available) the already calculated values in @ref EnergyDensity to
/// compute the projected energy density in physical units. The transverse and longitudinal as wells
/// as the electric and magnetic part are calculated and stored separately. The longitudinal
/// direction is @f$x@f$. Additionally the total energy of the active simulation grid is calculated.
/// @see    Settings::Direction
class ProjectedEnergyDensity : public Diagnostics
{
public:
  std::vector<rational> transverseElectric;
  std::vector<rational> longitudinalElectric;
  std::vector<rational> transverseMagnetic;
  std::vector<rational> longitudinalMagnetic;
  rational totalEnergy;

  std::shared_ptr<EnergyDensity> energyDensity;

  ProjectedEnergyDensity(Settings const & settings, std::shared_ptr<EnergyDensity> energyDensity,
                         const bool shouldWriteToFile = false);
  ProjectedEnergyDensity(Settings const & settings, const bool shouldWriteToFile = false);
  ~ProjectedEnergyDensity() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int projectedEnergyDensityId_;
  int transverseElectricId_;
  int longitudinalElectricId_;
  int transverseMagneticId_;
  int longitudinalMagneticId_;
  int totalEnergyId_;
};
}