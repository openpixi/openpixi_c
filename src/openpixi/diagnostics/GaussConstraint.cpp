#include "GaussConstraint.h"

#include <array>
#include <stddef.h>
#include <utility>
#include <omp.h>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
openpixi::GaussConstraint::GaussConstraint(Settings const & settings, std::shared_ptr<DivE> divE,
                                           const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile), divE(std::move(divE))
{
  using Direction = Settings::Direction;

  const auto n = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                 settings.gridSize[Direction::z];
  gaussConstraint.resize(n, AlgebraElement());
}


GaussConstraint::GaussConstraint(Settings const & settings, const bool shouldWriteToFile) :
  GaussConstraint(settings, nullptr, shouldWriteToFile)
{
}


/// @brief  Calculates the Gauss constraint, at the given position index in physical units.
AlgebraElement GaussConstraint::calculateGaussConstraint(Simulation const & simulation,
                                                         const index positionIndex)
{
  auto result = DivE::calculateDivE(simulation, positionIndex);
  result -= simulation.rho[positionIndex] * simulation.unitFactorRho;
  return result;
}


/// @brief  Calculates the Gauss constraint at ever point in the active simulation grid in physical
///         units.
///
/// @see    Settings::boundaryCondition
void GaussConstraint::execute(Simulation const & simulation)
{
  if(divE == nullptr)
  {
#pragma omp for schedule(static) nowait
    for(auto i = simulation.leftIndexBoundary(); i < simulation.rightIndexBoundary(); ++i)
    {
      gaussConstraint[i] = calculateGaussConstraint(simulation, i);
    }
  }
  else
  {
#pragma omp for schedule(static) nowait
    for(auto i = simulation.leftIndexBoundary(); i < simulation.rightIndexBoundary(); ++i)
    {
      gaussConstraint[i] = divE->divE[i] - simulation.rho[i] * simulation.unitFactorRho;
    }
  }
}


/// @brief  Creates a variable for the Gauss constraint in the given data file.
///
/// It is only created if @a shouldWriteToFile is true.
///
/// @param[in]  dataFile  The file in which the variable is created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int GaussConstraint::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("gaussConstraint", dimensionIds, gaussConstraintId_);
    return error;
  }

  return DataFile::noError;
}


/// @brief  Writes the square of the Gauss constraint to the data file.
///
/// It is only written if @a shouldWriteToFile is true.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int GaussConstraint::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                     const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::vector<rational> squaredGaussConstraint(gaussConstraint.size());
    for(index i = 0; i < gaussConstraint.size(); ++i)
    {
      squaredGaussConstraint[i] = gaussConstraint[i].square();
    }

    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(gaussConstraintId_, start, count, squaredGaussConstraint);
    return error;
  }

  return DataFile::noError;
}
}