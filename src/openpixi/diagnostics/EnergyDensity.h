#pragma once

#include <vector>
#include <array>

#include "Diagnostics.h"
#include "../CustomTypes.h"
#include "../Settings.h"

namespace openpixi
{
class Simulation;
class DataFile;

/// @brief  A class providing multiple ways to calculate the energy density at every grid point.
///
/// It is calculated and stored in physical units.
class EnergyDensity : public Diagnostics
{
public:
  /// @brief  Determines how the energy density is calculated.
  ///
  /// The electric part is always calculated in the same way:
  /// @f[\mathcal{H}_{x,\text{el}}=\sum_{i=1}^3\frac{\text{unitFactorE}_i^2}{2}\mathcal{E}_{x,i}
  ///    \mathcal{E}_{x,i}.@f]
  /// For the magnetic part there are two options, both of which use a time average to get the
  /// magnetic field or plaquette at the time of the electric field:
  /// @f[\mathcal{H}_{x,\text{mag}}=\sum_{i=1}^3\frac{\text{unitFactorB}_i^2}{2}\mathcal{B}_{x,i}
  ///    \mathcal{B}_{x,i}@f]
  /// @f[\mathcal{H}_{x,\text{mag}}=\sum_{i=1}^3\frac{1}{g^2a_i^2}\sum_{j\ne i}\frac{1}{a_j^2}
  ///    \mathrm{Re}\mathrm{Tr}(1-U_{x,ij})@f]
  ///
  /// energyDensityCalculationMode | description
  /// :--------------------------: | :-------------------:
  ///               1              | equation 1 at @f$t@f$
  ///               2              | equation 2 at @f$t@f$
  static constexpr int  energyDensityCalculationMode = 2;

  std::vector<rational> transverseElectric;
  std::vector<rational> longitudinalElectric;
  std::vector<rational> transverseMagnetic;
  std::vector<rational> longitudinalMagnetic;

  EnergyDensity(Settings const & settings, const bool shouldWriteToFile = false);
  ~EnergyDensity() = default;

  static rational calculateMagneticEnergyDensity(Simulation const & simulation,
                                                 const index positionIndex,
                                                 const index direction);
  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int energyDensityId_;
  int transverseElectricId_;
  int longitudinalElectricId_;
  int transverseMagneticId_;
  int longitudinalMagneticId_;
};
}