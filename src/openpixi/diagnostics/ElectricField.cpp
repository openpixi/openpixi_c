#include "ElectricField.h"

#include <array>
#include <stddef.h>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
ElectricField::ElectricField(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;
  const auto nCells = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                      settings.gridSize[Direction::z];

  electricFieldX.resize(nCells);
  electricFieldY.resize(nCells);
  electricFieldZ.resize(nCells);
}


/// @brief  Calculates the square of the E-field at every grid point in physical units.
void ElectricField::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;
  const std::array<rational, simulation.nDimensions> squaredUnitFactors =
  {simulation.unitFactorsE[Direction::x] * simulation.unitFactorsE[Direction::x],
   simulation.unitFactorsE[Direction::y] * simulation.unitFactorsE[Direction::y],
   simulation.unitFactorsE[Direction::z] * simulation.unitFactorsE[Direction::z]};

  // This is over the whole grid, not only the active part
#pragma omp for schedule(static) nowait
  for(index i = 0; i < simulation.accumulatedCellCounts[0]; ++i)
  {
    electricFieldX[i] = simulation.E[i][Direction::x].square() * squaredUnitFactors[Direction::x];
    electricFieldY[i] = simulation.E[i][Direction::y].square() * squaredUnitFactors[Direction::y];
    electricFieldZ[i] = simulation.E[i][Direction::z].square() * squaredUnitFactors[Direction::z];
  }
}


/// @brief  Creates variables for the square of the E-field in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a electricFieldX, @a electricFieldY and
/// @a electricFieldZ is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ElectricField::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("electricFieldX", dimensionIds, electricFieldXId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("electricFieldY", dimensionIds, electricFieldYId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("electricFieldZ", dimensionIds, electricFieldZId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the square of the electri field to the data file.
///
/// If @a shouldWriteToFile is true, then @a electricFieldX, @a electricFieldY and @a electricFieldZ
/// are written to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ElectricField::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(electricFieldXId_, start, count, electricFieldX);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(electricFieldYId_, start, count, electricFieldY);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(electricFieldZId_, start, count, electricFieldZ);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}