#include "ProjectedJInE.h"

#include <array>
#include <stddef.h>

#include "../GroupAndAlgebra.h"
#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
ProjectedJInE::ProjectedJInE(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile), totalJInE(0.0), integratedTotalJInE(0.0)
{
  projectedJInE.resize(settings.gridSize[Settings::Direction::x], 0.0);
}


openpixi::ProjectedJInE::~ProjectedJInE()
{
}


/// @brief  Calculates the projected and total dot product of @f$\mathcal{E}@f$ and
///         @f$\mathcal{j}@f$ as well as integrating the total over time.
///
/// The projected value is calculated by integrating over transversal planes, the total value by
/// further integrating over the longitudinal axis $x$ and the integrated total by integrating over
/// time. All calculations are done within the active simulation grid and in physical units.
///
/// @see    Settings::boundaryCondition
/// @see    Settings::Direction
void ProjectedJInE::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;
  const auto nTransversalCells = simulation.accumulatedCellCounts[1];
  const auto areaElement = simulation.latticeSpacings[Direction::y] *
                           simulation.latticeSpacings[Direction::z];
#pragma omp single nowait
  totalJInE = 0.0;

#pragma omp for schedule(static)
  for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
  {
    rational result = 0.0;
    auto     positionOffset = simulation.getIndex({iL, 0, 0});
    for(auto i = positionOffset; i < positionOffset + nTransversalCells; ++i)
    {
      result += (simulation.oldJ[i] + simulation.newJ[i]).dot(simulation.E[i][Direction::x]);
    }
    // Multiply by the unit factors and the area element to get the integral in physical units
    projectedJInE[iL] = result * 0.5 * simulation.unitFactorJ *
                        simulation.unitFactorsE[Direction::x] * areaElement;
  }

  rational sum = 0.0;
#pragma omp for schedule(static) nowait
  for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
  {
    sum += projectedJInE[iL];
  }
  // Multiply by the line element to really calculate the integral and not just the sum
#pragma omp critical
  totalJInE += sum * simulation.latticeSpacings[Direction::x];
#pragma omp barrier
  // Finally sum up the total values and multiply by the time step to perform the time integral
#pragma omp single
  integratedTotalJInE += totalJInE * simulation.timeStep;
}


/// @brief  Creates a variable for the projected, total and integrated total dot product of the
///         E-field and the current density in the given data file.
///
/// It is only created if @a shouldWriteToFile is true.
///
/// @param[in]  dataFile  The file in which the variable is created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedJInE::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 2> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId()};
    int error = dataFile.addVariable("J*E_proj", dimensionIds, projectedJInEId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable<1>("J*E_total", {dataFile.tDimensionId()}, totalJInEId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable<1>("J*E_total_integrated", {dataFile.tDimensionId()},
                                    integratedTotalJInEId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the projected, total and integrated total dot product of the E-field and the
///         current density to the data file.
///
/// It is only written if @a shouldWriteToFile is true.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedJInE::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  if(shouldWriteToFile)
  {
    std::array<std::size_t, 2> start = {currentTimeIndex, 0};
    std::array<std::size_t, 2> count = {1, projectedJInE.size()};
    int error = dataFile.writeVariable(projectedJInEId_, start, count, projectedJInE);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable<1>(totalJInEId_, {currentTimeIndex}, {1}, {totalJInE});
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable<1>(integratedTotalJInEId_, {currentTimeIndex}, {1},
                                      {integratedTotalJInE});
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}