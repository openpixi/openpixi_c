#pragma once

#include <vector>
#include <memory>

#include "../CustomTypes.h"
#include "Diagnostics.h"

namespace openpixi
{
class Settings;
class Simulation;
class GaussConstraint;
class DataFile;

/// @brief  A class providing a way to calculate the Gauss constraint integrated over transversal
///         planes.
///
/// It uses the functions or (if available) the already calculated values in @ref GaussConstraint to
/// compute the projected Gauss constraint in physical units. The longitudinal direction is @f$x@f$.
/// Additionally the total Gauss constraint of the active simulation grid is calculated.
/// @see    Settings::Direction
class ProjectedGaussConstraint : public Diagnostics
{
public:
  std::vector<rational> projectedGaussConstraint;
  rational totalGaussConstraint;
  std::shared_ptr<GaussConstraint> gaussConstraint;

  ProjectedGaussConstraint(Settings const & settings,
                           std::shared_ptr<GaussConstraint> gaussConstraint,
                           const bool shouldWriteToFile = false);
  ProjectedGaussConstraint(Settings const & settings, const bool shouldWriteToFile = false);
  ~ProjectedGaussConstraint() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int projectedGaussConstraintId_;
  int totalGaussConstraintId_;
};
}