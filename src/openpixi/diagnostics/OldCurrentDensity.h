#pragma once

#include <vector>

#include "Diagnostics.h"
#include "../CustomTypes.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the square of the old current density at every grid
///         point.
///
/// It is calculated and stored in physical units.
class OldCurrentDensity : public Diagnostics
{
public:
  std::vector<rational> oldCurrentDensity;

  OldCurrentDensity(Settings const & settings, const bool shouldWriteToFile = false);
  ~OldCurrentDensity() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int oldCurrentDensityId_;
};
}