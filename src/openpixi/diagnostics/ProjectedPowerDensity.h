#pragma once

#include <vector>
#include <memory>

#include "../CustomTypes.h"

#include "Diagnostics.h"

namespace openpixi
{
class Settings;
class Simulation;
class ProjectedEnergyDensity;
class DataFile;


/// @brief  A class providing a way to calculate the power density integrated over transversal
///         planes.
///
/// It uses the values provided by ProjectedEnergyDensity to calculate the power density by discrete
/// time derivation. The power density is stored in physical units. The transverse and longitudinal
/// as wells as the electric and magnetic part are calculated and stored separately. The
/// longitudinal direction is @f$x@f$.
/// @see    Settings::Direction
class ProjectedPowerDensity : public Diagnostics
{
public:
  std::vector<rational> transverseElectric;
  std::vector<rational> longitudinalElectric;
  std::vector<rational> transverseMagnetic;
  std::vector<rational> longitudinalMagnetic;

  std::vector<rational> oldTransverseElectricEnergyDensity;
  std::vector<rational> oldLongitudinalElectricEnergyDensity;
  std::vector<rational> oldTransverseMagneticEnergyDensity;
  std::vector<rational> oldLongitudinalMagneticEnergyDensity;

  std::shared_ptr<ProjectedEnergyDensity> newProjectedEnergyDensity;

  ProjectedPowerDensity(Settings const & settings,
                        std::shared_ptr<ProjectedEnergyDensity> projectedEnergyDensity,
                        const bool shouldWriteToFile = false);
  ~ProjectedPowerDensity();

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int projectedPowerDensityId_;
  int transverseElectricId_;
  int longitudinalElectricId_;
  int transverseMagneticId_;
  int longitudinalMagneticId_;
};
}