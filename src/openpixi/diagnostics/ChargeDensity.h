#pragma once

#include <vector>

#include "Diagnostics.h"
#include "../CustomTypes.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the square of the charge density at every grid
///         point.
///
/// It is calculated and stored in physical units.
class ChargeDensity : public Diagnostics
{
public:
  std::vector<rational> chargeDensity;

  ChargeDensity(Settings const & settings, const bool shouldWriteToFile = false);
  ~ChargeDensity() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int chargeDensityId_;
};
}