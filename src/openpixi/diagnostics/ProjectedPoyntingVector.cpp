#include "ProjectedPoyntingVector.h"

#include <stddef.h>
#include <utility>

#include <omp.h>

#include "PoyntingVector.h"
#include "../Simulation.h"
#include "../DataFile.h"
#include "../Utility.h"

namespace openpixi
{
ProjectedPoyntingVector::ProjectedPoyntingVector(Settings const & settings,
                                                 std::shared_ptr<PoyntingVector> poyntingVector,
                                                 const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile), poyntingVector(std::move(poyntingVector))
{
  using Direction = Settings::Direction;
  const auto n = settings.gridSize[Direction::x];
  projectedPoyntingVector[Direction::x].resize(n, 0.0);
  projectedPoyntingVector[Direction::y].resize(n, 0.0);
  projectedPoyntingVector[Direction::z].resize(n, 0.0);
}


ProjectedPoyntingVector::ProjectedPoyntingVector(Settings const & settings,
                                                 const bool shouldWriteToFile) :
  ProjectedPoyntingVector(settings, nullptr, shouldWriteToFile)
{
}


void ProjectedPoyntingVector::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;
  const auto areaElement = simulation.latticeSpacings[Direction::y] *
                           simulation.latticeSpacings[Direction::z];

  // Reset all projected Poynting vector values
  utility::parallelFill(projectedPoyntingVector[Direction::x], 0.0);
  utility::parallelFill(projectedPoyntingVector[Direction::y], 0.0);
  utility::parallelFill(projectedPoyntingVector[Direction::z], 0.0);
#pragma omp barrier
#pragma omp for schedule(static) nowait
  for(auto iX = simulation.leftBoundary(); iX < simulation.rightBoundary(); ++iX)
  {
    for(index iY = 0; iY < simulation.gridSize[Direction::y]; ++iY)
    {
      for(index iZ = 0; iZ < simulation.gridSize[Direction::z]; ++iZ)
      {
        const auto positionIndex = simulation.getIndex({iX, iY, iZ});
        if(poyntingVector == nullptr)
        {
          projectedPoyntingVector[Direction::x][iX] +=
            PoyntingVector::calculatePoyntingVector(simulation, positionIndex, Direction::x);
          projectedPoyntingVector[Direction::y][iX] +=
            PoyntingVector::calculatePoyntingVector(simulation, positionIndex, Direction::y);
          projectedPoyntingVector[Direction::z][iX] +=
            PoyntingVector::calculatePoyntingVector(simulation, positionIndex, Direction::z);
        }
        else
        {
          projectedPoyntingVector[Direction::x][iX] +=
            poyntingVector->poyntingVector[Direction::x][positionIndex];
          projectedPoyntingVector[Direction::y][iX] +=
            poyntingVector->poyntingVector[Direction::y][positionIndex];
          projectedPoyntingVector[Direction::z][iX] +=
            poyntingVector->poyntingVector[Direction::z][positionIndex];
        }
      }
    }
    projectedPoyntingVector[Direction::x][iX] *= areaElement;
    projectedPoyntingVector[Direction::y][iX] *= areaElement;
    projectedPoyntingVector[Direction::z][iX] *= areaElement;
  }
}


/// @brief  Creates variables for the projected Poynting vector in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for the x-, y- and z-component of the projected
/// Poynting vector is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedPoyntingVector::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 2> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId()};
    int error = dataFile.addVariable("S_x_proj", dimensionIds, projectedPoyntingVectorXId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("S_y_proj", dimensionIds, projectedPoyntingVectorYId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("S_z_proj", dimensionIds, projectedPoyntingVectorZId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the projected Poynting vector to the data file.
///
/// If @a shouldWriteToFile is true, then the x-, y- and z-component of the projected Poynting
/// vector are written to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedPoyntingVector::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                             const int currentTimeIndex) const
{
  using Direction = Settings::Direction;
  if(shouldWriteToFile)
  {
    std::array<std::size_t, 2> start = {currentTimeIndex, 0};
    std::array<std::size_t, 2> count = {1, settings.gridSize[Direction::x]};
    int error = dataFile.writeVariable(projectedPoyntingVectorXId_, start, count,
                                       projectedPoyntingVector[Direction::x]);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(projectedPoyntingVectorYId_, start, count,
                                   projectedPoyntingVector[Direction::y]);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(projectedPoyntingVectorZId_, start, count,
                                   projectedPoyntingVector[Direction::z]);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}