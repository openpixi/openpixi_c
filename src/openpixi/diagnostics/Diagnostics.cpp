#include "Diagnostics.h"

namespace openpixi
{
/// @brief  Constructor
///
/// @param[in]  shouldWriteToFile   If true, the diagnostic values will be written to a data file.
/// @see    Simulation::writeDiagnosticsToDataFile()
Diagnostics::Diagnostics(const bool shouldWriteToFile) : shouldWriteToFile(shouldWriteToFile)
{
}
}