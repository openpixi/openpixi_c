#include "EnergyDensity.h"

#include <omp.h>

#include "../Simulation.h"
#include "../DataFile.h"
#include "../Utility.h"

namespace openpixi
{
EnergyDensity::EnergyDensity(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;

  const auto n = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                 settings.gridSize[Direction::z];
  transverseElectric.resize(n, 0.0);
  transverseMagnetic.resize(n, 0.0);
  longitudinalElectric.resize(n, 0.0);
  longitudinalMagnetic.resize(n, 0.0);
}


/// @brief  Calculates the component of the magnetic energy density at the position index in the
///         given direction.
///
/// It is returned in physical units and EnergyDensity::energyDensityCalculationMode determines how
/// the calculation is done. The default value is 1.
rational EnergyDensity::calculateMagneticEnergyDensity(Simulation const & simulation,
                                                       const index positionIndex,
                                                       const index direction)
{
  switch(energyDensityCalculationMode)
  {
    default:
    // Fallthrough
    case 1:
      // Calculates the magnetic energy density = B^2 / 2, at the time of the electric field
      return 0.25 * (simulation.calculateB(positionIndex, direction, TimeIndex::previous).square() +
                     simulation.calculateB(positionIndex, direction, TimeIndex::next).square()) *
             simulation.unitFactorsB[direction] * simulation.unitFactorsB[direction];
      break;
    case 2:
      // Calculates the magnetic energy density = 2 / g^2 * Sum_{i<j} 1 / (a_i * a_j)^2 *
      // ReTr(1 - U_{x,ij}), with g being the coupling constant, a_i the lattice spacings, U the
      // plaquette and i != direction != j. The result is given at the time of the E-field.
      const auto i = Simulation::inverseEpsilonSymbol[direction][0];
      const auto j = Simulation::inverseEpsilonSymbol[direction][1];
      const auto timeAveragedPlaquette =
        (simulation.getPlaquette(positionIndex, i, j, Orientation::positive, Orientation::positive,
                                 TimeIndex::previous) +
         simulation.getPlaquette(positionIndex, i, j, Orientation::positive, Orientation::positive,
                                 TimeIndex::next)) * 0.5;

      return 2.0 * GroupElement::groupDimension * (1.0 - timeAveragedPlaquette.g0) /
             (simulation.couplingConstant * simulation.couplingConstant *
              simulation.latticeSpacings[i] * simulation.latticeSpacings[i] *
              simulation.latticeSpacings[j] * simulation.latticeSpacings[j]);
      break;
  }
}


/// @brief  Calculates the energy density at every point in the active simulation grid.
///
/// @see    Settings::boundaryCondition
void EnergyDensity::execute(Simulation const & simulation)
{
  // Reset all projected energy density values
  utility::parallelFill(transverseElectric, 0.0);
  utility::parallelFill(transverseMagnetic, 0.0);
  utility::parallelFill(longitudinalElectric, 0.0);
  utility::parallelFill(longitudinalMagnetic, 0.0);
#pragma omp barrier
#pragma omp for schedule(static) nowait
  for(auto x = simulation.leftIndexBoundary(); x < simulation.rightIndexBoundary(); ++x)
  {
    for(index i = 0; i < Settings::nDimensions; ++i)
    {
      auto electricEnergyDensity = 0.5 * simulation.E[x][i].square() *
                                   (simulation.unitFactorsE[i] * simulation.unitFactorsE[i]);
      auto magneticEnergyDensity = calculateMagneticEnergyDensity(simulation, x, i);

      if(i == Settings::Direction::x)
      {
        longitudinalElectric[x] += electricEnergyDensity;
        longitudinalMagnetic[x] += magneticEnergyDensity;
      }
      else
      {
        transverseElectric[x] += electricEnergyDensity;
        transverseMagnetic[x] += magneticEnergyDensity;
      }
    }
  }
}


/// @brief  Creates variables for the energy densities in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a transverseElectric, @a longitudinalElectric,
/// @a transverseMagnetic, @a longitudinalMagnetic and the total energy density is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int EnergyDensity::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("Energy", dimensionIds, energyDensityId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("Energy_te", dimensionIds, transverseElectricId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("Energy_le", dimensionIds, longitudinalElectricId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("Energy_tm", dimensionIds, transverseMagneticId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("Energy_lm", dimensionIds, longitudinalMagneticId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the energy densities to the data file.
///
/// If @a shouldWriteToFile is true, then @a transverseElectric, @a longitudinalElectric,
/// @a transverseMagnetic, @a longitudinalMagnetic and the total energy density are written to the
/// corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int EnergyDensity::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    const index n = transverseElectric.size();
    std::vector<rational> energyDensity(n);
    for(index x = 0; x < n; ++x)
    {
      energyDensity[x] = transverseElectric[x] + longitudinalElectric[x] + transverseMagnetic[x] +
                         longitudinalMagnetic[x];
    }

    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(energyDensityId_, start, count,
                                       energyDensity);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(transverseElectricId_, start, count, transverseElectric);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(longitudinalElectricId_, start, count, longitudinalElectric);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(transverseMagneticId_, start, count, transverseMagnetic);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(longitudinalMagneticId_, start, count, longitudinalMagnetic);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}