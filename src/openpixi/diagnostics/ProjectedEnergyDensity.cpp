#include "ProjectedEnergyDensity.h"

#include <array>
#include <stddef.h>
#include <utility>

#ifdef _OPENMP
#include <omp.h>
#endif

#include "EnergyDensity.h"
#include "../Simulation.h"
#include "../DataFile.h"
#include "../Utility.h"

namespace openpixi
{
ProjectedEnergyDensity::ProjectedEnergyDensity(Settings const & settings,
                                               std::shared_ptr<EnergyDensity> energyDensity,
                                               const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile), energyDensity(std::move(energyDensity))
{
  const auto n = settings.gridSize[Settings::Direction::x];
  transverseElectric.resize(n, 0.0);
  transverseMagnetic.resize(n, 0.0);
  longitudinalElectric.resize(n, 0.0);
  longitudinalMagnetic.resize(n, 0.0);
  totalEnergy = 0.0;
}


ProjectedEnergyDensity::ProjectedEnergyDensity(Settings const & settings,
                                               const bool shouldWriteToFile) :
  ProjectedEnergyDensity(settings, nullptr, shouldWriteToFile)
{
}


/// @brief  Calculates the projected and total energy density by integrating over transversal planes
///         and the whole active simulation grid respectively.
///
/// @see    Settings::boundaryCondition
void ProjectedEnergyDensity::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;
  const auto areaElement = simulation.latticeSpacings[Direction::y] *
                           simulation.latticeSpacings[Direction::z];

  // Reset all projected energy density values and total energy
  utility::parallelFill(transverseElectric, 0.0);
  utility::parallelFill(transverseMagnetic, 0.0);
  utility::parallelFill(longitudinalElectric, 0.0);
  utility::parallelFill(longitudinalMagnetic, 0.0);
#pragma omp single
  totalEnergy = 0.0;
#pragma omp for schedule(static)
  for(auto iX = simulation.leftBoundary(); iX < simulation.rightBoundary(); ++iX)
  {
    for(index iY = 0; iY < simulation.gridSize[Direction::y]; ++iY)
    {
      for(index iZ = 0; iZ < simulation.gridSize[Direction::z]; ++iZ)
      {
        auto positionIndex = simulation.getIndex({iX, iY, iZ});
        if(energyDensity == nullptr)
        {
          for(index i = 0; i < simulation.nDimensions; ++i)
          {
            auto electricEnergyDensity = 0.5 * simulation.E[positionIndex][i].square() *
                                         (simulation.unitFactorsE[i] *
                                          simulation.unitFactorsE[i]);
            auto magneticEnergyDensity =
              EnergyDensity::calculateMagneticEnergyDensity(simulation, positionIndex, i);

            if(i == Direction::x)
            {
              longitudinalElectric[iX] += electricEnergyDensity;
              longitudinalMagnetic[iX] += magneticEnergyDensity;
            }
            else
            {
              transverseElectric[iX] += electricEnergyDensity;
              transverseMagnetic[iX] += magneticEnergyDensity;
            }
          }
        }
        else
        {
          longitudinalElectric[iX] += energyDensity->longitudinalElectric[positionIndex];
          longitudinalMagnetic[iX] += energyDensity->longitudinalMagnetic[positionIndex];
          transverseElectric[iX]   += energyDensity->transverseElectric[positionIndex];
          transverseMagnetic[iX]   += energyDensity->transverseMagnetic[positionIndex];
        }
      }
    }
    // Multiply by the area element to really calculate the integral and not just the sum
    longitudinalElectric[iX] *= areaElement;
    longitudinalMagnetic[iX] *= areaElement;
    transverseElectric[iX]   *= areaElement;
    transverseMagnetic[iX]   *= areaElement;
  }

  rational energy = 0.0;
#pragma omp for schedule(static) nowait
  for(auto iX = simulation.leftBoundary(); iX < simulation.rightBoundary(); ++iX)
  {
    energy += longitudinalElectric[iX] + transverseElectric[iX] +
              longitudinalMagnetic[iX] + transverseMagnetic[iX];
  }
  // Multiply by the line element to really calculate the integral and not just the sum
#pragma omp critical
  totalEnergy += energy * simulation.latticeSpacings[Direction::x];
}


/// @brief  Creates variables for the projected energy densities in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a transverseElectric, @a longitudinalElectric,
/// @a transverseMagnetic, @a longitudinalMagnetic, the total projected energy density and the
/// total energy is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedEnergyDensity::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 2> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId()};
    int error = dataFile.addVariable("E_proj", dimensionIds, projectedEnergyDensityId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("E_proj_te", dimensionIds, transverseElectricId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("E_proj_le", dimensionIds, longitudinalElectricId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("E_proj_tm", dimensionIds, transverseMagneticId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("E_proj_lm", dimensionIds, longitudinalMagneticId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable<1>("E_total", {dataFile.tDimensionId()}, totalEnergyId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the projected energy densities to the data file.
///
/// If @a shouldWriteToFile is true, then @a transverseElectric, @a longitudinalElectric,
/// @a transverseMagnetic, @a longitudinalMagnetic, the total projected energy density and the total
/// energy are written to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedEnergyDensity::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                            const int currentTimeIndex) const
{
  if(shouldWriteToFile)
  {
    const index n = transverseElectric.size();
    std::vector<rational> projectedEnergyDensity(n);
    for(index i = 0; i < n; ++i)
    {
      projectedEnergyDensity[i] = transverseElectric[i] + longitudinalElectric[i] +
                                  transverseMagnetic[i] + longitudinalMagnetic[i];
    }

    std::array<std::size_t, 2> start = {currentTimeIndex, 0};
    std::array<std::size_t, 2> count = {1, n};
    int error = dataFile.writeVariable(projectedEnergyDensityId_, start, count,
                                       projectedEnergyDensity);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(transverseElectricId_, start, count, transverseElectric);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(longitudinalElectricId_, start, count, longitudinalElectric);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(transverseMagneticId_, start, count, transverseMagnetic);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(longitudinalMagneticId_, start, count, longitudinalMagnetic);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable<1>(totalEnergyId_, {currentTimeIndex}, {1}, {totalEnergy});
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}