#pragma once

#include <vector>

#include "Diagnostics.h"
#include "../CustomTypes.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the square of the electric field at every grid
///         point.
///
/// It is calculated and stored in physical units.
class ElectricField : public Diagnostics
{
public:
  std::vector<rational> electricFieldX;
  std::vector<rational> electricFieldY;
  std::vector<rational> electricFieldZ;

  ElectricField(Settings const & settings, const bool shouldWriteToFile = false);
  ~ElectricField() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int electricFieldXId_;
  int electricFieldYId_;
  int electricFieldZId_;
};
}