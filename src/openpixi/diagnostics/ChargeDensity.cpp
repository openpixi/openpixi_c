#include "ChargeDensity.h"

#include <array>
#include <stddef.h>
#include <omp.h>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
ChargeDensity::ChargeDensity(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;
  const auto nCells = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                      settings.gridSize[Direction::z];
  chargeDensity.resize(nCells);
}


/// @brief  Calculates the square of the charge density at every grid point in physical units.
void ChargeDensity::execute(Simulation const & simulation)
{
  // This is over the whole grid, not only the active part
#pragma omp for schedule(static) nowait
  for(index i = 0; i < simulation.accumulatedCellCounts[0]; ++i)
  {
    chargeDensity[i] = simulation.rho[i].square() * simulation.unitFactorRho *
                       simulation.unitFactorRho;
  }
}


/// @brief  Creates a variable for the square of the charge density in the given data file.
///
/// It is only created if @a shouldWriteToFile is true.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ChargeDensity::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("chargeDensity", dimensionIds, chargeDensityId_);

    return error;
  }

  return DataFile::noError;
}


/// @brief  Writes the square of the charge density to the data file.
///
/// It is only written if @a shouldWriteToFile is true.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ChargeDensity::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(chargeDensityId_, start, count, chargeDensity);
    return error;
  }

  return DataFile::noError;
}
}