#pragma once

#include <vector>

#include "../CustomTypes.h"
#include "Diagnostics.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the projected, total and integrated total dot
///         product of the current density and the E-field.
///
/// The projected value is calculated by integrating over transversal planes, the total value by
/// further integrating over the longitudinal axis $x$ and the integrated total by integrating over
/// time. All calculations are done within the active simulation grid and in physical units.
///
/// @see    Settings::boundaryCondition
/// @see    Settings::Direction
class ProjectedJInE : public Diagnostics
{
public:
  std::vector<rational> projectedJInE;
  rational totalJInE;
  rational integratedTotalJInE;

  ProjectedJInE(Settings const & settings, const bool shouldWriteToFile = false);
  ~ProjectedJInE();

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int projectedJInEId_;
  int totalJInEId_;
  int integratedTotalJInEId_;
};
}