#include "PoyntingVector.h"

#include <stddef.h>

#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
PoyntingVector::PoyntingVector(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;
  const auto n = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                 settings.gridSize[Direction::z];

  poyntingVector[Direction::x].resize(n);
  poyntingVector[Direction::y].resize(n);
  poyntingVector[Direction::z].resize(n);
}


PoyntingVector::~PoyntingVector()
{
}


/// @brief  Calculates a component of the Poynting vector at the given position index in physical
///         units.
///
/// PoyntingVector::poyntingVectorCalculationMode determines how the calculation is done. The
/// default value is 1.
rational PoyntingVector::calculatePoyntingVector(Simulation const & simulation,
                                                 const index positionIndex,
                                                 const index direction)
{
  rational poyntingVector = 0.0;
  switch(poyntingVectorCalculationMode)
  {
    default:
    // Fallthrough
    case 1:
      // Calculates S_{x,i} = 2 / g * Sum_{j!=i}(1 / (a_i * a_j) * ImTr(E_{x,j}^a * t_a *
      // U_{x,j-i}) with g being the coupling constant, a_i the lattice spacings, E the
      // electric field, t_a the ath group generator and U the plaquette.
      for(index j = 0; j < Simulation::nDimensions; ++j)
      {
        if(j != direction)
        {
          auto timeAveragedPlaquette =
            (simulation.getPlaquette(positionIndex, j, direction, Orientation::positive,
                                     Orientation::negative, TimeIndex::previous).project() +
             simulation.getPlaquette(positionIndex, j, direction, Orientation::positive,
                                     Orientation::negative, TimeIndex::next).project()) * 0.5;
          poyntingVector += simulation.E[positionIndex][j].dot(timeAveragedPlaquette) *
                            simulation.unitFactorsE[j] / simulation.latticeSpacings[j];
        }
      }
      poyntingVector = poyntingVector / (simulation.couplingConstant *
                                         simulation.latticeSpacings[direction]);
      break;
    case 2:
      // Calculates S = E x B
      // Directions for cross product
      index direction1 = simulation.inverseEpsilonSymbol[direction][0];
      index direction2 = simulation.inverseEpsilonSymbol[direction][1];

      auto  E1 = simulation.E[positionIndex][direction1];
      auto  E2 = simulation.E[positionIndex][direction2];
      auto  timeAveragedB1 =
        (simulation.calculateB(positionIndex, direction1, TimeIndex::previous) +
         simulation.calculateB(positionIndex, direction1, TimeIndex::next)) * 0.5;
      auto timeAveragedB2 =
        (simulation.calculateB(positionIndex, direction2, TimeIndex::previous) +
         simulation.calculateB(positionIndex, direction2, TimeIndex::next)) * 0.5;

      // Convert to physical units
      E1 *= simulation.unitFactorsE[direction];
      E2 *= simulation.unitFactorsE[direction];
      timeAveragedB1 *= simulation.unitFactorsB[direction];
      timeAveragedB2 *= simulation.unitFactorsB[direction];

      // S = E x B
      poyntingVector = E1.dot(timeAveragedB2) - E2.dot(timeAveragedB1);
      break;
  }

  return poyntingVector;
}


/// @brief  Calculates a component of the Poynting vector at the given position in physical units.
///
/// PoyntingVector::poyntingVectorCalculationMode determines how the calculation is done. The
/// default value is 1.
rational
PoyntingVector::calculatePoyntingVector(Simulation const & simulation,
                                        const std::array<index, Settings::nDimensions> position,
                                        const index direction)
{
  auto positionIndex = simulation.getIndex(position);
  return calculatePoyntingVector(simulation, positionIndex, direction);
}


/// @brief  Calculates and stores the Poynting vector inside the active simulation grid in physical
///         units.
///
/// @see    Settings::boundaryCondition
void PoyntingVector::execute(Simulation const & simulation)
{
  for(index i = 0; i < Settings::nDimensions; ++i)
  {
#if USE_INDEXES == 1
#pragma omp for schedule(static) nowait
    for(auto x = simulation.leftIndexBoundary(); x < simulation.rightIndexBoundary(); ++x)
    {
      poyntingVector[i][x] = calculatePoyntingVector(simulation, x, i);
    }
#else
    using Direction = Settings::Direction;
#pragma omp for schedule(static) nowait
    for(auto iX = simulation.leftBoundary(); iX < simulation.rightBoundary(); ++iX)
    {
      for(index iY = 0; iY < simulation.gridSize[Direction::y]; ++iY)
      {
        for(index iZ = 0; iZ < simulation.gridSize[Direction::z]; ++iZ)
        {
          auto positionIndex = simulation.getIndex({iX, iY, iZ});
          poyntingVector[i][positionIndex] = calculatePoyntingVector(simulation, {iX, iY, iZ}, i);
        }
      }
    }
#endif
  }
}


/// @brief  Creates a variable for each component of the poynting vector in the given data file.
///
/// It is only created if @a shouldWriteToFile is true.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int PoyntingVector::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("S_x", dimensionIds, poyntingVectorXId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("S_y", dimensionIds, poyntingVectorYId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("S_z", dimensionIds, poyntingVectorZId_);
    return error;
  }

  return DataFile::noError;
}


/// @brief  Writes the components of the Poynting vector to the data file.
///
/// It is only written if @a shouldWriteToFile is true.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int PoyntingVector::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                    const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(poyntingVectorXId_, start, count,
                                       poyntingVector[Direction::x]);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(poyntingVectorYId_, start, count, poyntingVector[Direction::y]);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(poyntingVectorZId_, start, count, poyntingVector[Direction::z]);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}