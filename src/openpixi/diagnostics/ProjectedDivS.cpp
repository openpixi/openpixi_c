#include "ProjectedDivS.h"

#include <array>
#include <stddef.h>
#include <utility>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"
#include "PoyntingVector.h"

namespace openpixi
{
ProjectedDivS::ProjectedDivS(Settings const & settings,
                             std::shared_ptr<PoyntingVector> newPoyntingVector,
                             const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile), poyntingVector(std::move(newPoyntingVector))
{
  projectedDivS.resize(settings.gridSize[Settings::Direction::x], 0.0);
}


ProjectedDivS::~ProjectedDivS()
{
}


/// @brief  Calculates the divergence of the Poynting vector projected onto the longitudinal
///         direction by integrating over each transversal plane in the active simulation grid.
///
/// @see    Settings::boundaryCondition
void ProjectedDivS::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;
  const auto areaElement = simulation.latticeSpacings[Direction::y] *
                           simulation.latticeSpacings[Direction::z];
#pragma omp barrier
#pragma omp for schedule(static) nowait
  for(auto iX = simulation.leftBoundary(); iX < simulation.rightBoundary(); ++iX)
  {
    rational result = 0.0;
    for(index iY = 0; iY < simulation.gridSize[Direction::y]; ++iY)
    {
      for(index iZ = 0; iZ < simulation.gridSize[Direction::z]; ++iZ)
      {
#if USE_LEFT_DERIVATION == 1
        auto positionIndex = simulation.getIndex({iX, iY, iZ});
        auto shiftedIndex  = simulation.shift(positionIndex, Direction::x, Orientation::negative);

        // Only calculate the x-derivative part, because due to the periodic boundary conditions in
        // the transversal directions, the other parts vanish
        result += (poyntingVector->poyntingVector[Direction::x][positionIndex] -
                   poyntingVector->poyntingVector[Direction::x][shiftedIndex]) /
                  simulation.latticeSpacings[Direction::x];
#else
        auto positionIndex = simulation.getIndex({iX, iY, iZ});
        auto shiftedIndex  = simulation.shift(positionIndex, Direction::x, Orientation::positive);

        // Only calculate the x-derivative part, because due to the periodic boundary conditions in
        // the transversal directions, the other parts vanish
        result += (poyntingVector->poyntingVector[Direction::x][shiftedIndex] -
                   poyntingVector->poyntingVector[Direction::x][positionIndex]) /
                  simulation.latticeSpacings[Direction::x];
#endif
      }
    }
    // Multiply by the area element to really calculate the integral and not just the sum
    projectedDivS[iX] = result * areaElement;
  }
}


/// @brief  Creates a variable for the projected divergence of the Poynting Vector in the given data
///         file.
///
/// It is only created if @a shouldWriteToFile is true.
///
/// @param[in]  dataFile  The file in which the variable is created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedDivS::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 2> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId()};
    int error = dataFile.addVariable("divS_proj", dimensionIds, projectedDivSId_);
    return error;
  }

  return DataFile::noError;
}


/// @brief  Writes the projected divergence of the Poynting vector to the data file.
///
/// It is only written if @a shouldWriteToFile is true.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedDivS::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  if(shouldWriteToFile)
  {
    std::array<std::size_t, 2> start = {currentTimeIndex, 0};
    std::array<std::size_t, 2> count = {1, projectedDivS.size()};
    int error = dataFile.writeVariable(projectedDivSId_, start, count, projectedDivS);
    return error;
  }

  return DataFile::noError;
}
}