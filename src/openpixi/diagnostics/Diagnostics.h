#pragma once

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  An abstract base class for all diagnostic calculations
class Diagnostics
{
public:
  /// @brief  Determines wheter the diagnostic values are written to a data file or not
  const bool shouldWriteToFile;

  explicit Diagnostics(const bool shouldWriteToFile);
  virtual ~Diagnostics() = default;

  /// @brief  Calculates the diagnostic values.
  virtual void execute(Simulation const & simulation) = 0;
  /// @brief  Creates a variable for each diagnostic value in the data file.
  virtual int createDataFileVariables(DataFile const & dataFile) = 0;
  /// @brief  Writes all diagnostic data to the corresponding variables in the data file.
  virtual int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                              const int timeIndex) const = 0;
};
}