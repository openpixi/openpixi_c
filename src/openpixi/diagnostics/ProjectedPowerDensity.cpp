#include "ProjectedPowerDensity.h"

#include <array>
#include <stddef.h>
#include <utility>

#include "ProjectedEnergyDensity.h"
#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
ProjectedPowerDensity::ProjectedPowerDensity(
  Settings const & settings,
  std::shared_ptr<ProjectedEnergyDensity> projectedEnergyDensity,
  const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile),
  newProjectedEnergyDensity(std::move(projectedEnergyDensity))
{
  const auto n = settings.gridSize[Settings::Direction::x];

  transverseElectric.resize(n, 0.0);
  transverseMagnetic.resize(n, 0.0);
  longitudinalElectric.resize(n, 0.0);
  longitudinalMagnetic.resize(n, 0.0);

  oldTransverseElectricEnergyDensity.resize(n, 0.0);
  oldTransverseMagneticEnergyDensity.resize(n, 0.0);
  oldLongitudinalElectricEnergyDensity.resize(n, 0.0);
  oldLongitudinalMagneticEnergyDensity.resize(n, 0.0);
}


ProjectedPowerDensity::~ProjectedPowerDensity()
{
}


/// @brief  Calculates the power density projected onto the longitudinal direction by summing over
///         each transversal plane in the active simulation grid.
///
/// @see    Settings::boundaryCondition
void ProjectedPowerDensity::execute(Simulation const & simulation)
{
#pragma omp for schedule(static)
  for(auto iL = simulation.leftBoundary(); iL < simulation.rightBoundary(); ++iL)
  {
    transverseElectric[iL]   = (newProjectedEnergyDensity->transverseElectric[iL] -
                                oldTransverseElectricEnergyDensity[iL]) / simulation.timeStep;
    longitudinalElectric[iL] = (newProjectedEnergyDensity->longitudinalElectric[iL] -
                                oldLongitudinalElectricEnergyDensity[iL]) / simulation.timeStep;
    transverseMagnetic[iL]   = (newProjectedEnergyDensity->transverseMagnetic[iL] -
                                oldTransverseMagneticEnergyDensity[iL]) / simulation.timeStep;
    longitudinalMagnetic[iL] = (newProjectedEnergyDensity->longitudinalMagnetic[iL] -
                                oldLongitudinalMagneticEnergyDensity[iL]) / simulation.timeStep;
  }

#pragma omp single
  {
    oldTransverseElectricEnergyDensity   = newProjectedEnergyDensity->transverseElectric;
    oldLongitudinalElectricEnergyDensity = newProjectedEnergyDensity->longitudinalElectric;
    oldTransverseMagneticEnergyDensity   = newProjectedEnergyDensity->transverseMagnetic;
    oldLongitudinalMagneticEnergyDensity = newProjectedEnergyDensity->longitudinalMagnetic;
  }
}


/// @brief  Creates variables for the projected power densities in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a transverseElectric, @a longitudinalElectric,
/// @a transverseMagnetic, @a longitudinalMagnetic and the total projected power density is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int openpixi::ProjectedPowerDensity::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 2> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId()};
    int error = dataFile.addVariable("P_proj", dimensionIds, projectedPowerDensityId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("P_proj_te", dimensionIds, transverseElectricId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("P_proj_le", dimensionIds, longitudinalElectricId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("P_proj_tm", dimensionIds, transverseMagneticId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("P_proj_lm", dimensionIds, longitudinalMagneticId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the projected power densities to the data file.
///
/// If @a shouldWriteToFile is true, then @a transverseElectric, @a longitudinalElectric,
/// @a transverseMagnetic, @a longitudinalMagnetic and the total projected power density are written
/// to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int ProjectedPowerDensity::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                           const int currentTimeIndex) const
{
  if(shouldWriteToFile)
  {
    const int n = transverseElectric.size();
    std::vector<rational> projectedPowerDensity(n);
    for(index i = 0; i < n; ++i)
    {
      projectedPowerDensity[i] = transverseElectric[i] + longitudinalElectric[i] +
                                 transverseMagnetic[i] + longitudinalMagnetic[i];
    }

    std::array<std::size_t, 2> start = {currentTimeIndex, 0};
    std::array<std::size_t, 2> count = {1, n};
    int error = dataFile.writeVariable(projectedPowerDensityId_, start, count,
                                       projectedPowerDensity);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(transverseElectricId_, start, count, transverseElectric);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(longitudinalElectricId_, start, count, longitudinalElectric);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(transverseMagneticId_, start, count, transverseMagnetic);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(longitudinalMagneticId_, start, count, longitudinalMagnetic);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}