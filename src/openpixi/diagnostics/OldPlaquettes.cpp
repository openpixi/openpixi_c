#include "OldPlaquettes.h"

#include <array>
#include <stddef.h>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
OldPlaquettes::OldPlaquettes(Settings const & settings, const bool shouldWriteToFile) :
  Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;
  const auto nCells = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                      settings.gridSize[Direction::z];

  oldPlaquettesXY.resize(nCells);
  oldPlaquettesXZ.resize(nCells);
  oldPlaquettesYZ.resize(nCells);
}


/// @brief  Calculates the square of linearized algebra elements corresponding to the old plaquettes
///         at every grid point.
///
/// This means that GroupElement::project() and AlgebraElement::square() are applied to all elements
/// of Simulation::oldPlaquettes.
void OldPlaquettes::execute(Simulation const & simulation)
{
  using Direction = Settings::Direction;

  // This is over the whole grid, not only the active part
#pragma omp for schedule(static) nowait
  for(index i = 0; i < simulation.accumulatedCellCounts[0]; ++i)
  {
    oldPlaquettesXY[i] = simulation.getPlaquette(i, Direction::x, Direction::y,
                                                 Orientation::positive, Orientation::positive,
                                                 TimeIndex::previous).project().square();
    oldPlaquettesXZ[i] = simulation.getPlaquette(i, Direction::x, Direction::z,
                                                 Orientation::positive, Orientation::positive,
                                                 TimeIndex::previous).project().square();
    oldPlaquettesYZ[i] = simulation.getPlaquette(i, Direction::y, Direction::z,
                                                 Orientation::positive, Orientation::positive,
                                                 TimeIndex::previous).project().square();
  }
}


/// @brief  Creates variables for the square of the old plaquettes in the given data file.
///
/// If @a shouldWriteToFile is true, a variable for @a oldPlaquettesXY, @a oldPlaquettesXZ and
/// @a oldPlaquettesYZ is created.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int OldPlaquettes::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("oldPlaquettesXY", dimensionIds, oldPlaquettesXYId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("oldPlaquettesXZ", dimensionIds, oldPlaquettesXZId_);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.addVariable("oldPlaquettesYZ", dimensionIds, oldPlaquettesYZId_);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


/// @brief  Writes the square of the old gauge links to the data file.
///
/// If @a shouldWriteToFile is true, then @a oldPlaquettesXY, @a oldPlaquettesXZ and
/// @a oldPlaquettesYZ are written to the corresponding variables in the data file.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int OldPlaquettes::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                                   const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(oldPlaquettesXYId_, start, count, oldPlaquettesXY);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(oldPlaquettesXZId_, start, count, oldPlaquettesXZ);
    if(error != DataFile::noError)
    {
      return error;
    }

    error = dataFile.writeVariable(oldPlaquettesYZId_, start, count, oldPlaquettesYZ);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}
}