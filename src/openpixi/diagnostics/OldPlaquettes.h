#pragma once

#include <vector>

#include "Diagnostics.h"
#include "../CustomTypes.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the square of the linearized algebra elements
///         corresponding to the old plaquettes at every grid point.
class OldPlaquettes : public Diagnostics
{
public:
  std::vector<rational> oldPlaquettesXY;
  std::vector<rational> oldPlaquettesXZ;
  std::vector<rational> oldPlaquettesYZ;

  OldPlaquettes(Settings const & settings, const bool shouldWriteToFile = false);
  ~OldPlaquettes() = default;

  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int oldPlaquettesXYId_;
  int oldPlaquettesXZId_;
  int oldPlaquettesYZId_;
};
}