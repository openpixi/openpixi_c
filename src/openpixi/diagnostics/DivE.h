#pragma once

#include <vector>

#include "../CustomTypes.h"
#include "../GroupAndAlgebra.h"
#include "Diagnostics.h"

namespace openpixi
{
class Settings;
class Simulation;
class DataFile;

/// @brief  A class providing a way to calculate the divergence of the electric field at every grid
///         point.
///
/// It is calculated and stored in physical units.
class DivE : public Diagnostics
{
public:
  std::vector<AlgebraElement> divE;

  DivE(Settings const & settings, const bool shouldWriteToFile = false);
  ~DivE() = default;

  static AlgebraElement calculateDivE(Simulation const & simulation, const index positionIndex);
  void execute(Simulation const & simulation);
  int createDataFileVariables(DataFile const & dataFile);
  int writeToDataFile(Settings const & settings, DataFile const & dataFile,
                      const int currentTimeIndex) const;

private:
  int divEId_;
};
}