#include "DivE.h"

#include <array>
#include <stddef.h>
#include <omp.h>

#include "../Settings.h"
#include "../Simulation.h"
#include "../DataFile.h"

namespace openpixi
{
DivE::DivE(Settings const & settings, const bool shouldWriteToFile) : Diagnostics(shouldWriteToFile)
{
  using Direction = Settings::Direction;

  const auto n = settings.gridSize[Direction::x] * settings.gridSize[Direction::y] *
                 settings.gridSize[Direction::z];
  divE.resize(n, AlgebraElement());
}


/// @brief  Calculates the divergence of the E-field at the given position index in physical units.
AlgebraElement DivE::calculateDivE(Simulation const & simulation, const index positionIndex)
{
  AlgebraElement result;

  for(index i = 0; i < Settings::nDimensions; ++i)
  {
    auto shiftedIndex = simulation.shift(positionIndex, i, Orientation::negative);
    auto difference   = simulation.E[positionIndex][i];
    difference -= simulation.E[shiftedIndex][i].act(simulation.oldU[shiftedIndex][i]);
    difference *= simulation.unitFactorsE[i] / simulation.latticeSpacings[i];
    result += difference;
  }

  return result;
}


/// @brief  Calculates the divergence of the E-field at every point in the active simulation grid.
///
/// @see    Settings::boundaryCondition
void DivE::execute(Simulation const & simulation)
{
#pragma omp for schedule(static) nowait
  for(auto i = simulation.leftIndexBoundary(); i < simulation.rightIndexBoundary(); ++i)
  {
    divE[i] = calculateDivE(simulation, i);
  }
}


/// @brief  Creates a variable for the divergence of the E-field in the given data file.
///
/// It is only created if @a shouldWriteToFile is true.
///
/// @param[in]  dataFile  The file in which the variables are created.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int DivE::createDataFileVariables(DataFile const & dataFile)
{
  if(shouldWriteToFile)
  {
    std::array<int, 4> dimensionIds = {dataFile.tDimensionId(), dataFile.xDimensionId(),
                                       dataFile.yDimensionId(), dataFile.zDimensionId()};
    int error = dataFile.addVariable("divE", dimensionIds, divEId_);
    return error;
  }

  return DataFile::noError;
}


/// @brief  Writes the square of the divergence of the E-field to the data file.
///
/// It is only written if @a shouldWriteToFile is true.
///
/// @param[in]  settings          The settings of the current simulation.
/// @param[in]  dataFile          The file, where the diagnostic values are written to.
/// @param[in]  currentTimeIndex  The current index of the time dimension of the data file. Must not
///                               be negative.
/// @return     A NetCDF error code if something went wrong, or DataFile::noError otherwise.
int DivE::writeToDataFile(Settings const & settings, DataFile const & dataFile,
                          const int currentTimeIndex) const
{
  using Direction = Settings::Direction;

  if(shouldWriteToFile)
  {
    std::vector<rational> squaredDivE(divE.size());
    for(index i = 0; i < divE.size(); ++i)
    {
      squaredDivE[i] = divE[i].square();
    }

    std::array<std::size_t, 4> start = {currentTimeIndex, 0, 0, 0};
    std::array<std::size_t, 4> count = {1, settings.gridSize[Direction::x],
                                        settings.gridSize[Direction::y],
                                        settings.gridSize[Direction::z]};
    int error = dataFile.writeVariable(divEId_, start, count, squaredDivE);
    return error;
  }

  return DataFile::noError;
}
}