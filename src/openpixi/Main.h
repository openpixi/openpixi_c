#pragma once

#include <iostream>
#include <fstream>
#include <chrono>
#include <memory>
#include <utility>
#include <cmath>
#include <array>
#include <string>
#include <sstream>
#include <iomanip>

#include <omp.h>
#include <mpi.h>

#include "CustomTypes.h"

#include "Settings.h"
#include "Simulation.h"
#include "Utility.h"
#include "SuperParticle.h"
#include "DataFile.h"
#include "MpiWrapper.h"

#include "initial/GaussianPulse.h"
#include "initial/GaussViolationExample.h"
#include "initial/CgcInitialCondition.h"
#include "initial/CoherentMvModel.h"

#include "diagnostics/EnergyDensity.h"
#include "diagnostics/ProjectedEnergyDensity.h"
#include "diagnostics/ProjectedPowerDensity.h"
#include "diagnostics/ProjectedDivS.h"
#include "diagnostics/PoyntingVector.h"
#include "diagnostics/GaussConstraint.h"
#include "diagnostics/ProjectedJInE.h"
#include "diagnostics/OldGaugeLinks.h"
#include "diagnostics/OldCurrentDensity.h"
#include "diagnostics/OldPlaquettes.h"
#include "diagnostics/DivE.h"
#include "diagnostics/ChargeDensity.h"
#include "diagnostics/ElectricField.h"
#include "diagnostics/ProjectedGaussConstraint.h"
#include "diagnostics/ProjectedPoyntingVector.h"

#ifndef SAVE_STDOUT_IN_FILE
/// @brief  Determines whether the output stream of the simulation is std::cout or a file stream.
#define SAVE_STDOUT_IN_FILE 0
#endif