#include "CgcInitialCondition.h"

#include <utility>
#include <algorithm>
#include <sstream>

#include "../Simulation.h"
#include "InitialChargeDensity.h"
#include "../diagnostics/DivE.h"

namespace openpixi
{
/// @brief  Constructs a CGC initial condition form the given charge density and parameters.
///
/// For an explination of `infraredRegulator`, `cutoffValue` and `cutoff` see
/// utility::solve2dPoissonEquation()
CgcInitialCondition::CgcInitialCondition(
  std::unique_ptr<const InitialChargeDensity> initialChargeDensity, const Velocity velocity,
  const rational infraredRegulator, const rational cutoffValue, utility::Cutoff cutoff) :
  initialChargeDensity{std::move(initialChargeDensity)}, velocity{velocity},
  infraredRegulator{infraredRegulator}, cutoffValue{cutoffValue}, cutoff{cutoff}
{
}


/// @brief  Sets E-field and gauge links and spawns super particles inside the nucleon's volume.
///         Gauge links are also set behind the nucleon.
void CgcInitialCondition::apply(Simulation & simulation)
{
  using Direction = Settings::Direction;

  auto rho    = initialChargeDensity->initialize();
  auto oldPhi = calculateOldPhi(rho, simulation);
  auto V = calculateGaugeTransformation(oldPhi, simulation);
  auto oldV   = convertGaugeTransformation(V);
  auto newPhi = calculateNewPhi(oldPhi, simulation);
  V = calculateGaugeTransformation(newPhi, simulation);
  auto newV   = convertGaugeTransformation(V);
  initializeGaugeLinks(simulation, oldV, TimeIndex::previous);
  initializeGaugeLinks(simulation, newV, TimeIndex::next);
  initializeElectricField(simulation);

  if(velocity == Velocity::positive)
  {
    spawnSuperParticles<Velocity::positive>(simulation, simulation.rightMovingSuperParticles);
  }
  else
  {
    spawnSuperParticles<Velocity::negative>(simulation, simulation.leftMovingSuperParticles);
  }
}


/// @brief  Returns a string describing the CGC initial condition with all its parameters
std::string openpixi::CgcInitialCondition::getDescription(void) const
{
  using namespace utility;

  std::stringstream description;
  description << "CGC model: " << std::endl <<
    " velocity = " << velocity << std::endl <<
    " infraredRegulator = " << infraredRegulator << std::endl <<
    " cutoffValue = " << cutoffValue << std::endl <<
    " cutoff = " << cutoff << std::endl;
  description << initialChargeDensity->getDescription();
  return description.str();
}


/// @brief  Returns the dimensionless @f$\phi@f$ solving the 2D Poisson equation
///         @f$-\Delta_T\phi=\rho@f$.
///
/// @param  rho   3D charge density of the nucleon in phys. units multiplied by @f$a_1@f$
std::vector<std::vector<AlgebraElement>>
CgcInitialCondition::calculateOldPhi(std::vector<std::vector<AlgebraElement>> & rho,
                                     Simulation const & simulation) const
{
  using Direction = Settings::Direction;
  if(rho.empty() == false)
  {
    std::vector<std::vector<AlgebraElement>> phi(rho.size(),
                                                 std::vector<AlgebraElement>(rho[0].size()));

    // Solve the 2D Poisson equation for each transversal plane
    for(index i = 0; i < rho.size(); ++i)
    {
      phi[i] = utility::solve2dPoissonEquation(rho[i],
                                               simulation.gridSize[Direction::y],
                                               simulation.gridSize[Direction::z],
                                               simulation.latticeSpacings[Direction::y],
                                               simulation.latticeSpacings[Direction::z],
                                               infraredRegulator, cutoff, cutoffValue);
    }
    return phi;
  }
  else
  {
    return std::vector<std::vector<AlgebraElement>>();
  }
}


/// @brief  Returns the path-ordered exponential of the integral over @f$\phi@f$ inside the nucleon
///         volume.
///
/// It is assumed that the integral from @f$-\infty@f$ to the left boundary of the nucleon is 0. The
/// gauge transformation is then calculated by stepwise multiplication of Wilson lines over a single
/// lattice spacing @f$a_x@f$ in the correct ordering.
std::vector<std::vector<GroupElement>> CgcInitialCondition::calculateGaugeTransformation(
  std::vector<std::vector<AlgebraElement>> const & phi, Simulation const & simulation) const
{
  using Direction = Settings::Direction;

  if(phi.empty() == false)
  {
    const index nLongitudinalCells = phi.size();
    const index nTransverseCells   = phi[0].size();

    std::vector<std::vector<GroupElement>> V(nLongitudinalCells,
                                             std::vector<GroupElement>(nTransverseCells,
                                                                       GroupElement{}));

    for(index iL = 1; iL < nLongitudinalCells; ++iL)
    {
      for(index iT = 0; iT < nTransverseCells; ++iT)
      {
        constexpr int refinement = 16;
        const auto    leftPhi    = -simulation.couplingConstant * phi[iL - 1][iT];
        const auto    rightPhi   = -simulation.couplingConstant * phi[iL][iT];
        // Calculate the current V by multiplying the V to the left with the Wilson line over a
        // single lattice spacing a_x
        V[iL][iT] = V[iL - 1][iT] * calculateRefinedWilsonLine(leftPhi, rightPhi, refinement,
                                                               simulation);
      }
    }
    return V;
  }
  else
  {
    return std::vector<std::vector<GroupElement>>();
  }
}


/// @brief  Returns the path-ordered exponential of the integral of the given field along a single
///         lattice spacing in +x-direction.
///
/// The integral is approximated by a sum with a step size of @f$\frac{a_x}{n}@f$, where @f$n@f$ is
/// the refinement. The field is linearly interpolated between `leftField` at and `rightField`. It
/// is also assumed, that the field is already multiplied by the lattice spacing, therefore the sum
/// only needs to be multiplied by @f$\frac{1}{n}@f$ to account for the refinded step size. The
/// path-ordered exponential of the sum is furthermore calculated as a path-ordered product of
/// exponentials.
GroupElement CgcInitialCondition::calculateRefinedWilsonLine(AlgebraElement const & leftField,
                                                             AlgebraElement const & rightField,
                                                             const int refinement,
                                                             Simulation const & simulation) const
{
  const auto   fieldInkrement = 1.0 / (refinement * refinement) * (rightField - leftField);
  auto field = 1.0 / refinement * leftField;
  GroupElement result;
  for(int i = 0; i < refinement; ++i)
  {
    field  += fieldInkrement;
    result *= field.exp();
  }
  return result;
}


/// @brief  Converts the vector of transversal planes to a simple vector considering the nucleon
///         velocity.
///
/// If the velocity is negative, the ordering in the returned vector is the same as in the vector of
/// vectors. If the velocity is positive, the ordering of the transversal planes is reversed.
std::vector<GroupElement> CgcInitialCondition::convertGaugeTransformation(
  std::vector<std::vector<GroupElement>> const & V) const
{
  if(V.empty() == false)
  {
    const index nLongitudinalCells = V.size();
    const index nTransversalCells  = V[0].size();
    std::vector<GroupElement> W(nLongitudinalCells * nTransversalCells);

    const int increment = (velocity == Velocity::negative ? nTransversalCells :
                           -static_cast<int>(nTransversalCells));
    index     i = (velocity == Velocity::negative ? 0 : W.size() - nTransversalCells);
    for(auto const & element : V)
    {
      std::copy(element.begin(), element.end(), &W[i]);
      i += increment;
    }
    return W;
  }
  else
  {
    return std::vector<GroupElement>();
  }
}


/// @brief  Calculates the new phi by shifting the old one, one time step to the left.
std::vector<std::vector<AlgebraElement>> CgcInitialCondition::calculateNewPhi(
  std::vector<std::vector<AlgebraElement>> const & oldPhi, Simulation const & simulation) const
{
  using Direction = Settings::Direction;

  if(oldPhi.empty() == false)
  {
    const index nLongitudinalCells = oldPhi.size();
    const index nTransversalCells  = oldPhi[0].size();
    std::vector<std::vector<AlgebraElement>> newPhi(nLongitudinalCells,
                                                    std::vector<AlgebraElement>(nTransversalCells));
    const rational factor = simulation.timeStep / simulation.latticeSpacings[Direction::x];

    for(index iL = 0; iL < nLongitudinalCells - 1; ++iL)
    {
      for(index iT = 0; iT < nTransversalCells; ++iT)
      {
        // Use linear interpolation to shift �oldPhi� one time step to the left
        newPhi[iL][iT] = oldPhi[iL][iT] * (1.0 - factor) + oldPhi[iL + 1][iT] * factor;
      }
    }
    std::copy(oldPhi[nLongitudinalCells - 1].begin(), oldPhi[nLongitudinalCells - 1].end(),
              newPhi[nLongitudinalCells - 1].begin());

    return newPhi;
  }
  else
  {
    return std::vector<std::vector<AlgebraElement>>();
  }
}


/// @brief  Initializes the gauge links in and behind the nucleon volume.
///
/// It is assumed that all gauge links in and behind the nucleon volume are 1 at the start of this
/// function.
void CgcInitialCondition::initializeGaugeLinks(Simulation & simulation,
                                               std::vector<GroupElement> const & V,
                                               const TimeIndex timeIndex) const
{
  using Direction = Settings::Direction;

  if(V.empty() == false)
  {
    const auto leftBoundary = initialChargeDensity->leftBoundary();
    const auto leftIndexBoundary = simulation.getIndex({leftBoundary, 0, 0});
    auto &     U = (timeIndex == TimeIndex::previous ? simulation.oldU : simulation.newU);

    // Initialize gauge links in the nucleon volume
    index x = leftIndexBoundary;
    for(index i = 0; i < V.size(); ++i)
    {
      auto shiftedIndex1 = simulation.shift(i, Direction::y, Orientation::positive);
      auto shiftedIndex2 = simulation.shift(i, Direction::z, Orientation::positive);
      U[x][Direction::y] = V[i] * V[shiftedIndex1].dagger();
      U[x][Direction::z] = V[i] * V[shiftedIndex2].dagger();
      x++;
    }

    // Initialize the gauge links behind the nucleon
    if(velocity == Velocity::positive)
    {
      // Take the left-most transversal plane of the nucleon
      auto sourceBegin = U.begin() + leftIndexBoundary;
      auto sourceEnd   = U.begin() + simulation.getIndex({leftBoundary + 1, 0, 0});
      // And copy it to every plane to the left of it
      for(index iL = 0; iL < leftBoundary; ++iL)
      {
        auto destination = U.begin() + simulation.getIndex({iL, 0, 0});
        std::copy(sourceBegin, sourceEnd, destination);
      }
    }
    else
    {
      // Take the right-most transversal plane of the nucleon
      const auto rightBoundary = initialChargeDensity->rightBoundary();
      auto sourceBegin = U.begin() + simulation.getIndex({rightBoundary - 1, 0, 0});
      auto sourceEnd   = U.begin() + simulation.getIndex({rightBoundary, 0, 0});
      // And copy it to every plane to the right of it
      for(index iL = rightBoundary; iL < simulation.gridSize[Direction::x]; ++iL)
      {
        auto destination = U.begin() + simulation.getIndex({iL, 0, 0});
        std::copy(sourceBegin, sourceEnd, destination);
      }
    }
  }
}


/// @brief  Initializes the electric field inside the nucleon volume.
///
/// It is assumed that the E-field inside the nucleon volume is 0 at the start of this function.
void openpixi::CgcInitialCondition::initializeElectricField(Simulation & simulation) const
{
  using Direction = Settings::Direction;

  const auto leftBoundary  = initialChargeDensity->leftBoundary();
  const auto rightBoundary = initialChargeDensity->rightBoundary();
  const auto leftIndexBoundary  = simulation.getIndex({leftBoundary, 0, 0});
  const auto rightIndexBoundary = simulation.getIndex({rightBoundary, 0, 0});

  // Initialize electric field in the nucleon volume
  for(index x = leftIndexBoundary; x < rightIndexBoundary; ++x)
  {
    simulation.E[x][Direction::y] = -(simulation.newU[x][Direction::y] *
                                      simulation.oldU[x][Direction::y].dagger()).log();
    simulation.E[x][Direction::z] = -(simulation.newU[x][Direction::z] *
                                      simulation.oldU[x][Direction::z].dagger()).log();
  }
}


/// @brief  Spawns super particles in the nucleon volume with a charge obeying the Gauss constraint.
template<Velocity v>
void CgcInitialCondition::spawnSuperParticles(Simulation & simulation,
                                              std::vector<SuperParticle<v>> & superParticles) const
{
  const auto  leftBoundary = initialChargeDensity->leftBoundary();
  const auto  rightBoudary = initialChargeDensity->rightBoundary();
  const index nTransversalCells = simulation.accumulatedCellCounts[1];
  const int   n = simulation.nParticlesPerCell;
  const rational factor = 1.0 / (simulation.unitFactorRho * n);

  superParticles.reserve(superParticles.size() + (rightBoudary - leftBoundary) * n);

  auto positionIndex = simulation.getIndex({leftBoundary, 0, 0});
  for(index iL = leftBoundary; iL < rightBoudary; ++iL)
  {
    // Spawn the super particle with the lowest inter cell position and fill its charges
    SuperParticle<v> superParticle(simulation, iL, -(n - 1) / 2);
    for(index iT = 0; iT < nTransversalCells; ++iT)
    {
      superParticle.Q[iT] = DivE::calculateDivE(simulation, positionIndex) * factor;
      positionIndex++;
    }
    superParticles.push_back(superParticle);

    // Copy this particle to the super particle vector until the second last inter cell position is
    // reached
    superParticle.interCellPosition++;
    while(superParticle.interCellPosition < n / 2)
    {
      superParticles.push_back(superParticle);
      superParticle.interCellPosition++;
    }
    // Move the last particle to the vector
    superParticles.push_back(std::move(superParticle));
  }
}
}