#pragma once

#include <memory>
#include <vector>
#include <array>
#include <string>

#include "InitialCondition.h"
#include "../CustomTypes.h"
#include "../Utility.h"
#include "../GroupAndAlgebra.h"
#include "../Settings.h"
#include "../SuperParticle.h"

namespace openpixi
{
class Simulation;
class InitialChargeDensity;
enum class TimeIndex;

/// @brief  An initial condition for a nucleon in the color-glass condensate model
///
/// The nucleon is traveling at the speed of light along the positive or negative x-direction. The
/// color current and gauge fields representing this nucleon are given by
/// @f[j^{a,\mu}=\rho^a(t,x,y,z)s^\mu,@f]
/// @f[A^{a,\mu}=\phi^a(t,x,y,z)s^\mu,@f]
/// with the initial charge density @f$\rho^a(t,x,y,z)@f$ and the light-like vector
/// @f$s^\mu=(1,v,0,0)^\mu@f$, where @f$v=\pm1@f$. To solve the equations of motion
/// @f${(D_\mu)_a}^bF_b^{\mu\nu}=j_a^\nu@f$ in the Lorenz gauge, @f$\phi@f$ must obey the 2D Poisson
/// equation in every transversal plane
/// @f[-\Delta_T\phi^a(t,x,y,z)=\rho^a(t,x,y,z).@f]
/// The gauge transformation from the Lorenz gauge to the temporal gauge is then given by
/// @f[V(t,x,y,z)=\mathcal{P}\exp\left(-\mathrm{i}g\int_{-\infty}^x\phi^a(t,x',y,z)t_a\mathrm{d}x'
/// \right),@f]
/// where @f$\mathcal{P}@f$ stands for path-ordering. With that, the gauge links can be calculated:
/// @f[U_{x,i}(t\pm a_0/2)=V_x(t\pm a_0/2)V_{x+i}^\dagger(t\pm a_0/2),\ i=2,3\quad U_{x,1}=1.@f]
/// The electric field follows from the lattice equations of motion for the gauge links:
/// @f[E_x^{a,i}t_a=-\log(U_{x,i}(t+a_0/2)U_{x,i}^\dagger(t-a_0/2)),\ i=2,3\quad E_x ^{a,1}=0.@f]
/// Finally super particles are spawned with the dimensionless charge
/// @f[Q_x=\frac{\rho_x}{n\cdot\text{unitFactorRho}},@f]
/// where @f$\rho_x@f$ is the value returned by GaussConstraint::calculateDivE() and @f$n@f$ is the
/// number of paricles per cell. With this, the Gauss constraint vanishes exactly at the start of
/// the simulation.
///
/// @see    utility::solve2dPoissonEquation()
class CgcInitialCondition : public InitialCondition
{
public:
  std::unique_ptr<const InitialChargeDensity> initialChargeDensity;
  const Velocity velocity;
  const rational infraredRegulator;
  const rational cutoffValue;
  const utility::Cutoff cutoff;

  CgcInitialCondition(std::unique_ptr<const InitialChargeDensity> initialChargeDensity,
                      const Velocity velocity, const rational infraredRegulator,
                      const rational cutoffValue, utility::Cutoff cutoff);
  ~CgcInitialCondition() = default;

  void apply(Simulation & simulation);
  std::string getDescription(void) const;

private:
  std::vector<std::vector<AlgebraElement>>
  calculateOldPhi(std::vector<std::vector<AlgebraElement>> & rho,
                  Simulation const & simulation) const;

  std::vector<std::vector<GroupElement>>
  calculateGaugeTransformation(std::vector<std::vector<AlgebraElement>> const & phi,
                               Simulation const & simulation) const;
  GroupElement calculateRefinedWilsonLine(AlgebraElement const & leftField,
                                          AlgebraElement const & rightField,
                                          const int refinement,
                                          Simulation const & simulation) const;
  std::vector<GroupElement>
  convertGaugeTransformation(std::vector<std::vector<GroupElement>> const & V) const;
  std::vector<std::vector<AlgebraElement>>
  calculateNewPhi(std::vector<std::vector<AlgebraElement>> const & oldPhi,
                  Simulation const & simulation) const;

  void initializeGaugeLinks(Simulation & simulation, std::vector<GroupElement> const & V,
                            const TimeIndex timeIndex) const;
  void initializeElectricField(Simulation & simulation) const;

  template<Velocity v>
  void spawnSuperParticles(Simulation & simulation,
                           std::vector<SuperParticle<v>> & superParticles) const;
};
}