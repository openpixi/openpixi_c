#include "RandomElectricField.h"

#include <random>
#include <sstream>
#include <functional>

#include "../GroupAndAlgebra.h"
#include "../Simulation.h"

namespace openpixi
{
RandomElectricField::RandomElectricField(const rational amplitude) : amplitude(amplitude)
{
}


RandomElectricField::~RandomElectricField()
{
}


/// @brief  Returns an AlgebraElement where each parameter is uniformly distributed between
///         @f$\pm@f$`amplitude`.
AlgebraElement RandomElectricField::createRandomAlgebraElement(rational amplitude)
{
  using namespace std;
  static random_device randomDevice;
  static mt19937 engine(randomDevice());

  uniform_real_distribution<> distribution(-amplitude, amplitude);
  auto getRandomNumber = std::bind(distribution, engine);
  return AlgebraElement(getRandomNumber(), getRandomNumber(), getRandomNumber());
}


/// @brief  Sets the electric field at each grid point of the simulation
void RandomElectricField::apply(Simulation & simulation)
{
  const index nCells = simulation.accumulatedCellCounts[0];

  for(index positionIndex = 0; positionIndex < nCells; ++positionIndex)
  {
    for(index direction = 0; direction < simulation.nDimensions; ++direction)
    {
      simulation.E[positionIndex][direction] = createRandomAlgebraElement(amplitude);
    }
  }
}


std::string RandomElectricField::getDescription(void) const
{
  std::stringstream description;
  description << "random electric field: " << std::endl <<
    " amplitude = " << amplitude;
  return description.str();
}
}