#pragma once

#include <array>
#include <string>

#include "../CustomTypes.h"

#include "../GroupAndAlgebra.h"
#include "../Settings.h"

#include "InitialCondition.h"

namespace openpixi
{
class Simulation;

/// @brief  A simple initial condition for testing the calculation of the Gauss constraint.
///
/// It sets the electric field according to the following equation:
/// @f[E_{x,i}^a=\frac{\rho^aA_i\sum_{j=1}^{3}n_jx_ja_j}{\text{unitFactorE}_i},@f]
/// which corresponds to a field, that increases linearly in direction of the @a normalVector
/// @f$n_i@f$, has spacial components according to the @a directionVector @f$A_i@f$ and a
/// @a colorAmplitude @f$\rho^a@f$. The analytic result of the Gauss constraint is also calculated.
/// The gauge links are not changed.
///
/// @see    GaussConstraint
class GaussViolationExample : public InitialCondition
{
public:
  rational amplitude;
  AlgebraElement colorAmplitude;
  std::array<rational, Settings::nDimensions> directionVector;
  std::array<rational, Settings::nDimensions> normalVector;
  rational analyticalResult;

  explicit GaussViolationExample(const rational amplitude);
  ~GaussViolationExample();

  void apply(Simulation & simulation);
  std::string getDescription(void) const;
};
}