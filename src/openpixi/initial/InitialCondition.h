#pragma once

#include <string>

namespace openpixi
{
class Simulation;

/// @brief  An abstract base class for all initial conditions.
class InitialCondition
{
public:
  virtual ~InitialCondition()
  {
  }


  /// @brief  Applies the initial condition to the Simulation object.
  virtual void apply(Simulation & simulation) = 0;
  /// @brief  Returns a string describing the initial condition with all its parameters.
  virtual std::string getDescription(void) const = 0;
};
}