#include "CoherentMvModel.h"

#include <cmath>
#include <random>
#include <algorithm>
#include <sstream>
#include <functional>

#include "../Simulation.h"

namespace openpixi
{
/// @brief  Constructs a charge density with the given parameters.
///
/// The boundaries of this initial charge density are `longitudinalPosition`@f$\pm@f$ 3 `width`.
///
/// @param[in]  longitudinalPosition  Position of the nucleon's center in units of @f$a_1@f$.
/// @param[in]  width                 Width of the Gaussion profile in units of @f$a_1@f$.
/// @param[in]  mu                    Parameter of the MV model in physical units.
/// @param[in]  settings              Settings of the current simulation.
/// @param[in]  seed                  Seed for the random number generator. If 0, the seed is
///                                   determined randomly.
CoherentMvModel::CoherentMvModel(const rational longitudinalPosition, const rational width,
                                 const rational mu, Settings const & settings, const int seed) :
  InitialChargeDensity(settings, static_cast<index>(longitudinalPosition - widthFactor * width),
                       static_cast<index>(longitudinalPosition + widthFactor * width),
                       settings.gridSize[Settings::Direction::y] *
                       settings.gridSize[Settings::Direction::z]),
  longitudinalPosition_(longitudinalPosition), width_(width), mu_(mu),
  couplingConstant_(settings.couplingConstant),
  transversalLatticeSpacing_(std::sqrt(settings.latticeSpacings[Settings::Direction::y] *
                                       settings.latticeSpacings[Settings::Direction::z])),
  seed_(seed)
{
}


/// @brief  Returns a charge density according to the coherent MV model in units of
///         @f$(a_2 a_3)^{-1}@f$
std::vector<std::vector<AlgebraElement>>
openpixi::CoherentMvModel::initialize(void) const
{
  static const rational pi = std::acos(-1.0);
  const index nLongitudinalCells = rightBoundary() - leftBoundary();
  std::vector<std::vector<AlgebraElement>> rho(nLongitudinalCells,
                                               std::vector<AlgebraElement>(nTransversalCells));
  if(nLongitudinalCells > 0)
  {
    // Get a predetermined seed or a random number (non-deterministic if hardware supports it)
    std::random_device randomDevice;
    auto seed = (seed_ == 0 ? randomDevice() : seed_);

    // Seed a 64-bit Mersenne Twister pseudorandom number generator with it
    std::mt19937_64 engine(seed);
    // Create a normal distribution with mean = 0 and standard deviation = g * mu / a_T
    std::normal_distribution<rational> normalDistribution(0.0, couplingConstant_ * mu_ /
                                                          transversalLatticeSpacing_);
    auto getRandomNumber = std::bind(normalDistribution, engine);

    // Initialize rho with random values in the transverse direction
    for(index i = 0; i < nTransversalCells; ++i)
    {
      // Rho has dimension 1 / L^2 because [width] = [mu / a_T] = 1 / L^2 and the random number must
      // have the same dimension as its standard deviation
      rho[0][i] = AlgebraElement(getRandomNumber(), getRandomNumber(), getRandomNumber());
    }
    // Copy them in longitudinal direction
    for(auto & element : rho)
    {
      if(&element != &rho[0])
      {
        std::copy(rho[0].begin(), rho[0].end(), element.begin());
      }
    }

    // Multiply rho with a Gaussian profile in the longitudinal direction
    for(index iX = 0; iX < nLongitudinalCells; ++iX)
    {
      const rational gaussProfile = std::exp(-(iX + leftBoundary() - longitudinalPosition_) *
                                             (iX + leftBoundary() - longitudinalPosition_) /
                                             (2.0 * width_ * width_)) /
                                    (std::sqrt(2.0 * pi) * width_);

      // Dimension of rho is still 1 / L^2 because the Gaussian profile uses dimensionless
      // quantities
      for(auto & element : rho[iX])
      {
        element *= gaussProfile;
      }
    }
  }
  return rho;
}


std::string CoherentMvModel::getDescription(void) const
{
  std::stringstream description;
  description << "coherent MV model: " << std::endl <<
    " longitudinalPosition = " << longitudinalPosition_ << std::endl <<
    " width = " << width_ << std::endl <<
    " mu = " << mu_ << std::endl <<
    " seed = " << seed_;
  return description.str();
}
}