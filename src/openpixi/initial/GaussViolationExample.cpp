#include "GaussViolationExample.h"

#include <random>
#include <sstream>
#include <functional>

#include "../Simulation.h"

namespace openpixi
{
/// @brief  Sets uniformly random values for @a colorAmplitude, @a directionVector and
///         @a normalVector.
GaussViolationExample::GaussViolationExample(const rational amplitude) : amplitude(amplitude),
  analyticalResult(0.0)
{
  // Set random parameters
  std::random_device randomDevice;
  std::mt19937 engine(randomDevice());
  std::uniform_real_distribution<> distribution(-amplitude, amplitude);
  auto getRandomNumber = std::bind(distribution, engine);

  colorAmplitude  = AlgebraElement(getRandomNumber(), getRandomNumber(), getRandomNumber());
  directionVector = {getRandomNumber(), getRandomNumber(), getRandomNumber()};
  normalVector    = {getRandomNumber(), getRandomNumber(), getRandomNumber()};
}


GaussViolationExample::~GaussViolationExample()
{
}


/// @brief  Sets the electric field at each grid point of the simulation and calculates the analytic
///         result of the Gauss constraint.
void GaussViolationExample::apply(Simulation & simulation)
{
  const index nCells   = simulation.accumulatedCellCounts[0];
  const auto  couplingConstant = simulation.couplingConstant;
  const auto  timeStep = simulation.timeStep;

  for(index positionIndex = 0; positionIndex < nCells; ++positionIndex)
  {
    const auto position   = simulation.getPosition(positionIndex);

    rational   dotProduct = 0.0;
    for(index direction = 0; direction < simulation.nDimensions; ++direction)
    {
      dotProduct += normalVector[direction] * position[direction] *
                    simulation.latticeSpacings[direction];
    }

    for(index direction = 0; direction < simulation.nDimensions; ++direction)
    {
      simulation.E[positionIndex][direction]  = AlgebraElement(colorAmplitude);
      simulation.E[positionIndex][direction] *= directionVector[direction] * dotProduct;

      // Lattice unit factors
      simulation.E[positionIndex][direction] *= 1. / simulation.unitFactorsE[direction];
    }
  }

  // Calculate analytical result
  rational dotProduct = 0.0;
  for(index direction = 0; direction < simulation.nDimensions; ++direction)
  {
    dotProduct += normalVector[direction] * directionVector[direction];
  }
  rational squaredColorAmplitude = colorAmplitude.square();
  analyticalResult = squaredColorAmplitude * dotProduct * dotProduct;
}


std::string openpixi::GaussViolationExample::getDescription(void) const
{
  std::stringstream description;
  description << "Gauss violation example: " << std::endl <<
    " amplitude = " << amplitude;
  return description.str();
}
}