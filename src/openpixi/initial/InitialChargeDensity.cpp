#include "InitialChargeDensity.h"

#include "../Settings.h"

namespace openpixi
{
/// @brief  Construtor
///
/// This constructor ensures that the following conditions are true.
/// @f[0\le \text{leftBoundary}<\text{longitudinalGridSize}@f]
/// @f[\text{leftBoundary}\le \text{rightBoundary}\le\text{longitudinalGridSize}@f]
///
/// @param[in]  settings              Settings of the current Simulation.
/// @param[in]  leftBoundary          Left boundary of the volume the charge density lies within.
/// @param[in]  rightBoundary         Right boundary of the volume the charge density lies within.
/// @param[in]  nTransversalCells     Number of cells in each transversal plane.
InitialChargeDensity::InitialChargeDensity(Settings const & settings, const index leftBoundary,
                                           const index rightBoundary,
                                           const index nTransversalCells) :
  nTransversalCells(nTransversalCells)
{
  if(leftBoundary < settings.leftBoundary())
  {
    leftBoundary_ = settings.leftBoundary();
  }
  else if(leftBoundary >= settings.rightBoundary())
  {
    leftBoundary_ = settings.rightBoundary() - 1;
  }
  else
  {
    leftBoundary_ = leftBoundary;
  }

  if(rightBoundary > settings.rightBoundary())
  {
    rightBoundary_ = settings.rightBoundary();
  }
  else if(rightBoundary < leftBoundary)
  {
    rightBoundary_ = leftBoundary_;
  }
  else
  {
    rightBoundary_ = rightBoundary;
  }
}


index InitialChargeDensity::leftBoundary(void) const
{
  return leftBoundary_;
}


index InitialChargeDensity::rightBoundary(void) const
{
  return rightBoundary_;
}
}