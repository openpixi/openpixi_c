#pragma once

#include <vector>

#include "InitialChargeDensity.h"
#include "../CustomTypes.h"
#include "../GroupAndAlgebra.h"

namespace openpixi
{
class Settings;

/// @brief  An initial charge density of a nucleon based on the McLerran-Venugopalan model with
///         choherent longitudinal structure.
///
/// The charge density is of the form
/// @f[\rho^a_x=f(x_1-t)\hat{\rho}^a_{x_T},@f]
/// where the longitudinal envelope is a Gaussian profile
/// @f[f(x_1-t)=\frac{1}{\sqrt{2\pi}\sigma_1}\exp\left(-\frac{(x_1-t-x_0)^2}{2\sigma_1^2}\right),@f]
/// with width @f$\sigma_1@f$ and center @f$x_0@f$, and the transversal part
/// @f$\hat{\rho}^a_{x_T}@f$ is normally distributed with the following correlation function:
/// @f[\langle\hat{\rho}^a_{x_T},\hat{\rho}^b_{y_T}\rangle=\delta^{ab}\delta_{x_Ty_T}
/// \frac{g^2\mu^2}{a_T^2}.@f]
/// This results in the distribution's standard deviation being
/// @f[\sigma_T=\frac{g\mu}{a_T},@f]
/// where @f$a_T@f$ is the transversal lattice spacing and @f$\mu@f$ is a parameter of the MV model.
class CoherentMvModel : public InitialChargeDensity
{
public:
  /// @brief  Factor between the width of the Gaussian profile and the width of the charge density.
  static constexpr rational widthFactor = 8.0;

  CoherentMvModel(const rational longitudinalPosition, const rational width, const rational mu,
                  Settings const & settings, const int seed = 0);
  ~CoherentMvModel() = default;

  std::vector<std::vector<AlgebraElement>> initialize(void) const;
  std::string getDescription(void) const;

private:
  const rational longitudinalPosition_;   // Center of the Gauss function in units of a_1
  const rational width_;                  // Width of the Gauss function in units of a_1
  const rational mu_;                     // Parameter of the MV model in physical units
  const rational couplingConstant_;
  const rational transversalLatticeSpacing_;
  const int seed_;                        // Seed for the random number generator
};
}