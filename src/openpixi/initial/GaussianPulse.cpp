#include "GaussianPulse.h"

#include <cmath>
#include <sstream>

#include "../Simulation.h"

namespace openpixi
{
GaussianPulse::GaussianPulse(void) : width(4.0), colorVector(1.0, 0.0, 0.0),
  directionVector{1.0, 0.0, 0.0}, amplitudeVector{0.0, 1.0, 0.0}, center{0.0, 0.0, 0.0}
{
}


GaussianPulse::~GaussianPulse()
{
}


/// @brief  Sets the electric field at each grid point of the simulation
void GaussianPulse::apply(Simulation & simulation)
{
  const index nCells = simulation.accumulatedCellCounts[0];

  for(index i = 0; i < nCells; ++i)
  {
    rational dotProduct = 0.0;
    auto     position   = simulation.getPosition(i);
    for(index j = 0; j < Simulation::nDimensions; ++j)
    {
      rational difference = (position[j] - center[j]) * directionVector[j];
      dotProduct += difference * difference;
    }
    for(index j = 0; j < Simulation::nDimensions; ++j)
    {
      simulation.E[i][j] = colorVector * std::exp(-0.5 * dotProduct / (width * width)) *
                           (1. / simulation.unitFactorsE[j]) * amplitudeVector[j];
    }
  }
}


std::string openpixi::GaussianPulse::getDescription(void) const
{
  std::stringstream description;
  description << "Gaussian pulse: " << std::endl <<
    " width = " << width << std::endl <<
    " center = (" << center[0] << ", " << center[1] << ", " << center[2] << ")" << std::endl <<
    " colorVector = (" << colorVector.getComponent(0) << ", " <<
    colorVector.getComponent(1) << ", " <<
    colorVector.getComponent(2) << ")" << std::endl <<
    " directionVector = (" << directionVector[0] << ", " << directionVector[1] << ", " <<
    directionVector[2] << ")" << std::endl <<
    " amplitudeVector = (" << amplitudeVector[0] << ", " << amplitudeVector[1] << ", " <<
    amplitudeVector[2] << ")" << std::endl;

  return description.str();
}
}