#pragma once

#include <array>
#include <string>

#include "../CustomTypes.h"

#include "../GroupAndAlgebra.h"
#include "../Settings.h"

#include "InitialCondition.h"

namespace openpixi
{
class Simulation;

/// @brief  A simple initial condition with an electric field in the form of a 1-dimensional
///         Gaussian pulse.
///
/// The Gaussian pulse is defined by its @a center @f$\mu_i@f$, @a directionVector @f$r_i@f$,
/// @a width @f$\sigma@f$, @a colorVector @f$\rho^a@f$ and spacial @a amplitudeVector @f$A_i@f$
/// according to the following equation:
/// @f[E_{x,i}^a=\frac{\rho^aA_i}{\text{unitFactorE}_i}\exp\left(-\frac{\sum_{j=1}^{3}((x_j-\mu_j)
/// r_j)^2}{2\sigma^2}\right).@f]
/// The gauge links are not changed.
class GaussianPulse : public InitialCondition
{
public:
  rational width;
  AlgebraElement colorVector;
  std::array<rational, Settings::nDimensions> directionVector;
  std::array<rational, Settings::nDimensions> amplitudeVector;
  std::array<rational, Settings::nDimensions> center;

  GaussianPulse(void);
  ~GaussianPulse();

  void apply(Simulation & simulation);
  std::string getDescription(void) const;
};
}