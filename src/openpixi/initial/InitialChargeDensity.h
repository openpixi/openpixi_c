#pragma once

#include <vector>
#include <string>

#include "../CustomTypes.h"
#include "../GroupAndAlgebra.h"

namespace openpixi
{
class Settings;

/// @brief  An abstract base class for all initial charge densities.
class InitialChargeDensity
{
public:
  const index nTransversalCells;

  virtual ~InitialChargeDensity() = default;
  InitialChargeDensity(Settings const & settings, const index leftBoundary,
                       const index rightBoundary, const index nTransversalCells);
  /// @brief  Returns a charge density for a partial area of the simulation grid.
  virtual std::vector<std::vector<AlgebraElement>> initialize(void) const = 0;
  virtual std::string getDescription(void) const = 0;

  /// @name   Boundaries of the charge density
  ///@{
  /// The charge density lies within the volume defined by
  /// @f[i_L\in [\text{leftBoundary}, \text{rightBoundary}),@f]
  /// where @f$i_L@f$ is the longitudinal index of the simulation grid.
  index leftBoundary(void) const;
  index rightBoundary(void) const;
  ///@}

private:
  index leftBoundary_;
  index rightBoundary_;
};
}