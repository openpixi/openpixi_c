#pragma once

#include <string>

#include "../CustomTypes.h"

#include "InitialCondition.h"

namespace openpixi
{
class AlgebraElement;
class Simulation;

/// @brief  A simple initial condition with random electric fields.
///
/// The parameters of the electric field are uniformly random distributed and generated for each
/// grid point. The gauge links are not changed.
class RandomElectricField : public InitialCondition
{
public:
  rational amplitude;

  explicit RandomElectricField(const rational amplitude);
  ~RandomElectricField();

  void apply(Simulation & simulation);
  std::string getDescription(void) const;

  AlgebraElement createRandomAlgebraElement(rational amplitude);
};
}