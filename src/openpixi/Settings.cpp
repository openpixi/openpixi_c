#include "Settings.h"

namespace openpixi
{
/// @brief  Constructs a Settings object with the given parameters
///
/// The number of particles per cell is initialized to @f$a_1/a_0@f$. Depending on the
/// `boundaryCondition` @a left- and @a right(Index)Boundary either cover the whole simulation grid
/// or spare the very first and last transversal plane.
///
/// @param[in]  gridSize            @f$(n_1, n_2, n_3)@f$
/// @param[in]  latticeSpacings     @f$(a_1, a_2, a_3)@f$, in phys. units
/// @param[in]  t0                  Start time of the simulation
/// @param[in]  nTimeSteps          The number of time steps, the simulation will run for.
/// @param[in]  timeStep            @f$a_0@f$, in phys. units
/// @param[in]  couplingConstant    @f$g@f$
/// @param[in]  boundaryCondition   Determines the values of @a left- and @a right(Index)Boundary
Settings::Settings(const std::array<index, nDimensions> gridSize,
                   const std::array<rational, nDimensions> latticeSpacings, const int t0,
                   const int nTimeSteps, const rational timeStep, const rational couplingConstant,
                   const BoundaryCondition boundaryCondition) :
  gridSize(gridSize), latticeSpacings(latticeSpacings), t0(t0), nTimeSteps(nTimeSteps),
  timeStep(timeStep), couplingConstant(couplingConstant),
  nParticlesPerCell(static_cast<int>(latticeSpacings[Direction::x] / timeStep)),
  boundaryCondition(boundaryCondition)
{
  if(boundaryCondition == BoundaryCondition::periodic)
  {
    leftBoundary_  = 0;
    rightBoundary_ = gridSize[Direction::x];
  }
  else
  {
    leftBoundary_  = 1;
    rightBoundary_ = gridSize[Direction::x] - 1;
  }
  const int nTransversalCells = gridSize[Direction::y] * gridSize[Direction::z];
  leftIndexBoundary_  = leftBoundary_ * nTransversalCells;
  rightIndexBoundary_ = rightBoundary_ * nTransversalCells;
}


/// @brief  Returns the lower bound for loops over longitudinal indexes in the active grid.
///
/// @see    Settings::boundaryCondition
index Settings::leftBoundary(void) const
{
  return leftBoundary_;
}


/// @brief  Returns the upper bound for loops over longitudinal indexes in the active grid.
///
/// @see    Settings::boundaryCondition
index Settings::rightBoundary(void) const
{
  return rightBoundary_;
}


/// @brief  Returns the lower bound for loops over position indexes in the active grid.
///
/// @see    Settings::boundaryCondition
index Settings::leftIndexBoundary(void) const
{
  return leftIndexBoundary_;
}


/// @brief  Returns the lower bound for loops over position indexes in the active grid.
///
/// @see    Settings::boundaryCondition
index Settings::rightIndexBoundary(void) const
{
  return rightIndexBoundary_;
}
}