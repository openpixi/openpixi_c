#pragma once

#include "CustomTypes.h"

#include <vector>
#include <ostream>

#include "GroupAndAlgebra.h"
#include "Settings.h"

namespace openpixi
{
class Simulation;

enum class Velocity
{
  negative = -1,  ///< @f$v=-1@f$
  positive = 1    ///< @f$v=+1@f$
};


std::ostream & operator<<(std::ostream & stream, Velocity const & velocity);


/// @brief  A class template describing all color charged point like particles in the same
///         transversal plane.
///
/// It uses the nearest-grid-point interpolation scheme to map the particles' charges to the local
/// charge density. The longitudinal direction is @f$x@f$.
/// @param  v   Is either Velocity::positive or Velocity::negative and describes right or left
///             moving super particles respectively.
template<Velocity v>
class SuperParticle
{
public:
  /// @brief  Previous longitudinal grid position, the super particle was interpolated to.
  index previousPosition;
  /// @brief  Current longitudinal grid position, the super particle is interpolated to.
  index currentPosition;
  /// @brief  Next longitudinal grid position, the super particle will be interpolated to.
  index nextPosition;
  /// @brief  Actual longitudinal grid position of the super particle relative to @a currentPosition
  ///
  /// This means the actual longitudinal position of the super particle is calculated like
  /// @f[x_1=a_1\left(\text{currentPosition}+
  /// \frac{\text{interCellPosition}}{\text{nParticlesPerCell}}\right)@f]
  int interCellPosition;

  /// @brief  @f$Q_{\text{dimensionless}} \cdot \text{unitFactorQ} = Q_{\text{physical}}@f$
  rational unitFactorQ;
  /// @brief  Dimensionless charges of the particles
  std::vector<AlgebraElement> Q;

  SuperParticle(Simulation const & simulation, const index position, const int interCellPosition);

  void updatePosition(Simulation const & simulation);
  void updateCharges(Simulation const & simulation);
  void interpolateChargeDensity(Simulation & simulation) const;
  void interpolateCurrentDensity(Simulation & simulation) const;
  void initializeChargeAndCurrentDensity(Simulation & simulation) const;
  int update(Simulation & simulation);
};
}