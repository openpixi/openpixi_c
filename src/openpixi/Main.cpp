#include "Main.h"


int main(void)
{
  using namespace openpixi;
  using Direction = Settings::Direction;
  using Cutoff    = utility::Cutoff;

  auto & mpi = mpiwrapper::Mpi::getInstance();
  int    nOpenMpThreads;

#pragma omp parallel
#pragma omp single
  nOpenMpThreads = omp_get_num_threads();

#if SAVE_STDOUT_IN_FILE == 1
  std::ofstream outputStream;
  std::stringstream fileName;
  fileName << std::setfill('0') << std::setw(2) << nOpenMpThreads << "_stdout" <<
    std::setfill('0') << std::setw(2) << mpi.worldRank() << ".txt";
  outputStream.open(fileName.str());
#else
  auto & outputStream = std::cout;
#endif

  mpi.writeWorldInfo(outputStream);
  utility::writeOpenMpInfo(outputStream);

  // Uncrustify-Off
  constexpr int nSimulations       = 4;
  const int     worldSize          = mpi.worldSize();
  const int     worldRank          = mpi.worldRank();
  const int     chunkSize          = nSimulations / worldSize;
  const int     minSimulationIndex = worldRank * chunkSize;
  const int     maxSimulationIndex = (worldRank == worldSize - 1 ?
                                      nSimulations :
                                      minSimulationIndex + chunkSize);
  // Uncrustify-On

  outputStream << "I do simulations [" << minSimulationIndex + 1 << "," << maxSimulationIndex + 1 <<
    ")." << std::endl;

  for(int i = minSimulationIndex; i < maxSimulationIndex; ++i)
  {
    auto begin = std::chrono::steady_clock::now();

    // --- SETTINGS --------------------------------------------------------------------------------
    // For stability, a_i / a_0 should be >= sqrt(nDimensions)
    // Natural units (c=1=h_bar) are used for all physical quntities with fm and 1/fm as base units.
    // 1GeV ~ 5/fm
    // Uncrustify-Off
    constexpr int      t0 = -128;
    constexpr int      nTimeSteps = 256;
    constexpr index    gridSizeX = 256;
    constexpr index    gridSizeY = 64;
    constexpr index    gridSizeZ = 64;
    constexpr rational latticeSpacingX = 0.04;
    constexpr rational latticeSpacingY = 0.04;
    constexpr rational latticeSpacingZ = 0.04;
    constexpr rational timeStep = latticeSpacingX / 2.0;
    constexpr rational couplingConstant = 2.0;
    constexpr auto     boundaryCondition = BoundaryCondition::fixed;
    // Uncrustify-On

    static_assert(utility::isInteger(latticeSpacingX / timeStep), "a_1 / a_0 must be integer!");
    Settings settings({gridSizeX, gridSizeY, gridSizeZ},
                      {latticeSpacingX, latticeSpacingY, latticeSpacingZ}, t0, nTimeSteps, timeStep,
                      couplingConstant, boundaryCondition);

    // --- INITIAL CONDITIONS ----------------------------------------------------------------------
    // Natural units (c=1=h_bar) are used for all physical quntities with fm and 1/fm as base units.
    // 1GeV ~ 5/fm
    // Uncrustify-Off
    constexpr rational width = 6.0;
    constexpr rational center = gridSizeX / 2.0 + static_cast<rational>(t0) / 2.0;
    constexpr rational mu = 3.232;                    // Physical units!
    constexpr int      seed = 0;                      // 0 = random seed
    constexpr auto     velocity = Velocity::positive;
    constexpr rational infraredRegulator = 1.0;       // Physical units!
    constexpr rational cutoffValue = 50.0;            // Physical units!
    constexpr auto     cutoff = Cutoff::hard;
    // Uncrustify-On

    auto coherentMvModel = std::make_unique<CoherentMvModel>(center, width, mu, settings, seed);
    auto cgcInitialCondition = std::make_shared<CgcInitialCondition>(std::move(coherentMvModel),
                                                                     velocity, infraredRegulator,
                                                                     cutoffValue, cutoff);
    settings.initialConditions.push_back(std::move(cgcInitialCondition));

    // Uncrustify-Off
    constexpr rational width2 = width;
    constexpr rational center2 = gridSizeX / 2.0 - static_cast<rational>(t0) / 2.0;
    constexpr rational mu2 = mu;
    constexpr int      seed2 = seed;
    constexpr auto     velocity2 = Velocity::negative;
    constexpr rational infraredRegulator2 = infraredRegulator;
    constexpr rational cutoffValue2 = cutoffValue;
    constexpr auto     cutoff2 = cutoff;
    // Uncrustify-On

    coherentMvModel = std::make_unique<CoherentMvModel>(center2, width2, mu2, settings, seed2);
    cgcInitialCondition = std::make_shared<CgcInitialCondition>(std::move(coherentMvModel),
                                                                velocity2, infraredRegulator2,
                                                                cutoffValue2, cutoff2);
    settings.initialConditions.push_back(std::move(cgcInitialCondition));

    // --- DIAGNOSTICS -----------------------------------------------------------------------------
    // Uncrustify-Off
    //auto energyDensity            = std::make_shared<EnergyDensity>(settings, false);
    auto projectedEnergyDensity   = std::make_shared<ProjectedEnergyDensity>(settings,
                                                                             true);
    //auto divE                     = std::make_shared<DivE>(settings, false);
    //auto gaussConstraint          = std::make_shared<GaussConstraint>(settings, divE, false);
    auto projectedGaussConstraint = std::make_shared<ProjectedGaussConstraint>(settings,
                                                                               true);
    //auto chargeDensity            = std::make_shared<ChargeDensity>(settings, false);
    //auto oldGaugeLinks            = std::make_shared<OldGaugeLinks>(settings, false);
    //auto oldCurrentDensity        = std::make_shared<OldCurrentDensity>(settings, false);
    //auto oldPlaquettes            = std::make_shared<OldPlaquettes>(settings, false);
    //auto electricField            = std::make_shared<ElectricField>(settings, false);
    //auto projectedPowerDensity    = std::make_shared<ProjectedPowerDensity>(settings,
    //                                                                        projectedEnergyDensity,
    //                                                                        true);
    //auto poyntingVector           = std::make_shared<PoyntingVector>(settings, false);
    //auto projectedPoyntingVector  = std::make_shared<ProjectedPoyntingVector>(settings,
    //                                                                          poyntingVector, true);
    //auto projectedDivS            = std::make_shared<ProjectedDivS>(settings, poyntingVector, true);
    auto projectedJInE            = std::make_shared<ProjectedJInE>(settings, true);
    //settings.diagnostics.push_back(energyDensity);
    settings.diagnostics.push_back(projectedEnergyDensity);
    //settings.diagnostics.push_back(divE);
    //settings.diagnostics.push_back(gaussConstraint);
    settings.diagnostics.push_back(projectedGaussConstraint);
    //settings.diagnostics.push_back(chargeDensity);
    //settings.diagnostics.push_back(oldGaugeLinks);
    //settings.diagnostics.push_back(oldCurrentDensity);
    //settings.diagnostics.push_back(oldPlaquettes);
    //settings.diagnostics.push_back(electricField);
    //settings.diagnostics.push_back(projectedPowerDensity);
    //settings.diagnostics.push_back(poyntingVector);
    //settings.diagnostics.push_back(projectedPoyntingVector);
    //settings.diagnostics.push_back(projectedDivS);
    settings.diagnostics.push_back(projectedJInE);
    // Uncrustify-On

    int error;
    std::stringstream fileName;
    fileName << "data" << std::setfill('0') << std::setw(2) << i;
    DataFile dataFile(fileName.str(), settings, error);
    if(error != 0)
    {
      return 1;
    }

    Simulation simulation(settings);
    simulation.applyInitialConditions();
#pragma omp parallel
    simulation.executeDiagnostics();

    // Print initial gauss constraint, total energy and start-up time
    outputStream << "Simulation " << i + 1 << "/" << nSimulations << std::endl;
    outputStream << "initial total energy     = " << projectedEnergyDensity->totalEnergy <<
      std::endl;
    outputStream << "initial gauss constraint = " <<
      projectedGaussConstraint->totalGaussConstraint << std::endl;

    auto end = std::chrono::steady_clock::now();

    outputStream << "start-up time            = ";
    outputStream << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    outputStream << "ms" << std::endl;

    // --- BEGIN SIMULATION LOOP -------------------------------------------------------------------
    begin = std::chrono::steady_clock::now();
    bool    errorHasOccured = false;

#pragma omp parallel
    for(int t = t0; t < t0 + nTimeSteps; ++t)
    {
      if(errorHasOccured)
      {
        continue;
      }

      // Execute diagnostics and write them to a data file
      simulation.executeDiagnostics();

#pragma omp single
      {
        error = simulation.writeDiagnosticsToDataFile(dataFile, t);
        if(error != DataFile::noError)
        {
          errorHasOccured = true;
        }
      }

      simulation.evolve();
    }

    if(errorHasOccured)
    {
      return error;
    }
    end = std::chrono::steady_clock::now();
    // --- END SIMULATION LOOP ---------------------------------------------------------------------

    // Print final gauss constraint, total energy and execution time
    outputStream << "final total energy       = " << projectedEnergyDensity->totalEnergy <<
      std::endl;
    outputStream << "final gauss constraint   = " <<
      projectedGaussConstraint->totalGaussConstraint << std::endl;
    outputStream << "integrated total J*E     = " <<
      projectedJInE->integratedTotalJInE << std::endl;
    outputStream << "execution time           = ";
    outputStream << std::chrono::duration_cast<std::chrono::milliseconds>(end - begin).count();
    outputStream << "ms" << std::endl << std::endl;
  }

#if SAVE_STDOUT_IN_FILE == 1
  outputStream.close();
#endif
  return 0;
}