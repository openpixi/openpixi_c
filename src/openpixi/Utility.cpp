#include "Utility.h"

#include <array>
#include <complex>
#include <cmath>

#include <fftw3.h>

namespace openpixi
{
namespace utility
{
/// @brief  Writes a string containing a one word description of the given cutoff to the given
///         output stream.
std::ostream & operator<<(std::ostream & stream, Cutoff const & cutoff)
{
  switch(cutoff)
  {
    default:
    case Cutoff::none:
      stream << "none";
      break;
    case Cutoff::hard:
      stream << "hard";
      break;
  }
  return stream;
}


/// @brief  Solves the 2d Poisson equation @f$-\Delta\phi(x,y)=\rho(x,y)@f$ in physical units.
///
/// This is done by Fourier transformation. The inverse Laplace operator in momentum space can be
/// regulated with a mass term:
/// @f[-\Delta^{-1}\rightarrow\frac{1}{k^2+m^2}.@f]
/// It is also possible to introduce a hard momentum cutoff:
/// @f[\phi(k)=0,\ \forall |k|>\Lambda.@f]
/// @param[in]  rho               @f$\rho(x,y)@f$ in units of @f$(a_x a_y)^{-1}@f$
/// @param[in]  gridSizeX         Grid size in x-direction.
/// @param[in]  gridSizeY         Grid size in y-direction.
/// @param[in]  latticeSpacingX   @f$a_x@f$, lattice spacing in x-direction in phys. units.
/// @param[in]  latticeSpacingY   @f$a_y@f$, lattice spacing in y-direction in phys. units.
/// @param[in]  infraredRegulator @f$m@f$ in phys. units
/// @param[in]  cutoff            Determines if a momentum cutoff is used or not.
/// @param[in]  cutoffValue       @f$\Lambda@f$
/// @return     @f$\phi(x,y)@f$
///
/// @note   Here @f$x@f$ and @f$y@f$ correspond to the two dimensions of the plane in which the
///         Poisson equation is solved. This does not need to (and usually does not) coincide with
///         the x- and y-directions in the three-dimensional simulation grid.
std::vector<AlgebraElement> solve2dPoissonEquation(std::vector<AlgebraElement> & rho,
                                                   const index gridSizeX, const index gridSizeY,
                                                   const rational latticeSpacingX,
                                                   const rational latticeSpacingY,
                                                   const rational infraredRegulator,
                                                   Cutoff cutoff, const rational cutoffValue)
{
  static const double pi = std::acos(-1.0);
  // Reciprocal lattice spacings in physical units
  const double dKx = 2.0 * pi / (gridSizeX * latticeSpacingX);
  const double dKy = 2.0 * pi / (gridSizeY * latticeSpacingY);
  // Also in physical units
  const double mSquared = infraredRegulator * infraredRegulator;
  const double lambdaSquared = cutoffValue * cutoffValue;
  const index  nCells   = gridSizeX * gridSizeY;

  std::vector<double> fourierInput(nCells);
  std::vector<std::complex<double>> fourierOutput(gridSizeX * (gridSizeY / 2 + 1));
  std::vector<AlgebraElement> phi(nCells);

  if(cutoffValue < 0)
  {
    cutoff = Cutoff::none;
  }

  // Solve the Poisson equation for every component rho_a
  for(index a = 0; a < GroupElement::nGenerators; ++a)
  {
    auto forwardPlan  = fftw_plan_dft_r2c_2d(gridSizeX, gridSizeY, fourierInput.data(),
                                             (fftw_complex *) fourierOutput.data(), FFTW_ESTIMATE);
    auto backwardPlan = fftw_plan_dft_c2r_2d(gridSizeX, gridSizeY,
                                             (fftw_complex *) fourierOutput.data(),
                                             fourierInput.data(), FFTW_ESTIMATE);

    for(index i = 0; i < nCells; ++i)
    {
      fourierInput[i] = static_cast<double>(rho[i].getComponent(a));
    }

    // Do the forward Fourier transformation
    fftw_execute(forwardPlan);

    for(index iX = 0; iX < gridSizeX; ++iX)
    {
      for(index iY = 0; iY < (gridSizeY / 2 + 1); ++iY)
      {
        index i = iX * (gridSizeY / 2 + 1) + iY;
        if(i == 0)
        {
          // Set DC component to 0 to ensure global color neutrality
          fourierOutput[0] = 0.0;
        }
        else
        {
          double kSquared = (iX * dKx) * (iX * dKx) + (iY * dKy) * (iY * dKy);
          if((cutoff == Cutoff::hard) && (kSquared > lambdaSquared))
          {
            fourierOutput[i] = 0.0;
          }
          else
          {
            fourierOutput[i] /= (kSquared + mSquared);
          }
        }
      }
    }

    // Do the inverse Fourier transformation
    fftw_execute(backwardPlan);
    // Divide each element by the number of cells, because FFTW calculates an unnormalized DFT
    for(auto & x : fourierInput)
    {
      x /= nCells;
    }

    for(index i = 0; i < nCells; ++i)
    {
      phi[i].setComponent(a, static_cast<rational>(fourierInput[i]));
    }

    fftw_destroy_plan(forwardPlan);
    fftw_destroy_plan(backwardPlan);
  }

  fftw_cleanup();

  return phi;
}


/// @brief  Writes information about the threads that are available and used for the simulation.
void writeOpenMpInfo(std::ostream & outputStream)
{
#ifdef _OPENMP
#pragma omp parallel
#pragma omp single
  {
    outputStream << omp_get_num_threads() << " threads will be used on " << omp_get_num_procs();
    outputStream << " available processors" << std::endl;
  }
#else
  outputStream << "This program was built without OpenMP!" << std::endl;
#endif
}


/// @brief  Platform independent and thread-safe implementation of `std::localtime()`.
tm localTime(const std::time_t & time)
{
  std::tm timeInfo;
#if (defined(WIN32) || defined(_WIN32) || defined(__WIN32__))
  localtime_s(&timeInfo, &time);
#else
  localtime_r(&time, &timeInfo); // POSIX
#endif
  return timeInfo;
}
}
}