#pragma once

#include <vector>
#include <ostream>
#include <ctime>

#include <omp.h>

#include "CustomTypes.h"
#include "GroupAndAlgebra.h"

namespace openpixi
{
namespace utility
{
/// @brief  This is used to specifiy what kind of momentum cutoff to use in
///         solve2dPoissonEquation().
enum class Cutoff
{
  none,
  hard
};

std::ostream & operator<<(std::ostream & stream, Cutoff const & cutoff);
std::vector<AlgebraElement> solve2dPoissonEquation(std::vector<AlgebraElement> & rho,
                                                   const index gridSizeX, const index gridSizeY,
                                                   const rational latticeSpacingX,
                                                   const rational latticeSpacingY,
                                                   const rational infraredRegulator = 0.0,
                                                   Cutoff cutoff = Cutoff::none,
                                                   const rational cutoffValue = -1.0);
void writeOpenMpInfo(std::ostream & outputStream);
tm localTime(const std::time_t & time);

/// @brief  Parallel version of `std::fill()`
template<typename T>
void parallelFill(std::vector<T> & vector, const T value)
{
#ifdef _OPENMP
  const auto threadId  = omp_get_thread_num();
  const auto chunkSize = vector.size() / omp_get_num_threads();
  const auto begin = vector.begin() + chunkSize * threadId;
  const auto end   = (threadId == omp_get_num_threads() - 1) ? vector.end() : begin + chunkSize;
#else
  const auto begin = vector.begin();
  const auto end   = vector.end();
#endif
  std::fill(begin, end, value);
}


/// @brief  Checks if the given value is integer.
///
/// @return true if the value is integer
/// @return false otherwise
constexpr bool isInteger(const rational value)
{
  return static_cast<openpixi::rational>(static_cast<int>(value)) == value;
}
}
}