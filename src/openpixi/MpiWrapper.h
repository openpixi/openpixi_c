#pragma once

#include <ostream>

namespace mpiwrapper
{
/// @brief  A lightweight C++ wrapper for MPI.
///
/// It is implemented as a singleton to ensure that only a single MPI environment can be
/// initialized.
class Mpi
{
public:
  Mpi(Mpi const &) = delete;                // No copy construction
  Mpi & operator=(Mpi const &) = delete;    // No copy assignment

  static Mpi & getInstance(void);

  void writeWorldInfo(std::ostream & outputStream) const;
  int worldSize(void) const;
  int worldRank(void) const;

private:
  Mpi();
  ~Mpi();

  int worldSize_;
  int worldRank_;
};
}