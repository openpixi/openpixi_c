#pragma once

#include <array>
#include <vector>
#include <fstream>

#include "CustomTypes.h"

#include "GroupAndAlgebra.h"
#include "Settings.h"
#include "SuperParticle.h"

#ifndef STORE_PLAQUETTES
/// @brief  Determines whether some plaquettes are stored for later use, or if they are always
///         calculated when needed.
///
/// Storing the plaquettes reduces execution time but increases memory consumption.
#define STORE_PLAQUETTES 1
#endif
#ifndef USE_INDEXES
/// @brief  Determines whether loops and calculations use indexes or 3d position vectors.
#define USE_INDEXES 1
#endif
#ifndef USE_STAPLE_SUM
/// @brief  Determines whether updateFieldsAndLinks() uses the staple sum or plaquettes to solve the
///         equations of motion.
///
/// @note   This option has no effect if @ref STORE_PLAQUETTES == 1
#define USE_STAPLE_SUM 0
#endif

namespace openpixi
{
class DataFile;

enum class TimeIndex
{
  previous,   ///< @f$t-a_0/2@f$
  next        ///< @f$t+a_0/2@f$
};

enum class Orientation
{
  negative = -1,  ///< -1
  positive = 1    ///< +1
};

/// @brief  A class containing the simulation core.
///
/// This includes vectors for the degrees of freedom, a solver for them, basic grid functions and
/// utility functions for physical quantities.
class Simulation : public Settings
{
public:
  /// @todo try to explain how this works
  // Uncrustify-Off
  static constexpr std::array<std::array<index, 2>, 3> inverseEpsilonSymbol = {{{1, 2},
                                                                                {2, 0},
                                                                                {0, 1}}};
  // Uncrustify-On

  std::array<index, nDimensions + 1> accumulatedCellCounts;
  /// @brief  @f$E_{\text{dimensionless}} \cdot \text{unitFactorsE} = E_{\text{physical}}@f$
  std::array<rational, nDimensions> unitFactorsE;
  /// @brief  @f$B_{\text{dimensionless}} \cdot \text{unitFactorsB} = B_{\text{physical}}@f$
  std::array<rational, nDimensions> unitFactorsB;
  /// @brief  @f$\rho_{\text{dimensionless}} \cdot \text{unitFactorRho} = \rho_{\text{physical}}@f$
  rational unitFactorRho;
  /// @brief  @f$j_{\text{dimensionless}} \cdot \text{unitFactorJ} = j_{\text{physical}}@f$
  rational unitFactorJ;
  /// @brief  Dimensionless electric field
  std::vector<std::array<AlgebraElement, nDimensions>> E;
  /// @brief  Dimensionless gauge links half a time step before the electric field
  std::vector<std::array<GroupElement, nDimensions>>   oldU;
  /// @brief  Dimensionless gauge links half a time step after the electric field
  std::vector<std::array<GroupElement, nDimensions>>   newU;
#if STORE_PLAQUETTES == 1
  /// @brief  Dimensionless plaquettes half a time step before the electric field
  std::vector<std::array<GroupElement, nPlaquettes>> oldPlaquettes;
  /// @brief  Dimensionless plaquettes half a time step after the electric field
  std::vector<std::array<GroupElement, nPlaquettes>> newPlaquettes;
#endif
  /// @brief  Dimensionless charge density at the time of the electric field
  std::vector<AlgebraElement> rho;
  /// @brief  Dimensionless, longitudinal current density half a time step before the electric field
  std::vector<AlgebraElement> oldJ;
  /// @brief  Dimensionless, longitudinal current density half a time step after the electric field
  std::vector<AlgebraElement> newJ;
  /// @brief  Super particles containing right moving, dimensionless, point-like charges
  std::vector<SuperParticle<Velocity::positive>> rightMovingSuperParticles;
  /// @brief  Super particles containing left moving, dimensionless, point-like charges
  std::vector<SuperParticle<Velocity::negative>> leftMovingSuperParticles;

  /// @name   Index bounds for super particles
  /// @brief  Only particles within these bounds will be updated.
  ///@{
  /// The bounds are initialized in applyInitialConditions().
  index minRightMovingParticleIndex;
  index maxRightMovingParticleIndex;
  index minLeftMovingParticleIndex;
  index maxLeftMovingParticleIndex;
  ///@}

  explicit Simulation(Settings settings);

  // Basic grid functions
  index getIndex(std::array<index, nDimensions> const & position) const;
  std::array<index, nDimensions> getPosition(const index positionIndex) const;
  index shift(const index positionIndex, const index direction,
              const Orientation orientation) const;
  std::array<index, nDimensions> shift(std::array<index, nDimensions> const & position,
                                       const index direction, const Orientation orientation) const;

  GroupElement getLink(const index positionIndex, const index direction,
                       const Orientation orientation, const TimeIndex timeIndex) const;
  GroupElement getLink(const std::array<index, nDimensions> position, const index direction,
                       const Orientation orientation, const TimeIndex timeIndex) const;
  GroupElement calculatePlaquette(const index positionIndex, const index direction1,
                                  const index direction2, const Orientation orientation1,
                                  const Orientation orientation2, const TimeIndex timeIndex) const;
  GroupElement calculatePlaquette(const std::array<index, nDimensions> position,
                                  const index direction1, const index direction2,
                                  const Orientation orientation1, const Orientation orientation2,
                                  const TimeIndex timeIndex) const;
  GroupElement calculateTemporalPlaquette(const index positionIndex, const index direction,
                                          const Orientation orientation) const;
  GroupElement calculateStapleSum(const index positionIndex, const index direction) const;
  GroupElement calculateStapleSum(const std::array<index, nDimensions> position,
                                  const index direction) const;
  GroupElement getPlaquette(const index positionIndex, const index direction1,
                            const index direction2, const Orientation orientation1,
                            const Orientation orientation2, const TimeIndex timeIndex) const;
  GroupElement getPlaquette(const std::array<index, nDimensions> position,
                            const index direction1, const index direction2,
                            const Orientation orientation1, const Orientation orientation2,
                            const TimeIndex timeIndex) const;
  AlgebraElement calculateB(const index positionIndex, const index direction,
                            const TimeIndex timeIndex) const;
  AlgebraElement calculateB(const std::array<index, nDimensions> position,
                            const index direction, const TimeIndex timeIndex) const;

  void applyInitialConditions(void);
  void applyGaugeTransformation(std::vector<GroupElement> V);

  // Equation of motion solver
  void updateFieldsAndLinks(const index positionIndex);
  void updateFieldsAndLinks(const std::array<index, nDimensions> position);
#if STORE_PLAQUETTES == 1
  void updatePlaquettes(const index positionIndex);
  void updatePlaquettes(const std::array<index, nDimensions> position);
#endif
  void evolve(void);

  void executeDiagnostics(void) const;
  int writeDiagnosticsToDataFile(DataFile const & dataFile, const int currentTimeStep) const;

  // Print utility functions
  void printProjectedE(std::ofstream & outputStream, const Direction direction) const;
  void printProjectedB(std::ofstream & outputStream, const Direction direction,
                       const TimeIndex timeIndex) const;
};
}