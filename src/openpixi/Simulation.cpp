#include "Simulation.h"

#include <utility>
#include <algorithm>
#include <omp.h>

#include "DataFile.h"
#include "diagnostics/Diagnostics.h"
#include "initial/InitialCondition.h"

namespace openpixi
{
constexpr std::array<std::array<index, 2>, 3> Simulation::inverseEpsilonSymbol;

/// @brief  Sets up all factors and constants for the simulation and resizes all vectors of the
///         degrees of freedom.
Simulation::Simulation(Settings settings) : Settings(std::move(settings))
{
  accumulatedCellCounts[nDimensions] = 1;
  for(int i = nDimensions - 1; i >= 0; i--)
  {
    accumulatedCellCounts[i] = accumulatedCellCounts[i + 1] * gridSize[i];
  }

  unitFactorsE = {1.0 / (couplingConstant * timeStep * latticeSpacings[x]),
                  1.0 / (couplingConstant * timeStep * latticeSpacings[y]),
                  1.0 / (couplingConstant * timeStep * latticeSpacings[z])};
  unitFactorsB = {1.0 / (couplingConstant * latticeSpacings[y] * latticeSpacings[z]),
                  1.0 / (couplingConstant * latticeSpacings[x] * latticeSpacings[z]),
                  1.0 / (couplingConstant * latticeSpacings[x] * latticeSpacings[y])};
  unitFactorRho = 1.0 / (couplingConstant * timeStep * latticeSpacings[x] * latticeSpacings[x]);
  unitFactorJ   = 1.0 / (couplingConstant * timeStep * timeStep * latticeSpacings[x]);

  const index nCells = accumulatedCellCounts[0];

  E.resize(nCells);
  oldU.resize(nCells);
  newU.resize(nCells);
#if STORE_PLAQUETTES == 1
  oldPlaquettes.resize(nCells);
  newPlaquettes.resize(nCells);
#endif
  rho.resize(nCells);
  oldJ.resize(nCells);
  newJ.resize(nCells);

#pragma omp parallel for
  for(index i = 0; i < nCells; ++i)
  {
    E[i]    = {AlgebraElement(), AlgebraElement(), AlgebraElement()};
    oldU[i] = {GroupElement(), GroupElement(), GroupElement()};
    newU[i] = {GroupElement(), GroupElement(), GroupElement()};
#if STORE_PLAQUETTES == 1
    oldPlaquettes[i] = {GroupElement(), GroupElement(), GroupElement()};
    newPlaquettes[i] = {GroupElement(), GroupElement(), GroupElement()};
#endif
    rho[i]  = AlgebraElement();
    oldJ[i] = AlgebraElement();
    newJ[i] = AlgebraElement();
  }

  minRightMovingParticleIndex = 0;
  maxRightMovingParticleIndex = 0;
  minLeftMovingParticleIndex  = 0;
  maxLeftMovingParticleIndex  = 0;
}


/// @brief  Returns the position index corresponding to the given position vector.
index Simulation::getIndex(std::array<index, nDimensions> const & position) const
{
  return (position[x] * gridSize[y] + position[y]) * gridSize[z] + position[z];
}


/// @brief  Returns the position vector corresponding to the given position index.
std::array<index, Simulation::nDimensions> Simulation::getPosition(const index positionIndex) const
{
  std::array<index, nDimensions> position;
  position[z] = positionIndex % gridSize[z];
  position[y] = positionIndex / gridSize[z] % gridSize[y];
  position[x] = positionIndex / (gridSize[y] * gridSize[z]);
  return position;
}


/// @brief  Shifts the position index in the given direction and orientation.
///
/// Periodic boundary conditions are used.
/// @param[in]  positionIndex Current position index.
/// @param[in]  direction     Direction of the shift. Must be 0, 1 or 2.
/// @param[in]  orientation   Sign of the direction.
/// @return     The shifted position index.
index Simulation::shift(const index positionIndex, const index direction,
                        const Orientation orientation) const
{
  index result = positionIndex;
  const index directionIndex = positionIndex / accumulatedCellCounts[direction + 1];
  const index withinDirectionIndex = directionIndex % gridSize[direction];
  if(orientation == Orientation::positive)
  {
    if(withinDirectionIndex == gridSize[direction] - 1)
    {
      // Wrap around along positive direction
      result -= accumulatedCellCounts[direction];
    }
    result += accumulatedCellCounts[direction + 1];
  }
  else
  {
    if(withinDirectionIndex == 0)
    {
      // Wrap around along negative direction
      result += accumulatedCellCounts[direction];
    }
    result -= accumulatedCellCounts[direction + 1];
  }
  return result;
}


/// @brief  Shifts the position vector in the given direction and orientation.
///
/// Periodic boundary conditions are used.
/// @param[in]  position    Current position vector
/// @param[in]  direction   Direction of the shift. Must be 0, 1 or 2.
/// @param[in]  orientation Sign of the direction.
/// @return     The shifted position vector.
std::array<index, Simulation::nDimensions> Simulation::shift(
  std::array<index, nDimensions> const & position, const index direction,
  const Orientation orientation) const
{
  auto result = position;
  result[direction] += static_cast<int>(orientation) + gridSize[direction];
  result[direction]  = result[direction] % gridSize[direction];
  return result;
}


/// @brief  Returns the gauge link at the position index, in the given direction and orientation.
///
/// @param[in]  positionIndex Position index of the gauge link.
/// @param[in]  direction     Direction of the gauge link. Must be 0, 1 or 2.
/// @param[in]  orientation   Sign of the direction.
/// @param[in]  timeIndex     If `timeIndex` = TimeIndex::previous, @a oldU is used, otherwise
///                           @a newU is used.
/// @return     The gauge link at the position index, in the given direction and orientation
GroupElement Simulation::getLink(const index positionIndex, const index direction,
                                 const Orientation orientation, const TimeIndex timeIndex) const
{
  if(timeIndex == TimeIndex::previous)
  {
    if(orientation == Orientation::negative)
    {
      return oldU[shift(positionIndex, direction, orientation)][direction].dagger();
    }
    return oldU[positionIndex][direction];
  }
  else
  {
    if(orientation == Orientation::negative)
    {
      return newU[shift(positionIndex, direction, orientation)][direction].dagger();
    }
    return newU[positionIndex][direction];
  }
}


/// @brief  Returns the gauge link at the position vector, in the given direction and orientation.
///
/// @param[in]  position    Vector to the position of the gauge link.
/// @param[in]  direction   Direction of the gauge link. Must be 0, 1 or 2.
/// @param[in]  orientation Sign of the direction.
/// @param[in]  timeIndex   If `timeIndex` = TimeIndex::previous, @a oldU is used, otherwise
///                         @a newU is used.
/// @return     The gauge link at the position, in the given direction and orientation
GroupElement Simulation::getLink(const std::array<index, nDimensions> position,
                                 const index direction, const Orientation orientation,
                                 const TimeIndex timeIndex) const
{
  if(timeIndex == TimeIndex::previous)
  {
    if(orientation == Orientation::negative)
    {
      return oldU[getIndex(shift(position, direction, orientation))][direction].dagger();
    }
    return oldU[getIndex(position)][direction];
  }
  else
  {
    if(orientation == Orientation::negative)
    {
      return newU[getIndex(shift(position, direction, orientation))][direction].dagger();
    }
    return newU[getIndex(position)][direction];
  }
}


/// @brief  Calculates the plaquette at the position index in the given directions and orientations.
///
/// The following definition of the plaquette is used:
/// @f[U_{x, ij} = U_{x, i} U_{x+i, j} U_{x+i+j, -i} U_{x+j, -j}@f]
///
/// @param[in]  positionIndex @f$x@f$
/// @param[in]  direction1    @f$i@f$
/// @param[in]  direction2    @f$j@f$
/// @param[in]  orientation1  Sign of @f$i@f$.
/// @param[in]  orientation2  Sign of @f$j@f$.
/// @param[in]  timeIndex     If `timeIndex` = TimeIndex::previous, @a oldU is used for the
///                           calculation, otherwise @a newU is used.
/// @return     @f$U_{x,ij}@f$
GroupElement Simulation::calculatePlaquette(const index positionIndex, const index direction1,
                                            const index direction2, const Orientation orientation1,
                                            const Orientation orientation2,
                                            const TimeIndex timeIndex) const
{
  const auto negativeOrientation1 = static_cast<Orientation>(-static_cast<int>(orientation1));
  const auto negativeOrientation2 = static_cast<Orientation>(-static_cast<int>(orientation2));

  // The four lattice indexes associated with the plaquette
  index x1 = positionIndex;
  index x2 = shift(x1, direction1, orientation1);
  index x3 = shift(x2, direction2, orientation2);
  index x4 = shift(x3, direction1, negativeOrientation1);

  // The four gauge links associated with the plaquette
  auto U1 = getLink(x1, direction1, orientation1, timeIndex);
  auto U2 = getLink(x2, direction2, orientation2, timeIndex);
  auto U3 = getLink(x3, direction1, negativeOrientation1, timeIndex);
  auto U4 = getLink(x4, direction2, negativeOrientation2, timeIndex);

  auto result = (U1 * U2) * (U3 * U4);
  return result;
}


/// @brief  Calculates the plaquette at the position vector in the given directions and
///         orientations.
///
/// The following definition of the plaquette is used:
/// @f[U_{x, ij} = U_{x, i} U_{x+i, j} U_{x+i+j, -i} U_{x+j, -j}@f]
///
/// @param[in]  position      Vector to the position of the plaquette.
/// @param[in]  direction1    @f$i@f$
/// @param[in]  direction2    @f$j@f$
/// @param[in]  orientation1  Sign of @f$i@f$.
/// @param[in]  orientation2  Sign of @f$j@f$.
/// @param[in]  timeIndex     If `timeIndex` = TimeIndex::previous, @a oldU is used for the
///                           calculation, otherwise @a newU is used.
/// @return     @f$U_{x,ij}@f$
GroupElement Simulation::calculatePlaquette(const std::array<index, nDimensions> position,
                                            const index direction1, const index direction2,
                                            const Orientation orientation1,
                                            const Orientation orientation2,
                                            const TimeIndex timeIndex) const
{
  const auto negativeOrientation1 = static_cast<Orientation>(-static_cast<int>(orientation1));
  const auto negativeOrientation2 = static_cast<Orientation>(-static_cast<int>(orientation2));

  // The four lattice indexes associated with the plaquette
  auto position1 = position;
  auto position2 = shift(position1, direction1, orientation1);
  auto position3 = shift(position2, direction2, orientation2);
  auto position4 = shift(position3, direction1, negativeOrientation1);

  // The four gauge links associated with the plaquette
  auto U1 = getLink(position1, direction1, orientation1, timeIndex);
  auto U2 = getLink(position2, direction2, orientation2, timeIndex);
  auto U3 = getLink(position3, direction1, negativeOrientation1, timeIndex);
  auto U4 = getLink(position4, direction2, negativeOrientation2, timeIndex);

  return (U1 * U2) * (U3 * U4);
}


/// @brief  Calculates the temporal plaquette at the position index in the given direction and
///         orientation.
///
/// The following definition of the temporal plaquette is used:
/// @f[U_{x,0i}(t-\Delta t/2)=U_{x,0}(t-\Delta t/2)U_{x,i}(t+\Delta t/2)U_{x+i,-0}(t+\Delta t/2)
/// U_{x+i,-i}(t-\Delta t/2)@f]
///
/// @param[in]  positionIndex @f$x@f$
/// @param[in]  direction     @f$i@f$
/// @param[in]  orientation   Sign of @f$i@f$.
/// @return     @f$U_{x,0i}(t-\Delta t/2)@f$
GroupElement Simulation::calculateTemporalPlaquette(const index positionIndex,
                                                    const index direction,
                                                    const Orientation orientation) const
{
  // Since temporal links U1 and U3 are trivial in the temporal gauge, only get U2 and U4
  auto U2 = getLink(positionIndex, direction, orientation, TimeIndex::next);
  auto U4 = getLink(positionIndex, direction, orientation, TimeIndex::previous).dagger();

  return U2 * U4;
}


/// @brief  Calculates the sum of staples surrounding the gauge link at the position index along the
///         given direction.
///
/// The following definition of the staple sum is used:
/// @f[S_{x,i}=\sum_{j\ne i}\frac{a_0^2}{a_j^2}\left(U_{x+i,j}U_{x+j,i}^\dagger U_{x,j}^\dagger+
/// U_{x+i-j,j}^\dagger U_{x-j,i}^\dagger U_{x-j,j}^\dagger\right)@f]
/// This already contains the correct prefactor for the equations of motion.
///
/// @param[in]  positionIndex @f$x@f$
/// @param[in]  direction     @f$i@f$
/// @return     @f$S_{x,i}@f$
GroupElement Simulation::calculateStapleSum(const index positionIndex, const index direction) const
{
  static const std::array<rational, nDimensions> cubedLatticeSpacings =
  {latticeSpacings[x] * latticeSpacings[x] * latticeSpacings[x],
   latticeSpacings[y] * latticeSpacings[y] * latticeSpacings[y],
   latticeSpacings[z] * latticeSpacings[z] * latticeSpacings[z]};
  static const std::array<rational, nDimensions> prefactor =
  {timeStep / (couplingConstant * cubedLatticeSpacings[x] * unitFactorsE[x]),
   timeStep / (couplingConstant * cubedLatticeSpacings[y] * unitFactorsE[y]),
   timeStep / (couplingConstant * cubedLatticeSpacings[z] * unitFactorsE[z])};

  const auto   positionIndex1 = shift(positionIndex, direction, Orientation::positive);

  GroupElement stapleSum(0.0, 0.0, 0.0, 0.0);
  for(index i = 0; i < nDimensions; ++i)
  {
    if(i != direction)
    {
      // Calculate all necessary position indexes
      auto positionIndex2 = shift(positionIndex, i, Orientation::positive);
      auto positionIndex3 = shift(positionIndex1, i, Orientation::negative);
      auto positionIndex4 = shift(positionIndex, i, Orientation::negative);

      auto staple1 = oldU[positionIndex1][i] *
                     (oldU[positionIndex][i] * oldU[positionIndex2][direction]).dagger();
      auto staple2 = (oldU[positionIndex4][direction] * oldU[positionIndex3][i]).dagger() *
                     oldU[positionIndex4][i];

      // Include the correct prefactor for the field equations of motion
      stapleSum += (staple1 + staple2) * prefactor[i];
    }
  }

  return stapleSum;
}


/// @brief  Calculates the sum of staples surrounding the gauge link at the position vector along
///         the given direction.
///
/// The following definition of the staple sum is used:
/// @f[S_{x,i}=\sum_{j\ne i}\frac{a_0^2}{a_j^2}\left(U_{x+i,j}U_{x+j,i}^\dagger U_{x,j}^\dagger+
/// U_{x+i-j,j}^\dagger U_{x-j,i}^\dagger U_{x-j,j}^\dagger\right)@f]
/// This already contains the correct prefactor for the equations of motion.
///
/// @param[in]  position  Vector to the position of the staple sum.
/// @param[in]  direction @f$i@f$
/// @return     @f$S_{x,i}@f$
GroupElement Simulation::calculateStapleSum(const std::array<index, nDimensions> position,
                                            const index direction) const
{
  static const std::array<rational, nDimensions> cubedLatticeSpacings =
  {latticeSpacings[x] * latticeSpacings[x] * latticeSpacings[x],
   latticeSpacings[y] * latticeSpacings[y] * latticeSpacings[y],
   latticeSpacings[z] * latticeSpacings[z] * latticeSpacings[z]};
  static const std::array<rational, nDimensions> prefactor =
  {timeStep / (couplingConstant * cubedLatticeSpacings[x] * unitFactorsE[x]),
   timeStep / (couplingConstant * cubedLatticeSpacings[y] * unitFactorsE[y]),
   timeStep / (couplingConstant * cubedLatticeSpacings[z] * unitFactorsE[z])};

  const auto   position1 = shift(position, direction, Orientation::positive);
  const auto   positionIndex  = getIndex(position);
  const auto   positionIndex1 = getIndex(position1);

  GroupElement stapleSum(0.0, 0.0, 0.0, 0.0);
  for(index i = 0; i < nDimensions; ++i)
  {
    if(i != direction)
    {
      // Calculate all necessary position vectors and indexes
      auto position2 = shift(position, i, Orientation::positive);
      auto position3 = shift(position1, i, Orientation::negative);
      auto position4 = shift(position, i, Orientation::negative);
      auto positionIndex2 = getIndex(position2);
      auto positionIndex3 = getIndex(position3);
      auto positionIndex4 = getIndex(position4);

      auto staple1 = oldU[positionIndex1][i] *
                     (oldU[positionIndex][i] * oldU[positionIndex2][direction]).dagger();
      auto staple2 = (oldU[positionIndex4][direction] * oldU[positionIndex3][i]).dagger() *
                     oldU[positionIndex4][i];

      // Include the correct prefactor for the field equations of motion
      stapleSum += (staple1 + staple2) * prefactor[i];
    }
  }

  return stapleSum;
}


/// @brief  Returns the required plaquette.
///
/// If `STORE_PLAQUETTES` is defined, the required plaquette is calculated from
/// Simulation::oldPlaquettes or Simulation::newPlaquettes, otherwise it is calculated from the
/// gauge links using calculatePlaquette().
///
/// @param[in]  positionIndex @f$x@f$
/// @param[in]  direction1    @f$i@f$
/// @param[in]  direction2    @f$j@f$
/// @param[in]  orientation1  Sign of @f$i@f$.
/// @param[in]  orientation2  Sign of @f$j@f$.
/// @param[in]  timeIndex     If `timeIndex` = TimeIndex::previous, the old plaquettes or links are
///                           used, otherwise the new ones are used.
/// @return     @f$U_{x,ij}@f$
GroupElement Simulation::getPlaquette(const index positionIndex, const index direction1,
                                      const index direction2, const Orientation orientation1,
                                      const Orientation orientation2,
                                      const TimeIndex timeIndex) const
{
#if STORE_PLAQUETTES == 1
  GroupElement result;

  if((orientation1 == Orientation::negative) && (orientation2 == Orientation::negative))
  {
    // The stored plaquette must be shifted with two gauge links to get the correct result
    auto positionIndex1 = shift(positionIndex, direction1, orientation1);
    auto positionIndex2 = shift(positionIndex1, direction2, orientation2);
    auto U1 = getLink(positionIndex1, direction1, Orientation::positive, timeIndex);
    auto U2 = getLink(positionIndex2, direction2, Orientation::positive, timeIndex);
    if(timeIndex == TimeIndex::previous)
    {
      result = (U2 * U1).dagger() * oldPlaquettes[positionIndex2][direction1 + direction2 - 1] *
               (U2 * U1);
    }
    else
    {
      result = (U2 * U1).dagger() * newPlaquettes[positionIndex2][direction1 + direction2 - 1] *
               (U2 * U1);
    }
  }
  else if(orientation2 == Orientation::negative)
  {
    // The stored plaquette must be shifted with a gauge link to get the correct result
    auto positionIndex1 = shift(positionIndex, direction2, orientation2);
    auto U1 = getLink(positionIndex1, direction2, Orientation::positive, timeIndex);
    if(timeIndex == TimeIndex::previous)
    {
      result = (oldPlaquettes[positionIndex1][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
    else
    {
      result = (newPlaquettes[positionIndex1][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
  }
  else if(orientation1 == Orientation::negative)
  {
    // The stored plaquette must be shifted with a gauge link to get the correct result
    auto positionIndex1 = shift(positionIndex, direction1, orientation1);
    auto U1 = getLink(positionIndex1, direction1, Orientation::positive, timeIndex);
    if(timeIndex == TimeIndex::previous)
    {
      result = (oldPlaquettes[positionIndex1][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
    else
    {
      result = (newPlaquettes[positionIndex1][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
  }
  else
  {
    // The stored plaquette can be used as it is
    if(timeIndex == TimeIndex::previous)
    {
      result = oldPlaquettes[positionIndex][direction1 + direction2 - 1];
    }
    else
    {
      result = newPlaquettes[positionIndex][direction1 + direction2 - 1];
    }
  }

  if(direction1 > direction2)
  {
    // U_{x,ji}^\dagger = U_{x,ij}
    result = result.dagger();
  }

  return result;
#else
  return calculatePlaquette(positionIndex, direction1, direction2, orientation1, orientation2,
                            timeIndex);
#endif
}


/// @brief  Returns the required plaquette.
///
/// If `STORE_PLAQUETTES` is defined, the required plaquette is calculated from
/// Simulation::oldPlaquettes or Simulation::newPlaquettes, otherwise it is calculated from the
/// gauge links using calculatePlaquette().
///
/// @param[in]  position      Vector to the position of the plaquette.
/// @param[in]  direction1    @f$i@f$
/// @param[in]  direction2    @f$j@f$
/// @param[in]  orientation1  Sign of @f$i@f$.
/// @param[in]  orientation2  Sign of @f$j@f$.
/// @param[in]  timeIndex     If `timeIndex` = TimeIndex::previous, the old plaquettes or links are
///                           used, otherwise the new ones are used.
/// @return     @f$U_{x,ij}@f$
GroupElement Simulation::getPlaquette(const std::array<index, nDimensions> position,
                                      const index direction1, const index direction2,
                                      const Orientation orientation1,
                                      const Orientation orientation2,
                                      const TimeIndex timeIndex) const
{
#if STORE_PLAQUETTES == 1
  GroupElement result;

  if((orientation1 == Orientation::negative) && (orientation2 == Orientation::negative))
  {
    // The stored plaquette must be shifted with two gauge links to get the correct result
    auto position1 = shift(position, direction1, orientation1);
    auto position2 = shift(position1, direction2, orientation2);
    auto positionIndex = getIndex(position2);
    auto U1 = getLink(position1, direction1, Orientation::positive, timeIndex);
    auto U2 = getLink(positionIndex, direction2, Orientation::positive, timeIndex);
    if(timeIndex == TimeIndex::previous)
    {
      result = (U2 * U1).dagger() * oldPlaquettes[positionIndex][direction1 + direction2 - 1] *
               (U2 * U1);
    }
    else
    {
      result = (U2 * U1).dagger() * newPlaquettes[positionIndex][direction1 + direction2 - 1] *
               (U2 * U1);
    }
  }
  else if(orientation2 == Orientation::negative)
  {
    // The stored plaquette must be shifted with a gauge link to get the correct result
    auto position1 = shift(position, direction2, orientation2);
    auto positionIndex = getIndex(position1);
    auto U1 = getLink(positionIndex, direction2, Orientation::positive, timeIndex);
    if(timeIndex == TimeIndex::previous)
    {
      result = (oldPlaquettes[positionIndex][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
    else
    {
      result = (newPlaquettes[positionIndex][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
  }
  else if(orientation1 == Orientation::negative)
  {
    // The stored plaquette must be shifted with a gauge link to get the correct result
    auto position1 = shift(position, direction1, orientation1);
    auto positionIndex = getIndex(position1);
    auto U1 = getLink(positionIndex, direction1, Orientation::positive, timeIndex);
    if(timeIndex == TimeIndex::previous)
    {
      result = (oldPlaquettes[positionIndex][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
    else
    {
      result = (newPlaquettes[positionIndex][direction1 + direction2 - 1] * U1).dagger() * U1;
    }
  }
  else
  {
    // The stored plaquette can be used as it is
    auto positionIndex = getIndex(position);
    if(timeIndex == TimeIndex::previous)
    {
      result = oldPlaquettes[positionIndex][direction1 + direction2 - 1];
    }
    else
    {
      result = newPlaquettes[positionIndex][direction1 + direction2 - 1];
    }
  }

  if(direction1 > direction2)
  {
    // U_{x,ji}^\dagger = U_{x,ij}
    result = result.dagger();
  }

  return result;
#else
  return calculatePlaquette(position, direction1, direction2, orientation1, orientation2,
                            timeIndex);
#endif
}


/// @brief  Calculates the magnetic field at the position index in the given direction.
///
/// The following definition of the magnetic field is used:
/// @f[\epsilon_{ijk}\mathcal{B}_x^k=-g a_i a_j\mathcal{F}_{x,ij}@f]
/// The field strength tensor @f$\mathcal{F}_{x,ij}@f$ is obtained by calculating the linearized
/// algebra element of the plaquette @f$U_{x,ij}@f$. The returned magnetic field
/// @f$\mathcal{B}_x^k@f$ is dimensionless.
///
/// @param[in]  positionIndex @f$x@f$
/// @param[in]  direction     @f$k@f$
/// @param[in]  timeIndex     If `timeIndex` = TimeIndex::previous, the old plaquettes are used,
///                           otherwise the new ones are used.
/// @return     @f$\mathcal{B}_x^k@f$
AlgebraElement Simulation::calculateB(const index positionIndex, const index direction,
                                      const TimeIndex timeIndex) const
{
  return getPlaquette(positionIndex, inverseEpsilonSymbol[direction][0],
                      inverseEpsilonSymbol[direction][1], Orientation::positive,
                      Orientation::positive, timeIndex).project();
}


/// @brief  Calculates the magnetic field at the position vector in the given direction.
///
/// The following definition of the magnetic field is used:
/// @f[\epsilon_{ijk}\mathcal{B}_x^k=-g a_i a_j\mathcal{F}_{x,ij}@f]
/// The field strength tensor @f$\mathcal{F}_{x,ij}@f$ is obtained by calculating the linearized
/// algebra element of the plaquette @f$U_{x,ij}@f$. The returned magnetic field
/// @f$\mathcal{B}_x^k@f$ is dimensionless.
///
/// @param[in]  position  Vector to the position of the magnetic field.
/// @param[in]  direction @f$k@f$
/// @param[in]  timeIndex If `timeIndex` = TimeIndex::previous, the old plaquettes are used,
///                       otherwise the new ones are used.
/// @return     @f$\mathcal{B}_x^k@f$
AlgebraElement Simulation::calculateB(const std::array<index, nDimensions> position,
                                      const index direction, const TimeIndex timeIndex) const
{
  return getPlaquette(position, inverseEpsilonSymbol[direction][0],
                      inverseEpsilonSymbol[direction][1], Orientation::positive,
                      Orientation::positive, timeIndex).project();
}


/// @brief  Initializes E-field, gauge links, super particles, current and charge density as well as
///         plaquettes.
///
/// This is done by applying all initial conditions stored in Settings::initialConditions, sorting
/// left and right moving super particles such that the ones hitting the boundary first have the
/// lowest index, initializing the super particle index bounds and interpolating the charge and
/// current density. If `STORE_PLAQUETTES` is defined, @a oldPlaquettes and @a newPlaquettes are
/// initialized as well.
/// @see    InitialCondition::apply()
/// @see    SuperParticle::initializeChargeAndCurrentDensity()
/// @see    updatePlaquettes()
void Simulation::applyInitialConditions(void)
{
  for(index i = 0; i < initialConditions.size(); ++i)
  {
    initialConditions[i]->apply(*this);
  }

  std::sort(rightMovingSuperParticles.begin(), rightMovingSuperParticles.end(),
            [](SuperParticle<Velocity::positive> first, SuperParticle<Velocity::positive> second)
    {
      return (first.currentPosition > second.currentPosition) ||
      ((first.currentPosition == second.currentPosition) &&
       (first.interCellPosition > second.interCellPosition));
    });
  std::sort(leftMovingSuperParticles.begin(), leftMovingSuperParticles.end(),
            [](SuperParticle<Velocity::negative> first, SuperParticle<Velocity::negative> second)
    {
      return (first.currentPosition < second.currentPosition) ||
      ((first.currentPosition == second.currentPosition) &&
       (first.interCellPosition < second.interCellPosition));
    });

  minRightMovingParticleIndex = 0;
  maxRightMovingParticleIndex = static_cast<index>(rightMovingSuperParticles.size());
  minLeftMovingParticleIndex  = 0;
  maxLeftMovingParticleIndex  = static_cast<index>(leftMovingSuperParticles.size());

  for(index i = minRightMovingParticleIndex; i < maxRightMovingParticleIndex; ++i)
  {
    rightMovingSuperParticles[i].initializeChargeAndCurrentDensity(*this);
  }
  for(index i = minLeftMovingParticleIndex; i < maxLeftMovingParticleIndex; ++i)
  {
    leftMovingSuperParticles[i].initializeChargeAndCurrentDensity(*this);
  }

#if STORE_PLAQUETTES == 1
  std::swap(oldU, newU);
  for(index x = leftIndexBoundary(); x < rightIndexBoundary(); ++x)
  {
    updatePlaquettes(x);
  }
  std::swap(oldPlaquettes, newPlaquettes);
  std::swap(oldU, newU);
  for(index x = leftIndexBoundary(); x < rightIndexBoundary(); ++x)
  {
    updatePlaquettes(x);
  }
#endif
}


/// @brief  Applies a time-independent gauge transformation to the fields and plaquettes.
///
/// The local gauge transformations with group elements @f$V_x@f$ are defined in the following way:
/// @f[E_{x,i}\rightarrow V_x E_{x,i} V_x^\dagger@f]
/// @f[U_{x,i}\rightarrow V_x U_{x,i} V_{x+i}^\dagger@f]
/// @f[U_{x,ij}\rightarrow V_x U_{x,ij} V_x^\dagger@f]
/// @f[\rho_{x,i}\rightarrow V_x \rho_{x,i} V_x^\dagger@f]
/// @f[j_{x,i}\rightarrow V_x j_{x,i} V_x^\dagger@f]
/// They are applied to the whole grid.
///
/// @param[in]  V Vector containing the elements @f$V_x@f$
/// @return     void
void Simulation::applyGaugeTransformation(std::vector<GroupElement> V)
{
  const index nCells = accumulatedCellCounts[0];

  for(index positionIndex = 0; positionIndex < nCells; ++positionIndex)
  {
    auto V1 = V[positionIndex].dagger();

    rho[positionIndex]  = rho[positionIndex].act(V1);
    oldJ[positionIndex] = oldJ[positionIndex].act(V1);
    newJ[positionIndex] = newJ[positionIndex].act(V1);
    for(index direction = 0; direction < nDimensions; ++direction)
    {
      E[positionIndex][direction] = E[positionIndex][direction].act(V1);

      auto shiftedIndex = shift(positionIndex, direction, Orientation::positive);
      auto V2 = V[shiftedIndex].dagger();
      oldU[positionIndex][direction] = V[positionIndex] * oldU[positionIndex][direction] * V2;
      newU[positionIndex][direction] = V[positionIndex] * newU[positionIndex][direction] * V2;
    }
#if STORE_PLAQUETTES == 1
    for(index i = 0; i < nPlaquettes; ++i)
    {
      oldPlaquettes[positionIndex][i] = V[positionIndex] * oldPlaquettes[positionIndex][i] * V1;
      newPlaquettes[positionIndex][i] = V[positionIndex] * newPlaquettes[positionIndex][i] * V1;
    }
#endif
  }
}


/// @brief  Solver for the equations of motion of the electric field and gauge links.
///
/// If `USE_STAPLE_SUM` is defined, the equations for the electric field read:
/// @f[E_x^{a,i}(t+\Delta t)= E_x^{a,i}(t)+2\mathrm{Im}\mathrm{Tr}\left(U_{x,i}(t+\Delta t/2)
/// S_{x,i}(t+\Delta t/2)t^a\right)-j_x^{a,i}(t+\Delta t/2)@f]
/// Otherwise, they use plaquettes and read:
/// @f[E_x^{a,i}(t+\Delta t)=E_x^{a,i}(t)+\sum_{j\ne i}\frac{2a_0^2}{a_j^2}\mathrm{Im}\mathrm{Tr}
/// \left(t^aU_{x,ij}(t+\Delta t/2)+t^aU_{x,i-j}(t+\Delta t/2)\right)-j_x^{a,i}(t+\Delta t/2)@f]
/// The equations of motion for the gauge links read:
/// @f[U_{x,i}(t+\Delta t/2)=\exp(-\mathrm{i}E_x^{a,i}(t)t_a)U_{x,i}(t-\Delta t/2)@f]
///
/// @param[in]  positionIndex @f$x@f$
/// @return     void
void Simulation::updateFieldsAndLinks(const index positionIndex)
{
#if USE_STAPLE_SUM == 1
  for(index i = 0; i < nDimensions; ++i)
  {
    // The staple sum already includes the correct prefactor
    E[positionIndex][i] += (oldU[positionIndex][i] *
                            calculateStapleSum(positionIndex, i)).project();
    if(i == Direction::x)
    {
      E[positionIndex][i] -= oldJ[positionIndex];
    }
    // The electric field used in the simulation is dimensionless and already contains the unit
    // factor g * a_t * a_i
    newU[positionIndex][i] = (-E[positionIndex][i]).exp() * oldU[positionIndex][i];
  }
#else
  for(index i = 0; i < nDimensions; ++i)
  {
    GroupElement plaquetteSum(0.0, 0.0, 0.0, 0.0);
    for(index j = 0; j < nDimensions; ++j)
    {
      if(j != i)
      {
        plaquetteSum += (getPlaquette(positionIndex, i, j, Orientation::positive,
                                      Orientation::positive, TimeIndex::previous) +
                         getPlaquette(positionIndex, i, j, Orientation::positive,
                                      Orientation::negative, TimeIndex::previous)) *
                        (1. / (latticeSpacings[j] * latticeSpacings[j]));
      }
    }
    // The electric field and current density used in the simulation are dimensionless and already
    // contain the correct prefactor
    E[positionIndex][i] += plaquetteSum.project() * (timeStep * timeStep);
    if(i == Direction::x)
    {
      E[positionIndex][i] -= oldJ[positionIndex];
    }
    newU[positionIndex][i] = (-E[positionIndex][i]).exp() * oldU[positionIndex][i];
  }
#endif
}


/// @brief  Solver for the equations of motion of the electric field and gauge links.
///
/// If `USE_STAPLE_SUM` is defined, the equations for the electric field read:
/// @f[E_x^{a,i}(t+\Delta t)= E_x^{a,i}(t)+2\mathrm{Im}\mathrm{Tr}\left(U_{x,i}(t+\Delta t/2)
/// S_{x,i}(t+\Delta t/2)t^a\right)-j_x^{a,i}(t+\Delta t/2)@f]
/// Otherwise, they use plaquettes and read:
/// @f[E_x^{a,i}(t+\Delta t)=E_x^{a,i}(t)+\sum_{j\ne i}\frac{2a_0^2}{a_j^2}\mathrm{Im}\mathrm{Tr}
/// \left(t^aU_{x,ij}(t+\Delta t/2)+t^aU_{x,i-j}(t+\Delta t/2)\right)-j_x^{a,i}(t+\Delta t/2)@f]
/// The equations of motion for the gauge links read:
/// @f[U_{x,i}(t+\Delta t/2)=\exp(-\mathrm{i}E_x^{a,i}(t)t_a)U_{x,i}(t-\Delta t/2)@f]
///
/// @param[in]  position  Vector to the position of the electric field and gauge link.
/// @return     void
void Simulation::updateFieldsAndLinks(const std::array<index, nDimensions> position)
{
  const index positionIndex = getIndex(position);
#if USE_STAPLE_SUM == 1
  for(index i = 0; i < nDimensions; ++i)
  {
    // The staple sum already includes the correct prefactor
    E[positionIndex][i] += (oldU[positionIndex][i] *
                            calculateStapleSum(position, i)).project();
    if(i == Direction::x)
    {
      E[positionIndex][i] -= oldJ[positionIndex];
    }
#else
  for(index i = 0; i < nDimensions; ++i)
  {
    GroupElement plaquetteSum(0.0, 0.0, 0.0, 0.0);
    for(index j = 0; j < nDimensions; ++j)
    {
      if(j != i)
      {
        plaquetteSum += (getPlaquette(position, i, j, Orientation::positive, Orientation::positive,
                                      TimeIndex::previous) +
                         getPlaquette(position, i, j, Orientation::positive, Orientation::negative,
                                      TimeIndex::previous)) *
                        (1. / (latticeSpacings[j] * latticeSpacings[j]));
      }
    }
    // The electric field and current density used in the simulation are dimensionless and already
    // contain the correct prefactor
    E[positionIndex][i] += plaquetteSum.project() * (timeStep * timeStep);
    if(i == Direction::x)
    {
      E[positionIndex][i] -= oldJ[positionIndex];
    }
#endif
    newU[positionIndex][i] = (-E[positionIndex][i].exp()) * oldU[positionIndex][i];
  }
}


#if STORE_PLAQUETTES == 1
/// @brief  Calculates and stores the new plaquettes using the new gauge links at the given position
///         index.
///
/// Only the following three plaquettes are stored:
/// 1. Simulation::newPlaquettes[x][0]=@f$U_{x,01}(t+\Delta t/2)@f$
/// 2. Simulation::newPlaquettes[x][1]=@f$U_{x,02}(t+\Delta t/2)@f$
/// 3. Simulation::newPlaquettes[x][2]=@f$U_{x,12}(t+\Delta t/2)@f$
///
/// All other plaquettes that are used in the simulation can be generated by these three.
/// @see getPlaquette()
void Simulation::updatePlaquettes(const index positionIndex)
{
  newPlaquettes[positionIndex][0] = calculatePlaquette(positionIndex, x, y, Orientation::positive,
                                                       Orientation::positive, TimeIndex::next);
  newPlaquettes[positionIndex][1] = calculatePlaquette(positionIndex, x, z, Orientation::positive,
                                                       Orientation::positive, TimeIndex::next);
  newPlaquettes[positionIndex][2] = calculatePlaquette(positionIndex, y, z, Orientation::positive,
                                                       Orientation::positive, TimeIndex::next);
}


#endif


#if STORE_PLAQUETTES == 1
/// @brief  Calculates and stores the new plaquettes using the new gauge links at the given
///         position vector.
///
/// Only the following three plaquettes are stored:
/// 1. Simulation::newPlaquettes[x][0]=@f$U_{x,01}(t+\Delta t/2)@f$
/// 2. Simulation::newPlaquettes[x][1]=@f$U_{x,02}(t+\Delta t/2)@f$
/// 3. Simulation::newPlaquettes[x][2]=@f$U_{x,12}(t+\Delta t/2)@f$
///
/// All other plaquettes that are used in the simulation can be generated by these three.
/// @see getPlaquette()
void Simulation::updatePlaquettes(const std::array<index, nDimensions> position)
{
  const index positionIndex = getIndex(position);

  newPlaquettes[positionIndex][0] = calculatePlaquette(position, x, y, Orientation::positive,
                                                       Orientation::positive, TimeIndex::next);
  newPlaquettes[positionIndex][1] = calculatePlaquette(position, x, z, Orientation::positive,
                                                       Orientation::positive, TimeIndex::next);
  newPlaquettes[positionIndex][2] = calculatePlaquette(position, y, z, Orientation::positive,
                                                       Orientation::positive, TimeIndex::next);
}


#endif


/// @brief  Updates all fields, links, plaquettes, densities and particles.
///
/// @see    Settings::boundaryCondition
void Simulation::evolve()
{
  using std::swap;

#pragma omp single
  {
    swap(oldU, newU);
#if STORE_PLAQUETTES == 1
    swap(oldPlaquettes, newPlaquettes);
#endif
    swap(oldJ, newJ);
  }
#pragma omp for schedule(static)
#if USE_INDEXES == 1
  for(index i = leftIndexBoundary(); i < rightIndexBoundary(); ++i)
  {
    updateFieldsAndLinks(i);
  }
#else
  for(index iX = leftBoundary; iX < rightBoundary; ++iX)
  {
    for(index iY = 0; iY < gridSize[y]; ++iY)
    {
      for(index iZ = 0; iZ < gridSize[z]; ++iZ)
      {
        std::array<index, nDimensions> position = {iX, iY, iZ};
        updateFieldsAndLinks(position);
      }
    }
  }
#endif

#if STORE_PLAQUETTES == 1
#pragma omp for schedule(static) nowait
#if USE_INDEXES == 1
  for(index i = leftIndexBoundary(); i < rightIndexBoundary(); ++i)
  {
    updatePlaquettes(i);
  }
#else
  for(index iX = leftBoundary; i < rightBoundary; ++iX)
  {
    for(index iY = 0; iY < gridSize[y]; ++iY)
    {
      for(index iZ = 0; iZ < gridSize[z]; ++iZ)
      {
        std::array<index, nDimensions> position = {iX, iY, iZ};
        updatePlaquettes(position);
      }
    }
  }
#endif
#endif

  const index nCells = accumulatedCellCounts[0];

  // Clear rho and newJ because the interpolation functions add the contributions of the particles
#pragma omp for schedule(static) nowait
  for(index i = 0; i < nCells; ++i)
  {
    rho[i] = AlgebraElement();
  }
#pragma omp for schedule(static)
  for(index i = 0; i < nCells; ++i)
  {
    newJ[i] = AlgebraElement();
  }

  // Update the particle charges and densities
  for(index i = minRightMovingParticleIndex; i < maxRightMovingParticleIndex; ++i)
  {
    auto returnValue = rightMovingSuperParticles[i].update(*this);
    if(returnValue != 0)
    {
      // If a particle hits the boundary, don't update it any more. Since the particles are
      // position ordered, the one hitting the boundary is the one with the lowest index.
#pragma omp single
      minRightMovingParticleIndex++;
    }
  }
  for(index i = minLeftMovingParticleIndex; i < maxLeftMovingParticleIndex; ++i)
  {
    auto returnValue = leftMovingSuperParticles[i].update(*this);
    if(returnValue != 0)
    {
      // If a particle hits the boundary, don't update it any more. Since the particles are
      // position ordered, the one hitting the boundary is the one with the lowest index.
#pragma omp single
      minLeftMovingParticleIndex++;
    }
  }
}


/// @brief  Executes all diagnostics stored in Settings::diagnostics.
///
/// @see    Diagnostics::execute()
void Simulation::executeDiagnostics(void) const
{
  for(index i = 0; i < diagnostics.size(); ++i)
  {
    diagnostics[i]->execute(*this);
  }
}


/// @brief  Writes all diagnostic values of the simulation for which @a shouldWriteToFile is true to
///         the given data file.
///
/// The time variable is always written.
///
/// @param[in]  dataFile        The dataFile the diagnostic values are written to.
/// @param[in]  currentTimeStep The time step, the simulation is currently at.
/// @return     A NetCDF error code if something went wrong, or @a noError otherwise.
///
/// @see    DataFile::writeTimeVariable()
/// @see    Settings::diagnostics
/// @see    Diagnostics::writeToDataFile()
int Simulation::writeDiagnosticsToDataFile(DataFile const & dataFile,
                                           const int currentTimeStep) const
{
  // First write the time variable
  int error = dataFile.writeTimeVariable(*this, currentTimeStep);
  if(error != DataFile::noError)
  {
    return error;
  }

  // Then write all the other diagnostics
  for(index i = 0; i < diagnostics.size(); ++i)
  {
    error = diagnostics[i]->writeToDataFile(*this, dataFile, currentTimeStep - t0);
    if(error != DataFile::noError)
    {
      return error;
    }
  }

  return DataFile::noError;
}


// --- Debug utility functions ---------------------------------------------------------------------

// Print the sum of the given E-field component over transversal planes in physical units
void Simulation::printProjectedE(std::ofstream & outputStream, const Direction direction) const
{
  index x = 0;
  for(index iL = 0; iL < gridSize[Direction::x]; ++iL)
  {
    rational sum = 0.0;
    for(index iT = 0; iT < accumulatedCellCounts[1]; ++iT)
    {
      sum += (E[x][direction] * unitFactorsE[direction]).square();
      x++;
    }
    outputStream << sum << " ";
  }
  outputStream << std::endl;
}


// Print the sum of the given B-field component over transversal planes in physical units
void Simulation::printProjectedB(std::ofstream & outputStream, const Direction direction,
                                 const TimeIndex timeIndex) const
{
  index x = 0;
  for(index iL = 0; iL < gridSize[Direction::x]; ++iL)
  {
    rational sum = 0.0;
    for(index iT = 0; iT < accumulatedCellCounts[1]; ++iT)
    {
      sum += (calculateB(x, direction, timeIndex) * unitFactorsB[direction]).square();
      x++;
    }
    outputStream << sum << " ";
  }
  outputStream << std::endl;
}
}